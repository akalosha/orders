# README #

This is  a simple Spring Boot application exposing REST-API for managing categories, products, order entries and orders. Main features are:

* REST APIs for CRUD operations on all entities - get, get all, add, edit, delete. 
* integration with Elasticsearch.
* launch with a single `docker-compose` command.

## Used technologies ##

* Java 13
* Sprint Boot
* Spring MVC
* Spring Data JPA & Spring Data Elasticsearch
* Liquibase
* Swagger 2
* Postgres
* Elasticsearch

## Running the app ##

### Running as docker container ###

```
cd orders/docker
sudo docker-compose up -d
```
This will start the server on port 8080.

### Build the app from sources ###

#### Prerequisites ####

* Git
* Maven 3.3+
* JDK 13
* **Postgres 11 running on port 5432**
* **Elasticsearch 6.5 running on ports 9200 & 9300**

Obtaining the sources:
```
git clone https://akalosha@bitbucket.org/akalosha/orders.git
```
Building the application:
```
cd orders
mvn clean package -DskipTests
```
(skipTests argument is needed to skip running integration tests which are designed to run in a separate integration environment)

Running the application:
```
java -jar target/orders-1.0-SNAPSHOT.jar
```
You can verify that app is running by executing GET request to [following URI](http://localhost:8080/products/1).

By default, application will listen to requests on port 8080. Port can be changed by passing an option `--server.port={port}` on the command line:
```
java -jar target/orders-1.0-SNAPSHOT.jar --server.port=8090
```

## API endpoints ##

List of all available endpoints is available by link [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).
Naturally, that requires the application to be up and running.

## Interacting with endpoints ##

Postman is highly recommended. Here is the link to collection of all available requests for application: 
[https://www.postman.com/akalosha/workspace/orders/collection/1650428-066d9cf5-234d-4d6f-acfe-eb696ad5fa9c?action=share&creator=1650428](https://www.postman.com/akalosha/workspace/orders/collection/1650428-066d9cf5-234d-4d6f-acfe-eb696ad5fa9c?action=share&creator=1650428) 

If server is using HTTPS, then go to Postman Settings and turn off the flag "SSL certificate verification", otherwise you will not 
be able to interact with endpoints.

## Design decisions / random thoughts ##
* I have done most of the development in master branch. This is fine because:
  * I am the only developer on the project.
  * I did not have to switch between branches, i.e. I could work on one feature until it's done without the need to switch
  to other branches.
  * I do not need to have release branches.
That was the case with me - I could just pick a feature, work on it until it is done and commit my changes to master. 
Under those conditions using industry standard branching strategies would not give any benefits; even git log looks the same (flat) 
when committing directly to master and when using develop/feature branches. 
I had created feature branches on number of occasions though. I've done that when I was working on a large feature and 
I needed to do some experimentation and I anticipated that I may need to revert my changes and start over. That happened 
with authentication feature, for example, - you can see the abandoned branch 'feature/jwt-custom-auth' because I 
realized that was a wrong way to follow and switched over to OAuth.
* Index initializers running on app startup are for convenience of development only. In production, they should be run only on demand.
* Admin APIs, such as getAllOrders, can be moved to separate controllers (like OrderAdminRestController) to not mix them with regular APIs.
* There are many features that are not implemented because I deemed them unnecessary for a sandbox-type project. 
Examples of such features are: localization, billing address support, faceted search, convenience stuff like favorites, 
saved addresses, saved payment methods etc.

### Custom exceptions and mapping exceptions to HTTP status codes ###
Application uses two custom exception classes, each with specific role:
1. `NotFoundException` is intended to be thrown when we expect to fetch some entity, but it can not be found.
2. `ServerException` is intended to be a wrapper for any unexpected and unrecoverable exception (think InterruptedException or RuntimeException).

Mapping of exceptions to HTTP status codes is done by `RestExceptionHandler`. It uses following mapping rules:
* `NotFoundException` is mapped to HTTP 404 Not Found.
* `IllegalArgumentException` is mapped to HTTP 400 Bad Request.
* Any other exception (including `ServerException`) are mapped to HTTP 500 Server Error. 
* Logic behind trowing NotFoundException vs other exception type:
```
  * If it is a service: 
    * If we are performing read operation (getting something from storage):
      * If a contract for a read operation is "get this thing but it is ok if nothing is found"
        * If entity is not found or it is found but failed some validations, then do not throw exceptions, but return an empty Optional.
      * If a contract for a read operation is "get this thing and failure is not an option"
        * If entity is not found or it is found but failed some validations, throw NotFoundException.
    * If we are performing something other then read, throw NotFoundException whenever anything essential is not found.
  * If it is a controller:
    * For read operations, if service returns empty Optional, just pass it to ResponseBuilder as it.
  * For other scenarios there are no specific rules and decision should be made on case-by-case basis.
```
* Be careful with "sort" query parameter. You can sort only by fields that have a "keyword" type in Elastic. Sorting by 
"text" type field will result in error response fom Elastic.
* When I finalized DB schema, I cleaned up Liquibase changelog XML by gathering the changes related to one table from multiple 
changesets and merging them to single one. Doing that would not be possible with live DB as you can't change the changesets
 that are already deployed (want to change something - add new changeset, but don't touch the old ones) But during development 
 the DB is recreated every time docker image is started, so refactoring changelog like that is perfectly fine.

## TODO ##
* ~~Add proper DTO model for responses~~
* ~~Add services for all operations~~
* ~~Redesign Elastic schema, DTO classes and make getAll methods return indexed data~~
* ~~Pagination~~
* ~~Redesign order-item API~~
* ~~Proper transaction handling~~
* ~~Proper exception handling and mapping~~
* ~~Add Swagger annotations to all controllers, review all controller annotations, review generated Swagger docs~~
* ~~More unit tests~~
* ~~Integration tests~~
* ~~Token-based authorization~~
  * ~~Do not require basic auth for /oauth/token~~ (not implemented, that would violate the OAuth 2 server contract defined in RFC 6749)
  * ~~Encode passwords~~
* ~~Add @Secured annotations to all controller methods~~
* ~~Owner validation for cart/order/user endpoints~~
* ~~Add order history endpoint~~
* ~~Argument validation~~
* ~~User controller~~
* ~~Merge Liquibase changesets, use descriptive changeset IDs~~
* ~~Javadocs~~
* ~~HTTPS~~ (HTTP is used by default, but HTTPS can be enabled by setting a 'server.ssl.enabled' config property to true)
* ~~Remove admin user and admin APIs like getAllUsers~~ (won't do, they have their uses)
* ~~Add getCurrentUser endpoint~~
* ~~Cart and order endpoints should have user/{id} in URL. Like /users/1/carts/2~~
* ~~Uniform naming for cart/order entries~~
* ~~Review usage of NotFoundException vs returning empty Optional. Consider not throwing any NotFoundExceptions from services;
 instead logging a warning and returning empty optional when something is not found~~ (done + added thoughts on NotFoundException usage)
* ~~Review working with dates~~
* ~~Check that all mappers delegate mapping of nested beans to other mappers instead of redefining the mapping~~
* Sites support requires catalog support as well. Implement catalogs.  
* ~~Review DB schema and add some indexes as needed~~
* Review server startup logs and check if there are any warnings
* ~~Review the default logging format~~
* ~~Add proper logging~~
* Check TODOs and FIXMEs.
* Add verify() to tests where it is appropriate
* Add a way to turn off indexing of all entities on application startup. Add API to trigger indexing in runtime.
* Upgrade all libs to the latest versions
* Update the readme
* Add license file
* Upload the project to GitHub