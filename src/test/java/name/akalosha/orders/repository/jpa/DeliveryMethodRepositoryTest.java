package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.TestApp;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.test.util.EntityFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApp.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
@ActiveProfiles("integration-test")
public class DeliveryMethodRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    @Test
    public void testFindBySite() {
        Site site = entityManager.persist(EntityFactory.createRandomSite());
        final int numOfDeliveryMethods = 3;
        for (int i = 0; i < numOfDeliveryMethods; i++) {
            entityManager.persist(EntityFactory.createRandomDeliveryMethod(site));
        }

        List<DeliveryMethod> deliveryMethods = deliveryMethodRepository.findBySite(site);
        Assert.assertTrue(CollectionUtils.isNotEmpty(deliveryMethods));
        Assert.assertEquals(numOfDeliveryMethods, deliveryMethods.size());
        deliveryMethods.forEach(dm -> Assert.assertEquals(dm.getSite(), site));
    }

    @Test
    public void testFindBySiteNoResult() {
        Site site1 = entityManager.persist(EntityFactory.createRandomSite());
        final int numOfDeliveryMethods = 3;
        for (int i = 0; i < numOfDeliveryMethods; i++) {
            entityManager.persist(EntityFactory.createRandomDeliveryMethod(site1));
        }

        Site site2 = entityManager.persist(EntityFactory.createRandomSite());

        List<DeliveryMethod> deliveryMethods = deliveryMethodRepository.findBySite(site2);
        Assert.assertTrue(CollectionUtils.isEmpty(deliveryMethods));
    }

    @Test
    public void testFindByName() {
        Site site = entityManager.persist(EntityFactory.createRandomSite());
        String deliveryMethodName1 = "test1";
        String deliveryMethodName2 = "test2";
        entityManager.persist(EntityFactory.createDeliveryMethod(site, deliveryMethodName1));
        entityManager.persist(EntityFactory.createDeliveryMethod(site, deliveryMethodName2));

        Optional<DeliveryMethod> deliveryMethodOptional = deliveryMethodRepository.findByName(deliveryMethodName1);
        Assert.assertTrue(deliveryMethodOptional.isPresent());
        Assert.assertEquals(deliveryMethodName1, deliveryMethodOptional.get().getName());
    }

    @Test
    public void testFindByNameNoResult() {
        Site site = entityManager.persist(EntityFactory.createRandomSite());
        String deliveryMethodName1 = "test1";
        String deliveryMethodName2 = "test2";
        entityManager.persist(EntityFactory.createDeliveryMethod(site, deliveryMethodName1));
        entityManager.persist(EntityFactory.createDeliveryMethod(site, deliveryMethodName2));

        Optional<DeliveryMethod> deliveryMethodOptional = deliveryMethodRepository.findByName("test3");
        Assert.assertTrue(deliveryMethodOptional.isEmpty());
    }

}
