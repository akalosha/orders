package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.TestApp;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.test.util.EntityFactory;
import name.akalosha.orders.util.DateTimeUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApp.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
@ActiveProfiles("integration-test")
public class OrderRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testFindByDatePlacedAfter() {
        User user = EntityFactory.createUser();
        user = entityManager.persist(user);

        Site site = EntityFactory.createRandomSite();
        site = entityManager.persist(site);

        Order order = createOrder(site, user);
        order.setDatePlaced(DateTimeUtil.toTimestamp(ZonedDateTime.now()));
        entityManager.persist(order);

        order = createOrder(site, user);
        order.setDatePlaced(DateTimeUtil.toTimestamp(ZonedDateTime.now().minusHours(1)));
        entityManager.persist(order);

        order = createOrder(site, user);
        order.setDatePlaced(DateTimeUtil.toTimestamp(ZonedDateTime.now().minusHours(3)));
        entityManager.persist(order);

        List<Order> orders = orderRepository.findByDatePlacedAfter(DateTimeUtil.toTimestamp(ZonedDateTime.now().minusHours(2)));
        Assert.assertNotNull(orders);
        Assert.assertFalse(orders.isEmpty());
        Assert.assertEquals(2, orders.size());
    }

    private Order createOrder(Site site, User user) {
        Order order = EntityFactory.createOrder(null, site, user);
        // we need to manually persist only delivery method - shipping address will be persisted by cascade
        order.setDeliveryMethod(entityManager.persist(order.getDeliveryMethod()));
        return order;
    }

    @Test
    public void testFindByDatePlacedAfterEmptyRepo() {
        List<Order> orders = orderRepository.findByDatePlacedAfter(DateTimeUtil.toTimestamp(ZonedDateTime.now().minusHours(2)));
        Assert.assertNotNull(orders);
        Assert.assertTrue(orders.isEmpty());
    }

}
