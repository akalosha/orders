package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.TestApp;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.test.util.EntityFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApp.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
@ActiveProfiles("integration-test")
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindByUserName() {
        String userName = "test";
        User user = EntityFactory.createUser(userName);
        entityManager.persist(user);

        Optional<User> userOptional = userRepository.findByUsername(userName);
        Assert.assertNotNull(userOptional);
        Assert.assertTrue(userOptional.isPresent());
        Assert.assertEquals(userName, userOptional.get().getUsername());
    }

    @Test
    public void testFindByUserNameNoResult() {
        String userName = "test";
        User user = EntityFactory.createUser(userName);
        entityManager.persist(user);

        Optional<User> userOptional = userRepository.findByUsername(userName + "1");
        Assert.assertNotNull(userOptional);
        Assert.assertTrue(userOptional.isEmpty());
    }

}
