package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.TestApp;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.test.util.EntityFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApp.class)
@DataJpaTest
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
@ActiveProfiles("integration-test")
public class SiteRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SiteRepository siteRepository;

    @Test
    public void testFindSiteByName() {
        String siteName = "site1";
        entityManager.persist(EntityFactory.createSite(siteName));

        Optional<Site> siteOptional = siteRepository.findSiteByName(siteName);
        Assert.assertTrue(siteOptional.isPresent());
        Assert.assertEquals(siteName, siteOptional.get().getName());
    }

    @Test
    public void testFindSiteByNameNoResult() {
        String siteName = "site1";
        entityManager.persist(EntityFactory.createSite(siteName));

        Optional<Site> siteOptional = siteRepository.findSiteByName("site42");
        Assert.assertTrue(siteOptional.isEmpty());
    }

}
