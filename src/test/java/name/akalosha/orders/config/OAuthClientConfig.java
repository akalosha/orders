package name.akalosha.orders.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import java.util.Collections;
import java.util.List;

/**
 * A client that can communicate with OAuth server (for example, for getting an access token).
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@EnableOAuth2Client
@Configuration
public class OAuthClientConfig {

    public static final String ADMIN_USER = "admin";
    // Admin user is needed only for testing in dev/CI environments. It must not be present in prod environment, so
    // keeping its credentials here in the code is fine.
    private static final String ADMIN_PASSWORD = "adminpass";

    public static final String NON_ADMIN_USER = "user";
    public static final String NON_ADMIN_USER_PASSWORD = "userpass";

    public class ClientBuilder {

        private static final String ADMIN_USER = "admin";
        private static final String ADMIN_PASSWORD = "adminpass";

        private String oauthServerHost;
        private int port;
        private String tokenPath;
        private String clientId;
        private String clientSecret;
        private String grantType;
        private List<String> scopes;
        private String username;
        private String password;

        public ClientBuilder() {
            this.oauthServerHost = OAuthClientConfig.this.oauthServerHost;
            this.port = OAuthClientConfig.this.port;
            this.tokenPath = OAuthClientConfig.this.tokenPath;
            this.clientId = OAuthClientConfig.this.clientId;
            this.clientSecret = OAuthClientConfig.this.clientSecret;
            this.grantType = "password";
            this.scopes = Collections.singletonList("any");
        }

        public ClientBuilder setOauthServerHost(String oauthServerHost) {
            this.oauthServerHost = oauthServerHost;
            return this;
        }

        public ClientBuilder setPort(int port) {
            this.port = port;
            return this;
        }

        public ClientBuilder setTokenPath(String tokenPath) {
            this.tokenPath = tokenPath;
            return this;
        }

        public ClientBuilder setClientId(String clientId) {
            this.clientId = clientId;
            return this;
        }

        public ClientBuilder setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
            return this;
        }

        public ClientBuilder setGrantType(String grantType) {
            this.grantType = grantType;
            return this;
        }

        public ClientBuilder setScopes(List<String> scopes) {
            this.scopes = scopes;
            return this;
        }

        public ClientBuilder setUsername(String username) {
            this.username = username;
            return this;
        }

        public ClientBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public ClientBuilder withAdminCredentials() {
            this.username = ADMIN_USER;
            this.password = ADMIN_PASSWORD;
            return this;
        }

        public OAuth2RestOperations build() {
            ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();
            resource.setAccessTokenUri(String.format("%s:%s%s", oauthServerHost, port, tokenPath));
            resource.setClientId(clientId);
            resource.setClientSecret(clientSecret);
            resource.setGrantType(grantType);
            resource.setScope(scopes);
            resource.setUsername(username);
            resource.setPassword(password);

            AccessTokenRequest request = new DefaultAccessTokenRequest();
            DefaultOAuth2ClientContext context = new DefaultOAuth2ClientContext(request);
            return new OAuth2RestTemplate(resource, context);
        }

    }

    @Value("${oauth.server.host:http://localhost}")
    private String oauthServerHost;

    @Value("${server.port:8080}")
    private int port;

    @Value("${oauth.token.path:/oauth/token}")
    private String tokenPath;

    @Value("${security.oauth2.client.client-id}")
    private String clientId;

    @Value("${security.oauth2.client.client-secret}")
    private String clientSecret;

    @Bean
    public ClientBuilder getAdminBuilder() {
        return new ClientBuilder().withAdminCredentials();
    }

    public OAuth2RestOperations getAdminClient() {
        return new ClientBuilder().withAdminCredentials().build();
    }

    @Bean
    public OAuth2RestOperations getOAuth2RestOperations(@Autowired ClientBuilder clientBuilder) {
        return clientBuilder.build();
    }

}
