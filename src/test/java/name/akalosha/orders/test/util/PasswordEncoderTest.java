package name.akalosha.orders.test.util;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Ignore
public class PasswordEncoderTest {

    // quick and dirty test to check how much time the password encoding takes
    @Test
    public void test() {
        int saltLength = 16;
        int hashLength = 32;
        int parallelism = 4; // parallelism parameter has no effect in current Spring implementation
        int memory = (int) Math.pow(2, 14); // 16 MiB
        int iterations = 35;
        Argon2PasswordEncoder encoder = new Argon2PasswordEncoder(saltLength, hashLength, parallelism, memory, iterations);

        int runs = 20;
        List<String> result = new ArrayList<>(runs);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for (int i = 0; i < runs; i++) {
            result.add(encoder.encode("pass"));
        }
        stopWatch.stop();
        System.out.println(result);
        System.out.println("Time: " + ((double) stopWatch.getNanoTime()) / runs / 1000000 + "ms");
    }

}
