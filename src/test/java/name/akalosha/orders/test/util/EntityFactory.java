package name.akalosha.orders.test.util;

import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.service.impl.UserServiceImpl;
import name.akalosha.orders.util.DateTimeUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.time.ZonedDateTime;
import java.util.Collections;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class EntityFactory {

    private EntityFactory() {
        throw new UnsupportedOperationException();
    }

    public static User createUser() {
        return createUser("test");
    }

    public static User createUser(String userName) {
        User user = new User();
        user.setUsername(userName);
        user.setPassword("test");
        user.setRoles(UserServiceImpl.DEFAULT_ROLES);
        return user;
    }

    public static Order createOrder(Long orderId, Site site, User user) {
        Order order = new Order();
        order.setId(orderId);
        order.setDatePlaced(DateTimeUtil.toTimestamp(ZonedDateTime.now()));
        order.setSite(site);
        order.setUser(user);
        order.setShippingAddress(createRandomAddress());
        order.setDeliveryMethod(createRandomDeliveryMethod(site));
        return order;
    }

    public static AddressForm createRandomAddressForm() {
        AddressForm addressForm = new AddressForm();
        addressForm.setCity(TestUtils.getRandomString());
        addressForm.setLine1(TestUtils.getRandomString());
        addressForm.setPhone(RandomStringUtils.randomNumeric(7, 9));

        return addressForm;
    }

    public static Address createRandomAddress() {
        Address address = new Address();
        address.setCity(TestUtils.getRandomString());
        address.setLine1(TestUtils.getRandomString());
        address.setPhone(RandomStringUtils.randomNumeric(7, 9));

        return address;
    }

    public static Site createSite(String siteName) {
        Site site = new Site();
        site.setName(siteName);
        site.setUrlPatterns(Collections.singletonList("whatever"));
        return site;
    }

    public static Site createRandomSite() {
        return createSite(RandomStringUtils.randomAlphabetic(5));
    }

    public static DeliveryMethod createDeliveryMethod(Site site, String deliveryMethodName) {
        DeliveryMethod deliveryMethod = new DeliveryMethod();
        deliveryMethod.setName(deliveryMethodName);
        deliveryMethod.setHoursToDeliver(RandomUtils.nextInt());
        deliveryMethod.setSite(site);
        return deliveryMethod;
    }

    public static DeliveryMethod createRandomDeliveryMethod(Site site) {
        return createDeliveryMethod(site, RandomStringUtils.randomAlphabetic(5));
    }

    public static Cart createEmptyCart(Site site, User user) {
        Cart cart = new Cart();
        cart.setSite(site);
        cart.setUser(user);
        return cart;
    }

}
