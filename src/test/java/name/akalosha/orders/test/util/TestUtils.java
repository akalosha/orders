package name.akalosha.orders.test.util;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Random;
import java.util.UUID;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class TestUtils {

    private static final int RANDOM_STRING_LENGTH = 12;

    public static <T> T getRandomElement(Collection<T> collection) {
        Assert.isTrue(CollectionUtils.isNotEmpty(collection), "Collection should not be empty");
        return IterableUtils.get(collection, new Random().nextInt(collection.size()));
    }

    public static String getRandomString() {
        return RandomStringUtils.randomAlphabetic(RANDOM_STRING_LENGTH);
    }

}
