package name.akalosha.orders.rest.controller;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.CartDTO;
import name.akalosha.orders.model.dto.rest.CartOrderEntryDTO;
import name.akalosha.orders.model.dto.rest.OrderDTO;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.repository.elastic.AddressSearchRepository;
import name.akalosha.orders.repository.jpa.AddressRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import name.akalosha.orders.repository.jpa.SiteRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import name.akalosha.orders.service.CartOrderEntryService;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.service.SiteService;
import name.akalosha.orders.test.util.EntityFactory;
import name.akalosha.orders.test.util.TestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * Integration test for {@link CartRestController}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class CartControllerTest implements CartOrderTest {

    public static final int PAGE_SIZE = 10;
    public static final String SORT_PROPERTY = "id";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private AddressSearchRepository addressSearchRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private CartOrderEntryService cartOrderEntryService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private SiteService siteService;


    private void initOAuthClientWithAdminCredentials() {
        oauthClient = getClientBuilderWithAdminCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithAdminCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    @Before
    public void initOAuthClientWithUserCredentials() {
        oauthClient = getClientBuilderWithUserCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithUserCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .setUsername(OAuthClientConfig.NON_ADMIN_USER)
                .setPassword(OAuthClientConfig.NON_ADMIN_USER_PASSWORD)
                .setPort(port);
    }

    @Before
    public void ensureThatWeHaveSomeCarts() {
        if (!existsValidCart()) {
            addDummyCartNoAddress(OAuthClientConfig.NON_ADMIN_USER);
        }
        Assert.assertTrue(existsValidCart());
    }

    private boolean existsValidCart() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Cart> cartsPage = cartRepository.findAll(pageRequest);
        if (!cartsPage.isEmpty()) {
            return cartsPage.stream()
                    .map(Cart::getId)
                    .map(cartService::findWithEagerFetch)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .anyMatch(this::isValidCartForTesting);
        }
        return false;
    }

    private boolean isValidCartForTesting(Cart cart) {
        return cart != null &&
                cart.getId() != null &&
                cart.getUser() != null &&
                CollectionUtils.isNotEmpty(cart.getEntries()) &&
                cart.getEntries().stream().anyMatch(this::isValidEntry);
    }

    private boolean isValidEntry(CartOrderEntry entry) {
        return entry != null &&
                entry.getId() != null &&
                entry.getQuantity() > 0 &&
                entry.getProduct() != null &&
                entry.getProduct().getId() != null &&
                entry.getProduct().getPrice() != null;
    }

    private Cart addDummyCart(String cartUserName) {
        Cart cart = addDummyCartNoAddress(cartUserName);
        addRandomShippingAddressToCart(cart.getId());
        addDefaultDeliveryMethodToCart(cart.getId());

        Optional<Cart> cartOptional = cartService.findWithEagerFetch(cart.getId());
        Assert.assertTrue(cartOptional.isPresent());
        Assert.assertEquals(2, cartOptional.get().getEntries().size());

        return cartOptional.get();
    }

    private Cart addDummyCartNoAddress(String cartUserName) {
        Cart cart = createEmptyCart(cartUserName);

        Product product1 = getRandomProduct();
        CartOrderEntry entry1 = new CartOrderEntry();
        entry1.setProduct(product1);
        entry1.setQuantity(2);
        cartOrderEntryService.save(cart.getId(), entry1);

        Product product2 = getProductOtherThan(product1);
        CartOrderEntry entry2 = new CartOrderEntry();
        entry2.setProduct(product2);
        entry2.setQuantity(3);
        cartOrderEntryService.save(cart.getId(), entry2);

        Optional<Cart> cartOptional = cartService.findWithEagerFetch(cart.getId());
        Assert.assertTrue(cartOptional.isPresent());
        Assert.assertEquals(2, cartOptional.get().getEntries().size());

        return cartOptional.get();
    }

    /**
     * First we get a cart from DB, and then we check that we can get the same cart via REST.
     */
    @Test
    public void testGetCart() {
        Cart dbCart = getRandomCart();

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}",
                HttpMethod.GET,
                httpEntity,
                CartDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        CartDTO restCart = response.getBody();
        Assert.assertNotNull(restCart);

        Assert.assertNotNull(restCart.getId());
        Assert.assertEquals(dbCart.getId(), restCart.getId());
        Assert.assertNotNull(restCart.getUser());
        Assert.assertEquals(dbCart.getUser().getUsername(), restCart.getUser());
        Assert.assertEquals(dbCart.getTotal(), restCart.getTotal());
        Assert.assertNotNull(restCart.getEntries());
        Assert.assertEquals(dbCart.getEntries().size(), restCart.getEntries().size());

        for (int i = 0; i < restCart.getEntries().size(); i++) {
            CartOrderEntryDTO restEntry = restCart.getEntries().get(i);
            CartOrderEntry dbEntry = dbCart.getEntries().get(i);
            Assert.assertNotNull(restEntry);
            Assert.assertEquals(dbEntry.getId(), restEntry.getId());
            Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
            Assert.assertNotNull(restEntry.getProduct());
            Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
            Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());
        }
    }

    /**
     * First we get a cart from DB, and then we try to get the same cart via REST, but we would be authenticating as a
     * user that doesn't own the cart. This should fail.
     */
    @Test
    public void testGetCartWithOtherUser() {
        // This test works only for two predefined test users. If the name of some third user would be passed as an
        // argument to testGetCartWithOtherUser(), it will fail because we need to authenticate with username and
        // password and we know only test users' passwords.
        testGetCartWithOtherUser(OAuthClientConfig.ADMIN_USER, OAuthClientConfig.NON_ADMIN_USER);
        // checking the reverse scenario as well
        testGetCartWithOtherUser(OAuthClientConfig.NON_ADMIN_USER, OAuthClientConfig.ADMIN_USER);
    }

    private void testGetCartWithOtherUser(String cartOwnerUsername, String otherUsername) {
        Cart cart = addDummyCart(cartOwnerUsername);

        User otherUser = getUser(otherUsername);
        initOAuthClientForUser(otherUser.getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}",
                HttpMethod.GET,
                httpEntity,
                CartDTO.class,
                otherUser.getId(),
                cart.getId()
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        cartService.delete(cart.getId());
    }

    /**
     * First we get a list of carts from DB, then we get carts from REST (using the same pagination settings)
     * and finally we are checking that both lists contain the same elements.
     */
    @Test
    public void testGetAllCarts() {
        PageRequest pageRequest = createDefaultSortedPageRequest();
        Page<Cart> cartsPage = cartRepository.findAll(pageRequest);
        Assert.assertFalse(cartsPage.isEmpty());

        // we are testing admin-only endpoint
        initOAuthClientWithAdminCredentials();

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<CartDTO>> response = restTemplate.exchange(
                "/carts?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<CartDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        PageDTO<CartDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);

        List<Cart> dbCarts = cartsPage.getContent();
        List<CartDTO> restCarts = restResult.getContent();
        Assert.assertNotNull(restCarts);
        Assert.assertEquals(dbCarts.size(), restCarts.size());

        // checking that we got the same carts from DB and from REST API, in the same order
        for (int i = 0; i < dbCarts.size(); i++) {
            Optional<Cart> dbCartOptional = cartRepository.findByIdWithEagerFetch(dbCarts.get(i).getId());
            Assert.assertTrue(dbCartOptional.isPresent());

            Cart dbCart = dbCartOptional.get();
            CartDTO restCart = restCarts.get(i);
            Assert.assertNotNull(restCart);
            Assert.assertEquals(dbCart.getId(), restCart.getId());
            Assert.assertEquals(dbCart.getUser().getUsername(), restCart.getUser());
            Assert.assertEquals(dbCart.getTotal(), restCart.getTotal());
            Assert.assertNotNull(restCart.getEntries());

            List<CartOrderEntry> dbEntries = dbCart.getEntries();
            List<CartOrderEntryDTO> restEntries = restCart.getEntries();
            // checking that we got the same cart entries from DB and from REST API, in the same order
            for (int j = 0; j < dbEntries.size(); j++) {
                CartOrderEntry dbEntry = dbEntries.get(j);
                CartOrderEntryDTO restEntry = restEntries.get(j);
                Assert.assertNotNull(restEntry);
                Assert.assertEquals(dbEntry.getId(), restEntry.getId());
                Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
                Assert.assertNotNull(restEntry.getProduct());
                Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
                Assert.assertEquals(dbEntry.getProduct().getName(), restEntry.getProduct().getName());
                Assert.assertEquals(dbEntry.getProduct().getSku(), restEntry.getProduct().getSku());
                Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());
            }
        }
    }

    /**
     * /carts endpoint is admin-only, so checking that a regular user can not access it.
     */
    @Test
    public void testGetAllCartsWrongUser() {
        initOAuthClientWithUserCredentials();

        PageRequest pageRequest = createDefaultSortedPageRequest();
        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<CartDTO>> response = restTemplate.exchange(
                "/carts?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<CartDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * We are creating a new cart via REST and checking that the cart was created in DB.
     */
    @Test
    public void testAddCart() {
        User user = getUser(OAuthClientConfig.NON_ADMIN_USER);
        initOAuthClientForUser(user.getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartDTO> response = restTemplate.exchange(
                "/users/{userId}/carts",
                HttpMethod.POST,
                httpEntity,
                CartDTO.class,
                user.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        CartDTO restCart = response.getBody();
        Assert.assertNotNull(restCart.getId());
        Assert.assertNotNull(restCart.getTotal());
        Assert.assertTrue(CollectionUtils.isEmpty(restCart.getEntries()));
        Assert.assertTrue(StringUtils.isNotBlank(restCart.getUser()));

        Optional<Cart> dbCartOptional = cartRepository.findByIdWithEagerFetch(restCart.getId());
        Assert.assertTrue(dbCartOptional.isPresent());
        Cart dbCart = dbCartOptional.get();
        Assert.assertNotNull(dbCart.getUser());
        Assert.assertEquals(dbCart.getUser().getUsername(), restCart.getUser());
        Assert.assertTrue(CollectionUtils.isEmpty(dbCart.getEntries()));

        cartService.delete(dbCart.getId());
    }

    /**
     * Placing an order via REST and checking that the REST endpoint returned the same order data that is present in DB.
     * Also checking that the cart that was used to place an order is no longer present in DB.
     */
    @Test
    public void testPlaceOrder() {
        Cart cart = addDummyCart(OAuthClientConfig.NON_ADMIN_USER);

        initOAuthClientForUser(cart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<OrderDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}/place-order",
                HttpMethod.POST,
                httpEntity,
                OrderDTO.class,
                cart.getUser().getId(),
                cart.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        OrderDTO restOrder = response.getBody();
        Assert.assertNotNull(restOrder.getId());
        Assert.assertNotNull(restOrder.getTotal());
        Assert.assertNotNull(restOrder.getDatePlaced());
        Assert.assertTrue(StringUtils.isNotBlank(restOrder.getUser()));
        Assert.assertTrue(CollectionUtils.isNotEmpty(restOrder.getEntries()));

        Optional<Order> dbOrderOptional = orderService.findWithEagerFetch(restOrder.getId());
        Assert.assertTrue(dbOrderOptional.isPresent());
        Order dbOrder = dbOrderOptional.get();
        Assert.assertEquals(dbOrder.getTotal(), restOrder.getTotal());
        Assert.assertEquals(dbOrder.getDatePlaced(), restOrder.getDatePlaced());
        Assert.assertEquals(dbOrder.getUser().getUsername(), restOrder.getUser());
        Assert.assertEquals(dbOrder.getEntries().size(), restOrder.getEntries().size());

        List<CartOrderEntry> dbEntries = dbOrder.getEntries();
        List<CartOrderEntryDTO> restEntries = restOrder.getEntries();
        // checking that we got the same order entries from DB and from REST API, in the same order
        for (int i = 0; i < dbEntries.size(); i++) {
            CartOrderEntry dbEntry = dbEntries.get(i);
            CartOrderEntryDTO restEntry = restEntries.get(i);
            Assert.assertEquals(dbEntry.getId(), restEntry.getId());
            Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
            Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
            Assert.assertEquals(dbEntry.getProduct().getSku(), restEntry.getProduct().getSku());
            Assert.assertEquals(dbEntry.getProduct().getName(), restEntry.getProduct().getName());
            Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());
        }

        Optional<Cart> dbCartOptional = cartService.find(cart.getId());
        Assert.assertTrue(dbCartOptional.isEmpty());

        orderService.delete(dbOrder.getId());
    }

    /**
     * Using an empty cart to place an order. This should fail.
     */
    @Test
    public void testPlaceOrderForEmptyCart() {
        Cart cart = createEmptyCart(OAuthClientConfig.NON_ADMIN_USER);
        User user = cart.getUser();

        initOAuthClientForUser(user.getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<OrderDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}/place-order",
                HttpMethod.POST,
                httpEntity,
                OrderDTO.class,
                user.getId(),
                cart.getId()
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        cartService.delete(cart.getId());
    }

    @Test
    public void testSetShippingAddress() {
        Cart dbCart = addDummyCartNoAddress(OAuthClientConfig.NON_ADMIN_USER);
        // for this scenario cart should not have any shipping address set
        Assert.assertNull(dbCart.getShippingAddress());

        initOAuthClientForUser(dbCart.getUser().getUsername());

        AddressForm addressForm = EntityFactory.createRandomAddressForm();

        ResponseEntity<CartDTO> response = makeSetShippingAddressRequest(dbCart, addressForm);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        CartDTO restCart = response.getBody();
        Assert.assertNotNull(restCart.getId());
        Assert.assertEquals(dbCart.getId(), restCart.getId());
        Assert.assertNotNull(restCart.getShippingAddress());
        // checking that in a REST response we have the same address data that was provided in a request
        Assert.assertEquals(addressForm.getCity(), restCart.getShippingAddress().getCity());
        Assert.assertEquals(addressForm.getLine1(), restCart.getShippingAddress().getLine1());
        Assert.assertEquals(addressForm.getPhone(), restCart.getShippingAddress().getPhone());

        Optional<Cart> dbCartOptional = cartService.findWithEagerFetch(restCart.getId());
        Assert.assertTrue(dbCartOptional.isPresent());
        dbCart = dbCartOptional.get();
        // checking that REST response contains the same address data that we have in DB
        Assert.assertEquals(dbCart.getShippingAddress().getCity(), restCart.getShippingAddress().getCity());
        Assert.assertEquals(dbCart.getShippingAddress().getLine1(), restCart.getShippingAddress().getLine1());
        Assert.assertEquals(dbCart.getShippingAddress().getPhone(), restCart.getShippingAddress().getPhone());

        dbCart.setShippingAddress(null);
        cartService.delete(dbCart.getId());
    }

    @Test
    public void testReplaceShippingAddress() {
        Cart dbCart = addDummyCart(OAuthClientConfig.NON_ADMIN_USER);
        // for this scenario cart should have some shipping address set
        Assert.assertNotNull(dbCart.getShippingAddress());

        initOAuthClientForUser(dbCart.getUser().getUsername());

        AddressForm replacementAddressForm = EntityFactory.createRandomAddressForm();
        ResponseEntity<CartDTO> response = makeSetShippingAddressRequest(dbCart, replacementAddressForm);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        CartDTO restCart = response.getBody();
        Assert.assertNotNull(restCart.getId());
        Assert.assertEquals(dbCart.getId(), restCart.getId());
        Assert.assertNotNull(restCart.getShippingAddress());
        // checking that in a REST response we have the same address data that was provided in a request
        Assert.assertEquals(replacementAddressForm.getCity(), restCart.getShippingAddress().getCity());
        Assert.assertEquals(replacementAddressForm.getLine1(), restCart.getShippingAddress().getLine1());
        Assert.assertEquals(replacementAddressForm.getPhone(), restCart.getShippingAddress().getPhone());

        Optional<Cart> dbCartOptional = cartService.findWithEagerFetch(restCart.getId());
        Assert.assertTrue(dbCartOptional.isPresent());
        dbCart = dbCartOptional.get();
        // checking that REST response contains the same address data that we have in DB
        Assert.assertEquals(dbCart.getShippingAddress().getCity(), restCart.getShippingAddress().getCity());
        Assert.assertEquals(dbCart.getShippingAddress().getLine1(), restCart.getShippingAddress().getLine1());
        Assert.assertEquals(dbCart.getShippingAddress().getPhone(), restCart.getShippingAddress().getPhone());

        cartService.delete(dbCart.getId());
    }

    @Test
    public void testSetShippingAddressInvalidAddress() {
        Cart dbCart = getRandomCart();

        // empty "city" field scenario
        AddressForm addressForm = EntityFactory.createRandomAddressForm();
        addressForm.setCity(null);
        ResponseEntity<?> response = makeSetShippingAddressRequest(dbCart, addressForm);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        // empty "line1" field scenario
        addressForm = EntityFactory.createRandomAddressForm();
        addressForm.setLine1(null);
        response = makeSetShippingAddressRequest(dbCart, addressForm);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        // empty "phone" field scenario
        addressForm = EntityFactory.createRandomAddressForm();
        addressForm.setPhone(null);
        response = makeSetShippingAddressRequest(dbCart, addressForm);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        // invalid format of "phone" field scenario
        addressForm = EntityFactory.createRandomAddressForm();
        addressForm.setPhone("notAValidPhone");
        response = makeSetShippingAddressRequest(dbCart, addressForm);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    private ResponseEntity<CartDTO> makeSetShippingAddressRequest(Cart cart, AddressForm addressForm) {
        initOAuthClientForUser(cart.getUser().getUsername());

        HttpEntity<AddressForm> httpEntity = createHttpEntity(addressForm);
        return restTemplate.exchange(
                "/users/{userId}/carts/{id}/shipping-address",
                HttpMethod.POST,
                httpEntity,
                CartDTO.class,
                cart.getUser().getId(),
                cart.getId()
        );
    }

    @Test
    public void testSetDeliveryMethod() {
        // we will be using a cart with a shipping address but with no delivery method
        Cart dbCart = addDummyCartNoAddress(OAuthClientConfig.NON_ADMIN_USER);
        dbCart = addRandomShippingAddressToCart(dbCart.getId());
        Assert.assertNotNull(dbCart.getShippingAddress());
        Assert.assertNull(dbCart.getDeliveryMethod());

        DeliveryMethod deliveryMethod = getDefaultDeliveryMethod();

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}/delivery-method?deliveryMethodId={deliveryMethodId}",
                HttpMethod.POST,
                httpEntity,
                CartDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                deliveryMethod.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        CartDTO restCart = response.getBody();
        Assert.assertNotNull(restCart.getId());
        Assert.assertEquals(dbCart.getId(), restCart.getId());
        Assert.assertNotNull(restCart.getDeliveryMethod());
        // checking that in a REST response we have the same delivery mode data that we referenced in a request
        Assert.assertEquals(deliveryMethod.getId(), restCart.getDeliveryMethod().getId());
        Assert.assertEquals(deliveryMethod.getName(), restCart.getDeliveryMethod().getName());

        Optional<Cart> dbCartOptional = cartService.findWithEagerFetch(restCart.getId());
        Assert.assertTrue(dbCartOptional.isPresent());
        dbCart = dbCartOptional.get();
        // checking that REST response contains the same delivery mode data that we have in DB
        Assert.assertEquals(dbCart.getDeliveryMethod().getId(), restCart.getDeliveryMethod().getId());
        Assert.assertEquals(dbCart.getDeliveryMethod().getName(), restCart.getDeliveryMethod().getName());

        dbCart.setShippingAddress(null);
        cartService.delete(dbCart.getId());
    }

    @Test
    public void testSetNonExistingDeliveryMethod() {
        // we will be using a cart with a shipping address but with no delivery method
        Cart dbCart = addDummyCartNoAddress(OAuthClientConfig.NON_ADMIN_USER);
        dbCart = addRandomShippingAddressToCart(dbCart.getId());
        Assert.assertNotNull(dbCart.getShippingAddress());
        Assert.assertNull(dbCart.getDeliveryMethod());

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}/delivery-method?deliveryMethodId={deliveryMethodId}",
                HttpMethod.POST,
                httpEntity,
                CartDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                -1L
        );
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        dbCart.setShippingAddress(null);
        cartService.delete(dbCart.getId());
    }

    @Test
    public void testSetDeliveryMethodFromOtherSite() {
        // we will be using a cart with a shipping address but with no delivery method
        Cart dbCart = addDummyCartNoAddress(OAuthClientConfig.NON_ADMIN_USER);
        dbCart = addRandomShippingAddressToCart(dbCart.getId());
        Assert.assertNotNull(dbCart.getShippingAddress());
        Assert.assertNull(dbCart.getDeliveryMethod());

        Site otherSite = getSiteOtherThan(dbCart.getSite());
        // getting delivery method from the site other than cart's site
        List<DeliveryMethod> deliveryMethods = deliveryMethodRepository.findBySite(otherSite);
        Assert.assertTrue(CollectionUtils.isNotEmpty(deliveryMethods));
        Optional<DeliveryMethod> deliveryMethodOptional = deliveryMethods.stream().findAny();
        Assert.assertTrue(deliveryMethodOptional.isPresent());

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{id}/delivery-method?deliveryMethodId={deliveryMethodId}",
                HttpMethod.POST,
                httpEntity,
                CartDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                deliveryMethodOptional.get().getId()
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        dbCart.setShippingAddress(null);
        cartService.delete(dbCart.getId());
    }

    private PageRequest createDefaultPageRequest() {
        return PageRequest.of(0, PAGE_SIZE);
    }

    private PageRequest createDefaultSortedPageRequest() {
        return PageRequest.of(0, PAGE_SIZE, Sort.by(Sort.Direction.ASC, SORT_PROPERTY));
    }

    private <T> HttpEntity<T> createHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);

        return new HttpEntity<>(body, headers);
    }

    private void addAuthorizationHeader(HttpHeaders headers) {
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        headers.set(HttpHeaders.AUTHORIZATION, accessToken.getTokenType() + " " + accessToken.getValue());
    }

    private String toQueryParamString(Pageable pageable) {
        return String.format(
                "size=%s&page=%s&sort=%s",
                pageable.getPageSize(), pageable.getPageNumber(), pageable.getSort().toString().replaceAll(": ", ",")
        );
    }

    private Product getRandomProduct() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productsPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productsPage.isEmpty());

        Product dbProduct = TestUtils.getRandomElement(productsPage.getContent());
        Assert.assertNotNull(dbProduct.getId());

        return dbProduct;
    }

    private Product getProductOtherThan(Product productToAvoid) {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productPage.isEmpty());

        Optional<Product> productOptional = productPage.get()
                .filter(product -> !product.getId().equals(productToAvoid.getId()))
                .findAny();
        Assert.assertTrue(productOptional.isPresent());

        return productOptional.get();
    }

    private Cart getRandomCart() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Cart> cartPage = cartRepository.findAll(pageRequest);
        Assert.assertFalse(cartPage.isEmpty());

        Cart dbCart = TestUtils.getRandomElement(cartPage.getContent());
        Optional<Cart> cartOptional = cartService.findWithEagerFetch(dbCart.getId());
        Assert.assertTrue(cartOptional.isPresent());
        dbCart = cartOptional.get();
        Assert.assertNotNull(dbCart.getId());
        Assert.assertNotNull(dbCart.getUser());
        Assert.assertNotNull(dbCart.getUser().getId());

        return dbCart;
    }

    private boolean isAdminUser(String username) {
        Assert.assertNotNull(username);
        return username.equals(OAuthClientConfig.ADMIN_USER);
    }

    private boolean isRegularUser(String username) {
        Assert.assertNotNull(username);
        return username.equals(OAuthClientConfig.NON_ADMIN_USER);
    }

    private void initOAuthClientForUser(String username) {
        if (isAdminUser(username)) {
            initOAuthClientWithAdminCredentials();
        } else if (isRegularUser(username)) {
            initOAuthClientWithUserCredentials();
        } else {
            throw new IllegalStateException("Unable to authenticate unknown user " + username);
        }
    }

    private Site getSiteOtherThan(Site siteToAvoid) {
        List<Site> sites = siteRepository.findAll();
        Assert.assertTrue(CollectionUtils.isNotEmpty(sites));

        Optional<Site> siteOptional = sites.stream()
                .filter(site -> !site.getId().equals(siteToAvoid.getId()))
                .findAny();
        Assert.assertTrue(siteOptional.isPresent());

        return siteOptional.get();
    }

    @Override
    public DeliveryMethodRepository getDeliveryMethodRepository() {
        return deliveryMethodRepository;
    }

    @Override
    public CartService getCartService() {
        return cartService;
    }

    @Override
    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    public SiteService getSiteService() {
        return siteService;
    }

}
