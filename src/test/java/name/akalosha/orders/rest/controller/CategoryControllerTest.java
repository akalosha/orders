package name.akalosha.orders.rest.controller;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import name.akalosha.orders.converters.mappers.dto.CategoryDTOMapper;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.rest.CategoryDTO;
import name.akalosha.orders.model.dto.rest.ErrorDTO;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.repository.jpa.CategoryRepository;
import name.akalosha.orders.service.CategoryService;
import name.akalosha.orders.test.util.TestUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * Integration test for {@link CategoryRestController}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class CategoryControllerTest {

    public static final int PAGE_SIZE = 10;
    public static final String SORT_PROPERTY = "id";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryDTOMapper categoryDTOMapper;

    private void initOAuthClientWithAdminCredentials() {
        oauthClient = getClientBuilderWithAdminCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithAdminCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    @Before
    public void initOAuthClientWithUserCredentials() {
        oauthClient = getClientBuilderWithUserCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithUserCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .setUsername(OAuthClientConfig.NON_ADMIN_USER)
                .setPassword(OAuthClientConfig.NON_ADMIN_USER_PASSWORD)
                .setPort(port);
    }

    /**
     * First we get some random category from DB, and then we check that we can get the same category via REST.
     */
    @Test
    public void testGetCategory() {
        Category category = getRandomCategory();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CategoryDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.GET, httpEntity, CategoryDTO.class, category.getId());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        CategoryDTO returnedCategory = response.getBody();
        Assert.assertNotNull(returnedCategory);
        Assert.assertNotNull(returnedCategory.getId());
        Assert.assertEquals(category.getId(), returnedCategory.getId());
        Assert.assertNotNull(returnedCategory.getName());
        Assert.assertEquals(category.getName(), returnedCategory.getName());
    }

    /**
     * Checking that we are getting expected response when the category is not found.
     */
    @Test
    public void testGetNonExistingCategory() {
        long categoryId = getNonExistingCategoryId();
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.GET, httpEntity, ErrorDTO.class, categoryId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * First we get a list of categories from DB, then we get categories from REST (using the same pagination settings)
     * and finally we are checking that both lists contain the same elements.
     */
    @Test
    public void testGetAllCategories() {
        PageRequest dbPageRequest = PageRequest.of(0, PAGE_SIZE, Sort.by(Sort.Direction.ASC, SORT_PROPERTY));
        Page<Category> categoriesPage = categoryRepository.findAll(dbPageRequest);
        Assert.assertFalse(categoriesPage.isEmpty());

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        PageRequest searchPageRequest = PageRequest.of(0, PAGE_SIZE, Sort.by(Sort.Direction.ASC, SORT_PROPERTY));

        ResponseEntity<PageDTO<CategoryDTO>> response = restTemplate.exchange(
                "/categories?" + toQueryParamString(searchPageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<CategoryDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        PageDTO<CategoryDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);

        List<Category> dbCategories = categoriesPage.getContent();
        List<CategoryDTO> restCategories = restResult.getContent();
        Assert.assertNotNull(restCategories);
        Assert.assertEquals(dbCategories.size(), restCategories.size());

        // checking that we got the same categories from DB and from RST API, in the same order
        for (int i = 0; i < dbCategories.size(); i++) {
            Category dbCategory = dbCategories.get(i);
            CategoryDTO restCategory = restCategories.get(i);
            Assert.assertEquals(dbCategory.getId(), restCategory.getId());
            Assert.assertEquals(dbCategory.getName(), restCategory.getName());
        }
    }

    /**
     * Adding new category via REST and then checking that we can get that category from DB.
     */
    @Test
    public void testAddCategory() {
        Category category = new Category();
        String name = TestUtils.getRandomString();
        category.setName(name);

        initOAuthClientWithAdminCredentials();

        CategoryDTO categoryDTO = categoryDTOMapper.map(category);
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(categoryDTO);

        ResponseEntity<CategoryDTO> response = restTemplate.exchange("/categories", HttpMethod.POST, httpEntity, CategoryDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        CategoryDTO returnedCategory = response.getBody();
        Assert.assertNotNull(returnedCategory);
        Assert.assertNotNull(returnedCategory.getId());
        Assert.assertNotNull(returnedCategory.getName());
        Assert.assertEquals(category.getName(), returnedCategory.getName());

        Category addedCategory = getCategoryById(returnedCategory.getId());
        Assert.assertEquals(name, addedCategory.getName());

        // removing added category to keep the number of Categories in DB and in Elastic constant
        categoryService.delete(addedCategory.getId());
    }

    /**
     * Checking that we are getting expected response when the request body is not provided in request.
     */
    @Test
    public void testAddCategoryNoBody() {
        initOAuthClientWithAdminCredentials();

        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<CategoryDTO> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories", HttpMethod.POST, httpEntity, ErrorDTO.class);
        // server returns status 500 and an error message saying that request body is missing
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to add a category.
     */
    @Test
    public void testAddCategoryWrongUser() {
        initOAuthClientWithUserCredentials();

        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(new CategoryDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories", HttpMethod.POST, httpEntity, ErrorDTO.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Editing the randomly picked category via REST, then reading that category from DB and checking that our edits
     * were persisted.
     */
    @Test
    public void testEditCategory() {
        Category category = getRandomCategory();
        String newName = TestUtils.getRandomString();
        category.setName(newName);

        initOAuthClientWithAdminCredentials();

        CategoryDTO categoryDTO = categoryDTOMapper.map(category);
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(categoryDTO);
        ResponseEntity<CategoryDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.PUT, httpEntity, CategoryDTO.class, categoryDTO.getId());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        CategoryDTO returnedCategory = response.getBody();
        Assert.assertNotNull(returnedCategory);
        Assert.assertNotNull(returnedCategory.getId());
        Assert.assertEquals(category.getId(), returnedCategory.getId());
        Assert.assertNotNull(returnedCategory.getName());
        Assert.assertEquals(category.getName(), returnedCategory.getName());

        Category editedCategory = getCategoryById(category.getId());
        Assert.assertEquals(newName, editedCategory.getName());

        // TODO: implement category name rollback
    }

    /**
     * Checking that we are getting expected response when the request body is not provided in request.
     */
    @Test
    public void testEditCategoryNoBody() {
        initOAuthClientWithAdminCredentials();

        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<CategoryDTO> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories", HttpMethod.PUT, httpEntity, ErrorDTO.class);
        // server returns status 500 and an error message saying that request body is missing
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when the category is not found.
     */
    @Test
    public void testEditNonExistingCategory() {
        initOAuthClientWithAdminCredentials();

        long categoryId = getNonExistingCategoryId();
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(new CategoryDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.PUT, httpEntity, ErrorDTO.class, categoryId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to edit a category.
     */
    @Test
    public void testEditCategoryWrongUser() {
        initOAuthClientWithUserCredentials();

        long categoryId = 1; // id doesn't matter
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(new CategoryDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.PUT, httpEntity, ErrorDTO.class, categoryId);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Deleting a category via REST and checking that it was deleted from DB.
     */
    @Test
    public void testDeleteCategory() {
        // Creating a new category only to delete it a bit later. Such approach does not modify the number of categories
        // in DB after each test run, i.e. we are avoiding a situation when multiple runs of this test deplete the
        // Categories table, which can cause unexpected failures of other tests. Just rolling back the transaction
        // would be very convenient here, but we can't do that as explained in https://stackoverflow.com/a/46818347
        Category newCategory = new Category();
        newCategory.setName(TestUtils.getRandomString());
        Category categoryToDelete = categoryRepository.save(newCategory);
        Assert.assertNotNull(categoryToDelete.getId());

        initOAuthClientWithAdminCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<Void> response = restTemplate.exchange("/categories/{id}", HttpMethod.DELETE, httpEntity, Void.class, categoryToDelete.getId());
        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());

        Optional<Category> deletedCategoryOptional = categoryRepository.findById(categoryToDelete.getId());
        Assert.assertTrue(deletedCategoryOptional.isEmpty());
    }

    /**
     * Checking that we are getting expected response when the category is not found.
     */
    @Test
    public void testDeleteNonExistingCategory() {
        initOAuthClientWithAdminCredentials();

        long categoryId = getNonExistingCategoryId();
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.DELETE, httpEntity, ErrorDTO.class, categoryId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to delete a category.
     */
    @Test
    public void testDeleteCategoryWrongUser() {
        initOAuthClientWithUserCredentials();

        long categoryId = 1; // id doesn't matter
        HttpEntity<CategoryDTO> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/categories/{id}", HttpMethod.DELETE, httpEntity, ErrorDTO.class, categoryId);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    private long getNonExistingCategoryId() {
        return -42; // should be fine until we decide to use negative IDs
    }

    private PageRequest createDefaultPageRequest() {
        return PageRequest.of(0, PAGE_SIZE);
    }

    private <T> HttpEntity<T> createHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);

        return new HttpEntity<>(body, headers);
    }

    private void addAuthorizationHeader(HttpHeaders headers) {
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        headers.set(HttpHeaders.AUTHORIZATION, accessToken.getTokenType() + " " + accessToken.getValue());
    }

    private String toQueryParamString(Pageable pageable) {
        return String.format(
                "size=%s&page=%s&sort=%s",
                pageable.getPageSize(), pageable.getPageNumber(), pageable.getSort().toString().replaceAll(": ", ",")
        );
    }

    private Category getRandomCategory() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Category> categoryPage = categoryRepository.findAll(pageRequest);
        Assert.assertFalse(categoryPage.isEmpty());

        Category category = TestUtils.getRandomElement(categoryPage.getContent());
        Assert.assertNotNull(category.getId());

        return category;
    }

    private Category getCategoryById(Long id) {
        Optional<Category> addedCategoryOptional = categoryRepository.findById(id);
        Assert.assertFalse(addedCategoryOptional.isEmpty());
        return addedCategoryOptional.get();
    }

}
