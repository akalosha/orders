package name.akalosha.orders.rest.controller;

import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.SiteService;
import name.akalosha.orders.test.util.EntityFactory;
import org.junit.Assert;

import java.util.Optional;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CartOrderTest {
    String DELIVERY_METHOD_NAME = "UPS2Day";
    String SITE_NAME = "USSite";

    default Cart addRandomShippingAddressToCart(Long cartId) {
        AddressForm addressForm = EntityFactory.createRandomAddressForm();
        return getCartService().setShippingAddress(cartId, addressForm);
    }

    default Cart addDefaultDeliveryMethodToCart(Long cartId) {
        DeliveryMethod deliveryMethod = getDefaultDeliveryMethod();
        return getCartService().setDeliveryMethod(cartId, deliveryMethod.getId());
    }

    default DeliveryMethod getDefaultDeliveryMethod() {
        Optional<DeliveryMethod> deliveryMethodOptional = getDeliveryMethodRepository().findByName(DELIVERY_METHOD_NAME);
        Assert.assertTrue(deliveryMethodOptional.isPresent());
        Assert.assertNotNull(deliveryMethodOptional.get().getId());
        return deliveryMethodOptional.get();
    }

    default Cart createEmptyCart(String orderUserName) {
        Optional<Site> siteOptional = getSiteService().findSiteByName(SITE_NAME);
        Assert.assertTrue(siteOptional.isPresent());
        User user = getUser(orderUserName);

        Cart cart = EntityFactory.createEmptyCart(siteOptional.get(), user);
        return getCartService().save(cart);
    }

    default User getUser(String username) {
        Optional<User> userOptional = getUserRepository().findByUsername(username);
        Assert.assertTrue(userOptional.isPresent());
        return userOptional.get();
    }

    CartService getCartService();
    DeliveryMethodRepository getDeliveryMethodRepository();
    SiteService getSiteService();
    UserRepository getUserRepository();

}
