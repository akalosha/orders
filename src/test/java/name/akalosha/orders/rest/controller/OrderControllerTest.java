package name.akalosha.orders.rest.controller;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.CartOrderEntryDTO;
import name.akalosha.orders.model.dto.rest.ErrorDTO;
import name.akalosha.orders.model.dto.rest.OrderDTO;
import name.akalosha.orders.model.dto.rest.ProductDTO;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.model.dto.rest.report.RevenueReport;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.repository.jpa.OrderRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import name.akalosha.orders.service.CartOrderEntryService;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.service.SiteService;
import name.akalosha.orders.test.util.TestUtils;
import name.akalosha.orders.util.DateTimeUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Integration test for {@link CartRestController}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class OrderControllerTest implements CartOrderTest {

    public static final int PAGE_SIZE = 10;
    public static final String SORT_PROPERTY = "id";
    public static final int MAX_ENTRY_QUANTITY = 5;
    public static final int ORDERS_FOR_REVENUE_REPORT = 5;
    private static final String SINCE_PARAM_NAME = "since";
    private static final String PRODUCT_NAME_PARAM_NAME = "productName";
    private static final int MIN_ORDERS_FOR_ORDER_HISTORY = 3;

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private CartOrderEntryService cartOrderEntryService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private SiteService siteService;


    private void initOAuthClientWithAdminCredentials() {
        oauthClient = getClientBuilderWithAdminCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithAdminCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    @Before
    public void initOAuthClientWithUserCredentials() {
        oauthClient = getClientBuilderWithUserCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithUserCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .setUsername(OAuthClientConfig.NON_ADMIN_USER)
                .setPassword(OAuthClientConfig.NON_ADMIN_USER_PASSWORD)
                .setPort(port);
    }

    @Before
    public void ensureThatWeHaveSomeOrders() {
        if (!existsValidOrder()) {
            addDummyOrder(OAuthClientConfig.NON_ADMIN_USER, 2);
        }
        Assert.assertTrue(existsValidOrder());
    }

    private boolean existsValidOrder() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Order> ordersPage = orderRepository.findAll(pageRequest);
        if (!ordersPage.isEmpty()) {
            return ordersPage.stream()
                    .map(Order::getId)
                    .map(orderService::findWithEagerFetch)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .anyMatch(this::isValidOrderForTesting);
        }
        return false;
    }

    private boolean isValidOrderForTesting(Order order) {
        return order != null &&
                order.getId() != null &&
                order.getUser() != null &&
                CollectionUtils.isNotEmpty(order.getEntries()) &&
                order.getEntries().stream().anyMatch(this::isValidEntry);
    }

    private boolean isValidEntry(CartOrderEntry entry) {
        return entry != null &&
                entry.getId() != null &&
                entry.getQuantity() > 0 &&
                entry.getProduct() != null &&
                entry.getProduct().getId() != null &&
                entry.getProduct().getPrice() != null;
    }

    private Order addDummyOrder(String orderUserName, int entriesNumber) {
        Cart cart = createEmptyCart(orderUserName);

        addEntriesToCart(cart, entriesNumber);
        addRandomShippingAddressToCart(cart.getId());
        addDefaultDeliveryMethodToCart(cart.getId());

        Order order = orderService.placeOrder(cart.getId());
        Optional<Order> orderOptional = orderService.findWithEagerFetch(order.getId());
        Assert.assertTrue(orderOptional.isPresent());
        Assert.assertEquals(entriesNumber, orderOptional.get().getEntries().size());

        return orderOptional.get();
    }

    private void addEntriesToCart(Cart cart, int numberOfEntriesToAdd) {
        List<Product> alreadyUsedProducts = new ArrayList<>();
        for (int i = 0; i < numberOfEntriesToAdd; i++) {
            Optional<Product> productOptional = getProductOtherThan(alreadyUsedProducts);
            Product product = productOptional.orElse(getRandomProduct());
            alreadyUsedProducts.add(product);

            CartOrderEntry entry = new CartOrderEntry();
            entry.setProduct(product);
            entry.setQuantity(RandomUtils.nextInt(1, MAX_ENTRY_QUANTITY));
            cartOrderEntryService.save(cart.getId(), entry);
        }
    }

    /**
     * First we get an order from DB, and then we check that we can get the same order via REST.
     */
    @Test
    public void testGetOrder() {
        Order dbOrder = getRandomOrder();

        initOAuthClientForUser(dbOrder.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<OrderDTO> response = restTemplate.exchange(
                "/users/{userId}/orders/{id}",
                HttpMethod.GET,
                httpEntity,
                OrderDTO.class,
                dbOrder.getUser().getId(),
                dbOrder.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        OrderDTO restOrder = response.getBody();
        Assert.assertNotNull(restOrder);

        Assert.assertNotNull(restOrder.getId());
        Assert.assertEquals(dbOrder.getId(), restOrder.getId());
        Assert.assertNotNull(restOrder.getUser());
        Assert.assertEquals(dbOrder.getUser().getUsername(), restOrder.getUser());
        Assert.assertEquals(dbOrder.getTotal(), restOrder.getTotal());
        Assert.assertNotNull(restOrder.getDatePlaced());
        Assert.assertEquals(dbOrder.getDatePlaced(), restOrder.getDatePlaced());
        Assert.assertNotNull(restOrder.getEntries());
        Assert.assertEquals(dbOrder.getEntries().size(), restOrder.getEntries().size());

        for (int i = 0; i < restOrder.getEntries().size(); i++) {
            CartOrderEntryDTO restEntry = restOrder.getEntries().get(i);
            CartOrderEntry dbEntry = dbOrder.getEntries().get(i);
            Assert.assertNotNull(restEntry);
            Assert.assertEquals(dbEntry.getId(), restEntry.getId());
            Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
            Assert.assertNotNull(restEntry.getProduct());
            Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
            Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());
        }
    }

    /**
     * First we get an order from DB, and then we try to get the same order via REST, but we would be authenticating as
     * a user that doesn't own the order. This should fail.
     */
    @Test
    public void testGetOrderWithOtherUser() {
        // This test works only for two predefined test users. If the name of some third user would be passed as an
        // argument to testGetCartWithOtherUser(), it will fail because we need to authenticate with username and
        // password and we know only test users' passwords.
        testGetOrderWithOtherUser(OAuthClientConfig.ADMIN_USER, OAuthClientConfig.NON_ADMIN_USER);
        // checking the reverse scenario as well
        testGetOrderWithOtherUser(OAuthClientConfig.NON_ADMIN_USER, OAuthClientConfig.ADMIN_USER);
    }

    private void testGetOrderWithOtherUser(String orderOwnerUsername, String otherUsername) {
        Order order = addDummyOrder(orderOwnerUsername, 1);

        User otherUser = getUser(otherUsername);
        initOAuthClientForUser(otherUser.getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<OrderDTO> response = restTemplate.exchange(
                "/users/{userId}/orders/{id}",
                HttpMethod.GET,
                httpEntity,
                OrderDTO.class,
                otherUser.getId(),
                order.getId()
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

        orderService.delete(order.getId());
    }

    /**
     * First we get a list of orders from DB, then we get orders from REST (using the same pagination settings)
     * and finally we are checking that both lists contain the same elements.
     */
    @Test
    public void testGetAllOrders() {
        PageRequest pageRequest = createDefaultSortedPageRequest();
        Page<Order> ordersPage = orderRepository.findAll(pageRequest);
        Assert.assertFalse(ordersPage.isEmpty());

        // we are testing admin-only endpoint
        initOAuthClientWithAdminCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<OrderDTO>> response = restTemplate.exchange(
                "/orders?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<OrderDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        PageDTO<OrderDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);

        List<Order> dbOrders = ordersPage.getContent();
        List<OrderDTO> restOrders = restResult.getContent();
        Assert.assertNotNull(restOrders);
        Assert.assertEquals(dbOrders.size(), restOrders.size());

        // checking that we got the same orders from DB and from REST API, in the same order
        validateOrdersList(dbOrders, restOrders);
    }

    /**
     * /orders endpoint is admin-only, so checking that a regular user can not access it.
     */
    @Test
    public void testGetAllCartsWrongUser() {
        initOAuthClientWithUserCredentials();

        PageRequest pageRequest = createDefaultSortedPageRequest();
        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<OrderDTO>> response = restTemplate.exchange(
                "/orders?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<OrderDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Checking that /revenue-report endpoint produces expected data.
     */
    @Test
    public void testGetRevenueReport() {
        testGetRevenueReportLimitedRandomness();
        testGetRevenueReportPredefinedData();
    }

    /**
     * Checking the /revenue-report endpoint for the case when we have some randomly-generated orders in DB
     */
    public void testGetRevenueReportLimitedRandomness() {
        List<Order> addedOrders = addRandomOrdersForRevenueReport();
        LocalDate date = LocalDate.now();
        RevenueReport referenceReport = orderService.generateRevenueReport(date);

        initOAuthClientWithAdminCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<RevenueReport> response = restTemplate.exchange(
                "/orders/revenue-report?" + SINCE_PARAM_NAME + "=" + DateTimeFormatter.ISO_DATE.format(date),
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<RevenueReport>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(MapUtils.isNotEmpty(response.getBody().getRevenueByDay()));
        Assert.assertEquals(referenceReport.getRevenueByDay(), response.getBody().getRevenueByDay());

        // removing all orders that we have added for this test
        addedOrders.forEach(o -> orderService.delete(o.getId()));
    }

    /**
     * Checking the /revenue-report endpoint for the case when we have only few manually-generated orders in DB.
     */
    public void testGetRevenueReportPredefinedData() {
        // Removing all existing orders. When the revenue report will be generated, we want only a predefined set of
        // orders to exist in DB. Removing all orders will not cause any issues, because ensureThatWeHaveSomeOrders()
        // will create a dummy order if needed.
        orderRepository.findAll().forEach(o -> orderService.delete(o.getId()));

        Map<LocalDate, BigDecimal> referenceReportData = createPredefinedRevenueReport();

        initOAuthClientWithAdminCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        LocalDate date = LocalDate.parse("1999-01-01", DateTimeFormatter.ISO_DATE);
        ResponseEntity<RevenueReport> response = restTemplate.exchange(
                "/orders/revenue-report?" + SINCE_PARAM_NAME + "=" + DateTimeFormatter.ISO_DATE.format(date),
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<RevenueReport>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(MapUtils.isNotEmpty(response.getBody().getRevenueByDay()));
        Assert.assertEquals(referenceReportData, response.getBody().getRevenueByDay());

        // removing all orders that we have added for this test
        orderRepository.findAll().forEach(o -> orderService.delete(o.getId()));
    }

    /**
     * /revenue-report endpoint is admin-only, so checking that a regular user can not access it.
     */
    @Test
    public void testGetRevenueReportWrongUser() {
        initOAuthClientWithUserCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        LocalDate date = LocalDate.parse("1999-01-01", DateTimeFormatter.ISO_DATE);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange(
                "/orders/revenue-report?" + SINCE_PARAM_NAME + "=" + DateTimeFormatter.ISO_DATE.format(date),
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<ErrorDTO>() {}
        );
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Checking that searching orders by product name produces expected results.
     */
    @Test
    public void testSearchByProduct() {
        testSearchByProductSingleMatchingOrder();
        testSearchByProductMultipleMatchingOrders();
    }

    /**
     * Test for the case when we have only one matching order in DB.
     */
    public void testSearchByProductSingleMatchingOrder() {
        initOAuthClientWithAdminCredentials();

        Order dbOrder = getRandomOrder();
        String productName = TestUtils.getRandomElement(dbOrder.getEntries()).getProduct().getName();
        Assert.assertTrue(productName.length() > 1);
        removeOtherOrdersContainingProduct(dbOrder, productName);

        // we will be searching by a part of product's name
        String searchQuery = StringUtils.substringBefore(productName, " ");
        searchQuery = StringUtils.left(searchQuery, productName.length() - 1);

        PageRequest pageRequest = createDefaultSortedPageRequest();
        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<OrderDTO>> response = restTemplate.exchange(
                "/orders/search-by-product?" + PRODUCT_NAME_PARAM_NAME + "=" + searchQuery + "&" + toQueryParamString(pageRequest),
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<OrderDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Assert.assertNotNull(response.getBody());
        List<OrderDTO> restOrders = response.getBody().getContent();
        Assert.assertTrue(CollectionUtils.isNotEmpty(restOrders));
        Assert.assertEquals(1, restOrders.size());
        OrderDTO restOrder = restOrders.iterator().next();
        Assert.assertNotNull(restOrder);
        Assert.assertTrue(CollectionUtils.isNotEmpty(restOrder.getEntries()));
        Assert.assertTrue(
                restOrder.getEntries().stream()
                        .map(CartOrderEntryDTO::getProduct)
                        .map(ProductDTO::getName)
                        .anyMatch(productName::equals)
        );
    }

    /**
     * Test for the case when we have a number of matching orders in DB.
     */
    public void testSearchByProductMultipleMatchingOrders() {
        // ensuring that we would have at least 2 orders containing a product named "Red Screwdriver"
        String productNamePart = "Red";
        int expectedOrdersCount = 2;
        List<Order> addedOrders = ensureNOrdersContainingProduct(productNamePart, expectedOrdersCount, OAuthClientConfig.NON_ADMIN_USER);

        initOAuthClientWithAdminCredentials();

        PageRequest pageRequest = createDefaultSortedPageRequest();
        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<OrderDTO>> response = restTemplate.exchange(
                "/orders/search-by-product?" + PRODUCT_NAME_PARAM_NAME + "=" + productNamePart + "&" + toQueryParamString(pageRequest),
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<OrderDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Assert.assertNotNull(response.getBody());
        List<OrderDTO> restOrders = response.getBody().getContent();
        Assert.assertTrue(CollectionUtils.isNotEmpty(restOrders));
        Assert.assertEquals(expectedOrdersCount, restOrders.size());

        for (OrderDTO restOrder : restOrders) {
            Assert.assertNotNull(restOrder);
            Assert.assertTrue(CollectionUtils.isNotEmpty(restOrder.getEntries()));
            Assert.assertTrue(
                    restOrder.getEntries().stream()
                            .map(CartOrderEntryDTO::getProduct)
                            .map(ProductDTO::getName)
                            .anyMatch(name -> name.contains(productNamePart))
            );
        }

        addedOrders.stream()
                .map(Order::getId)
                .forEach(orderService::delete);
    }

    /**
     * Checking that search queries that should match all orders produce expected results.
     */
    @Test
    public void testSearchByProductAllOrders() {
        testSearchByProductWildcard();
        testSearchByProductEmptyQuery();
    }

    /**
     * Test for a search query that should match all orders.
     */
    public void testSearchByProductWildcard() {
        long dbOrderCount = orderRepository.count();
        String searchQuery = "*";
        validateSearchAllOrders(searchQuery, dbOrderCount);
    }

    /**
     * Test for an empty search query. It should match all orders.
     */
    public void testSearchByProductEmptyQuery() {
        long dbOrderCount = orderRepository.count();
        String searchQuery = "";
        validateSearchAllOrders(searchQuery, dbOrderCount);
    }

    private void validateSearchAllOrders(String searchQuery, long dbOrderCount) {
        initOAuthClientWithAdminCredentials();

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        PageRequest pageRequest = createDefaultSortedPageRequest();
        ResponseEntity<PageDTO<OrderDTO>> response = restTemplate.exchange(
                "/orders/search-by-product?" + PRODUCT_NAME_PARAM_NAME + "=" + searchQuery + "&" + toQueryParamString(pageRequest),
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<OrderDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        PageDTO<OrderDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);
        Assert.assertEquals(dbOrderCount, restResult.getTotalElements());

        List<OrderDTO> restOrders = restResult.getContent();
        Assert.assertTrue(CollectionUtils.isNotEmpty(restOrders));
        Assert.assertEquals(dbOrderCount, restOrders.size());
    }

    /**
     * /search-by-product endpoint is admin-only, so checking that a regular user can not access it.
     */
    @Test
    public void testSearchByProductWrongUser() {
        initOAuthClientWithUserCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange(
                "/orders/search-by-product?" + PRODUCT_NAME_PARAM_NAME + "=",
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<ErrorDTO>() {}
        );
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    @Test
    public void testGetOrderHistory() {
        String username = OAuthClientConfig.NON_ADMIN_USER;
        PageRequest pageRequest = createDefaultSortedPageRequest();
        List<Order> dbOrders = getOrdersByUser(username, pageRequest);

        List<Order> addedOrders = new ArrayList<>();
        if (dbOrders.size() < MIN_ORDERS_FOR_ORDER_HISTORY) {
            // Ensuring that we have more than one order. This way we would get multiple entries in order history.
            for (int i = 0; i < MIN_ORDERS_FOR_ORDER_HISTORY - dbOrders.size(); i++) {
                Order addedOrder = addDummyOrder(username, 1);
                addedOrders.add(addedOrder);
            }
            dbOrders = getOrdersByUser(username, pageRequest);
        }

        initOAuthClientForUser(username);

        User user = getUser(username);
        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<OrderDTO>> response = restTemplate.exchange(
                "/users/{userId}/orders/history?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<OrderDTO>>() {},
                user.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        PageDTO<OrderDTO> responseBody = response.getBody();
        Assert.assertNotNull(responseBody);
        List<OrderDTO> restOrders = responseBody.getContent();
        Assert.assertTrue(CollectionUtils.isNotEmpty(restOrders));
        Assert.assertEquals(dbOrders.size(), restOrders.size());

        // checking that we got the same orders from DB and from REST API, in the same order
        validateOrdersList(dbOrders, restOrders);

        addedOrders.stream()
                .map(Order::getId)
                .forEach(orderService::delete);
    }

    private void validateOrdersList(List<Order> dbOrders, List<OrderDTO> restOrders) {
        for (int i = 0; i < dbOrders.size(); i++) {
            Optional<Order> dbOrderOptional = orderRepository.findByIdWithEagerFetch(dbOrders.get(i).getId());
            Assert.assertTrue(dbOrderOptional.isPresent());

            Order dbOrder = dbOrderOptional.get();
            OrderDTO restOrder = restOrders.get(i);
            Assert.assertNotNull(restOrder);
            Assert.assertEquals(dbOrder.getId(), restOrder.getId());
            Assert.assertEquals(dbOrder.getUser().getUsername(), restOrder.getUser());
            Assert.assertEquals(dbOrder.getTotal(), restOrder.getTotal());
            Assert.assertNotNull(restOrder.getDatePlaced());
            Assert.assertEquals(dbOrder.getDatePlaced(), restOrder.getDatePlaced());
            Assert.assertNotNull(restOrder.getEntries());

            List<CartOrderEntry> dbEntries = dbOrder.getEntries();
            List<CartOrderEntryDTO> restEntries = restOrder.getEntries();
            // checking that we got the same order entries from DB and from REST API, in the same order
            for (int j = 0; j < dbEntries.size(); j++) {
                CartOrderEntry dbEntry = dbEntries.get(j);
                CartOrderEntryDTO restEntry = restEntries.get(j);
                Assert.assertNotNull(restEntry);
                Assert.assertEquals(dbEntry.getId(), restEntry.getId());
                Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
                Assert.assertNotNull(restEntry.getProduct());
                Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
                Assert.assertEquals(dbEntry.getProduct().getName(), restEntry.getProduct().getName());
                Assert.assertEquals(dbEntry.getProduct().getSku(), restEntry.getProduct().getSku());
                Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());
            }
        }
    }

    private List<Order> getOrdersByUser(String username, PageRequest pageRequest) {
        // order count in DB should not exceed the default page size, otherwise this method may fail to do what it is designed to do
        Page<Order> ordersPage = orderRepository.findAll(pageRequest);
        Assert.assertFalse(ordersPage.isEmpty());

        return ordersPage.stream()
                .filter(order -> username.equals(order.getUser().getUsername()))
                .collect(Collectors.toList());
    }

    private void removeOtherOrdersContainingProduct(Order orderToKeep, String productNamePartToAvoid) {
        // order count in DB should not exceed the default page size, otherwise this method may fail to do what it is designed to do
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Order> ordersPage = orderRepository.findAll(pageRequest);
        Assert.assertFalse(ordersPage.isEmpty());

        ordersPage.stream()
                .filter(order -> !order.getId().equals(orderToKeep.getId()))
                .filter(order -> containsProductName(order, productNamePartToAvoid))
                .map(Order::getId)
                .forEach(orderService::delete);
    }

    private List<Order> ensureNOrdersContainingProduct(String productNamePart, int requiredOrdersCount, String username) {
        List<Order> ordersContainingProduct = getOrdersContainingProduct(productNamePart);
        List<Order> addedOrders = new ArrayList<>();
        for (int i = 0; i < requiredOrdersCount - ordersContainingProduct.size(); i++) {
            Order addedOrder = addDummyOrder(username, productNamePart);
            addedOrders.add(addedOrder);
        }

        return addedOrders;
    }

    private List<Order> getOrdersContainingProduct(String productNamePart) {
        // order count in DB should not exceed the default page size, otherwise this method may fail to do what it is designed to do
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Order> ordersPage = orderRepository.findAll(pageRequest);
        Assert.assertFalse(ordersPage.isEmpty());

        return ordersPage.stream()
                .filter(order -> containsProductName(order, productNamePart))
                .collect(Collectors.toList());
    }

    private boolean containsProductName(Order order, String productNamePart) {
        return order.getEntries().stream()
                .map(CartOrderEntry::getProduct)
                .map(Product::getName)
                .anyMatch(name -> name.contains(productNamePart));
    }

    private PageRequest createDefaultPageRequest() {
        return PageRequest.of(0, PAGE_SIZE);
    }

    private PageRequest createDefaultSortedPageRequest() {
        return PageRequest.of(0, PAGE_SIZE, Sort.by(Sort.Direction.ASC, SORT_PROPERTY));
    }

    private <T> HttpEntity<T> createHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);

        return new HttpEntity<>(body, headers);
    }

    private void addAuthorizationHeader(HttpHeaders headers) {
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        headers.set(HttpHeaders.AUTHORIZATION, accessToken.getTokenType() + " " + accessToken.getValue());
    }

    private String toQueryParamString(Pageable pageable) {
        return String.format(
                "size=%s&page=%s&sort=%s",
                pageable.getPageSize(), pageable.getPageNumber(), pageable.getSort().toString().replaceAll(": ", ",")
        );
    }

    private Product getRandomProduct() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productsPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productsPage.isEmpty());

        Product dbProduct = TestUtils.getRandomElement(productsPage.getContent());
        Assert.assertNotNull(dbProduct.getId());

        return dbProduct;
    }

    private Optional<Product> getProductOtherThan(List<Product> productsToAvoid) {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productPage.isEmpty());

        return productPage.get()
                .filter(product -> productsToAvoid.stream()
                        .map(Product::getId)
                        .noneMatch(Predicate.isEqual(product.getId()))
                )
                .findAny();
    }

    private Order getRandomOrder() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Order> ordersPage = orderRepository.findAll(pageRequest);
        Assert.assertFalse(ordersPage.isEmpty());

        Order dbOrder = TestUtils.getRandomElement(ordersPage.getContent());
        Optional<Order> orderOptional = orderService.findWithEagerFetch(dbOrder.getId());
        Assert.assertTrue(orderOptional.isPresent());
        dbOrder = orderOptional.get();
        Assert.assertNotNull(dbOrder.getId());
        Assert.assertNotNull(dbOrder.getUser());
        Assert.assertNotNull(dbOrder.getUser().getId());

        return dbOrder;
    }

    private boolean isAdminUser(String username) {
        Assert.assertNotNull(username);
        return username.equals(OAuthClientConfig.ADMIN_USER);
    }

    private boolean isRegularUser(String username) {
        Assert.assertNotNull(username);
        return username.equals(OAuthClientConfig.NON_ADMIN_USER);
    }

    private void initOAuthClientForUser(String username) {
        if (isAdminUser(username)) {
            initOAuthClientWithAdminCredentials();
        } else if (isRegularUser(username)) {
            initOAuthClientWithUserCredentials();
        } else {
            throw new IllegalStateException("Unable to authenticate unknown user " + username);
        }
    }

    private List<Order> addRandomOrdersForRevenueReport() {
        long orderCount = orderRepository.count();
        if (orderCount >= ORDERS_FOR_REVENUE_REPORT) {
            return Collections.emptyList();
        }

        List<Order> addedOrders = new ArrayList<>();
        for (int i = 0; i < ORDERS_FOR_REVENUE_REPORT - orderCount; i++) {
            Order order = addDummyOrder(OAuthClientConfig.NON_ADMIN_USER, RandomUtils.nextInt(1, MAX_ENTRY_QUANTITY));
            addedOrders.add(order);
        }

        return addedOrders;
    }

    private Map<LocalDate, BigDecimal> createPredefinedRevenueReport() {
        long orderCount = orderRepository.count();
        // for this test case we need to start with an empty DB
        Assert.assertEquals(0, orderCount);

        Map<LocalDate, BigDecimal> revenueByDay = new HashMap<>();

        Order order = addDummyOrder(OAuthClientConfig.NON_ADMIN_USER, RandomUtils.nextInt(1, MAX_ENTRY_QUANTITY));
        LocalDate datePlaced = LocalDate.parse("2001-01-01", DateTimeFormatter.ISO_DATE);
        // Changing order's placed date to a predefined value, so that we could create a revenue report with fixed dates.
        // Normally there would be no sense in editing order's placed date (one may even want to prohibit such operation),
        // but for this test it is useful.
        order.setDatePlaced(DateTimeUtil.toTimestamp(datePlaced));
        order = orderRepository.save(order);
        revenueByDay.put(datePlaced, order.getTotal());

        order = addDummyOrder(OAuthClientConfig.NON_ADMIN_USER, RandomUtils.nextInt(1, MAX_ENTRY_QUANTITY));
        datePlaced = LocalDate.parse("2010-10-10", DateTimeFormatter.ISO_DATE);
        order.setDatePlaced(DateTimeUtil.toTimestamp(datePlaced));
        order = orderRepository.save(order);
        revenueByDay.put(datePlaced, order.getTotal());

        return revenueByDay;
    }

    private Order addDummyOrder(String orderUserName, String productNamePart) {
        Cart cart = createEmptyCart(orderUserName);

        CartOrderEntry entry = new CartOrderEntry();
        entry.setProduct(getProductByNamePart(productNamePart));
        entry.setQuantity(RandomUtils.nextInt(1, MAX_ENTRY_QUANTITY));
        cartOrderEntryService.save(cart.getId(), entry);

        addRandomShippingAddressToCart(cart.getId());
        addDefaultDeliveryMethodToCart(cart.getId());

        Order order = orderService.placeOrder(cart.getId());
        Optional<Order> orderOptional = orderService.findWithEagerFetch(order.getId());
        Assert.assertTrue(orderOptional.isPresent());
        List<CartOrderEntry> entries = orderOptional.get().getEntries();
        Assert.assertEquals(1, entries.size());
        Assert.assertTrue(entries.iterator().next().getProduct().getName().contains(productNamePart));

        return orderOptional.get();
    }

    private Product getProductByNamePart(String productNamePart) {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productsPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productsPage.isEmpty());

        Optional<Product> dbProductOptional = productsPage.getContent().stream()
                .filter(p -> p.getName().contains(productNamePart))
                .findAny();
        Assert.assertTrue(dbProductOptional.isPresent());
        Assert.assertNotNull(dbProductOptional.get().getId());

        return dbProductOptional.get();
    }

    @Override
    public DeliveryMethodRepository getDeliveryMethodRepository() {
        return deliveryMethodRepository;
    }

    @Override
    public CartService getCartService() {
        return cartService;
    }

    @Override
    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    public SiteService getSiteService() {
        return siteService;
    }

}
