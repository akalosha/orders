package name.akalosha.orders.rest.controller;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import name.akalosha.orders.converters.mappers.dto.ProductDTOMapper;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.rest.ErrorDTO;
import name.akalosha.orders.model.dto.rest.ProductDTO;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.repository.jpa.CategoryRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import name.akalosha.orders.service.ProductService;
import name.akalosha.orders.test.util.TestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Integration test for {@link ProductRestController}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class ProductControllerTest {

    public static final int PAGE_SIZE = 10;
    public static final String SORT_PROPERTY = "id";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDTOMapper productDTOMapper;

    private void initOAuthClientWithAdminCredentials() {
        oauthClient = getClientBuilderWithAdminCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithAdminCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    @Before
    public void initOAuthClientWithUserCredentials() {
        oauthClient = getClientBuilderWithUserCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithUserCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .setUsername(OAuthClientConfig.NON_ADMIN_USER)
                .setPassword(OAuthClientConfig.NON_ADMIN_USER_PASSWORD)
                .setPort(port);
    }

    /**
     * First we get some random product from DB, and then we check that we can get the same product via REST.
     */
    @Test
    public void testGetProduct() {
        Product dbProduct = getRandomProduct();

        HttpEntity<ProductDTO> httpEntity = createHttpEntity(null);
        ResponseEntity<ProductDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.GET, httpEntity, ProductDTO.class, dbProduct.getId());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        ProductDTO restProduct = response.getBody();
        Assert.assertNotNull(restProduct);

        Assert.assertNotNull(restProduct.getId());
        Assert.assertEquals(dbProduct.getId(), restProduct.getId());
        Assert.assertNotNull(restProduct.getSku());
        Assert.assertEquals(dbProduct.getSku(), restProduct.getSku());
        Assert.assertEquals(dbProduct.getName(), restProduct.getName());
        Assert.assertNotNull(restProduct.getPrice());
        Assert.assertEquals(dbProduct.getPrice(), restProduct.getPrice());
        Assert.assertNotNull(restProduct.getCategory());
        Assert.assertEquals(dbProduct.getCategory().getId(), restProduct.getCategory().getId());
    }

    /**
     * Checking that we are getting expected response when the product is not found.
     */
    @Test
    public void testGetNonExistingProduct() {
        long productId = getNonExistingProductId();
        HttpEntity<ProductDTO> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.GET, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * First we get a list of products from DB, then we get products from REST (using the same pagination settings)
     * and finally we are checking that both lists contain the same elements.
     */
    @Test
    public void testGetAllProducts() {
        PageRequest pageRequest = createDefaultSortedPageRequest();
        Page<Product> productsPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productsPage.isEmpty());

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<ProductDTO>> response = restTemplate.exchange(
                "/products?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<ProductDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        PageDTO<ProductDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);

        List<Product> dbProducts = productsPage.getContent();
        List<ProductDTO> restProducts = restResult.getContent();
        Assert.assertNotNull(restProducts);
        Assert.assertEquals(dbProducts.size(), restProducts.size());

        // checking that we got the same products from DB and from RST API, in the same order
        for (int i = 0; i < dbProducts.size(); i++) {
            Product dbProduct = dbProducts.get(i);
            ProductDTO restProduct = restProducts.get(i);
            Assert.assertEquals(dbProduct.getId(), restProduct.getId());
            Assert.assertEquals(dbProduct.getSku(), restProduct.getSku());
            Assert.assertEquals(dbProduct.getName(), restProduct.getName());
            Assert.assertEquals(dbProduct.getPrice(), restProduct.getPrice());
            Assert.assertNotNull(dbProduct.getCategory());
            Assert.assertNotNull(restProduct.getCategory());
            Assert.assertEquals(dbProduct.getCategory().getId(), restProduct.getCategory().getId());
        }
    }

    /**
     * Adding new product via REST and then checking that we can get that product from DB.
     */
    @Test
    public void testAddProduct() {
        Product product = new Product();
        String sku = TestUtils.getRandomString();
        product.setSku(sku);
        String name = TestUtils.getRandomString();
        product.setName(name);
        BigDecimal price = getRandomPrice();
        product.setPrice(price);

        Category category = getRandomCategory();
        product.setCategory(category);

        initOAuthClientWithAdminCredentials();

        ProductDTO productDTO = productDTOMapper.map(product);
        HttpEntity<ProductDTO> httpEntity = createHttpEntity(productDTO);
        ResponseEntity<ProductDTO> response = restTemplate.exchange("/products", HttpMethod.POST, httpEntity, ProductDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        ProductDTO returnedProduct = response.getBody();
        Assert.assertNotNull(returnedProduct);
        Assert.assertNotNull(returnedProduct.getId());
        Assert.assertNotNull(returnedProduct.getSku());
        Assert.assertNotNull(returnedProduct.getPrice());
        Assert.assertNotNull(returnedProduct.getCategory());
        Assert.assertNotNull(returnedProduct.getCategory().getId());

        Assert.assertEquals(product.getSku(), returnedProduct.getSku());
        Assert.assertEquals(product.getName(), returnedProduct.getName());
        Assert.assertEquals(product.getPrice(), returnedProduct.getPrice());
        Assert.assertEquals(product.getCategory().getId(), returnedProduct.getCategory().getId());

        Product addedProduct = getProductById(returnedProduct.getId());
        Assert.assertEquals(sku, addedProduct.getSku());

        // removing added product to keep the number of products in DB and in Elastic constant
        productService.delete(addedProduct.getId());
    }

    /**
     * Checking that we are getting expected response when the request body is not provided in request.
     */
    @Test
    public void testAddProductNoBody() {
        initOAuthClientWithAdminCredentials();

        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<ProductDTO> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products", HttpMethod.POST, httpEntity, ErrorDTO.class);
        // server returns status 500 and an error message saying that request body is missing
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to add a product.
     */
    @Test
    public void testAddProductWrongUser() {
        initOAuthClientWithUserCredentials();

        HttpEntity<ProductDTO> httpEntity = createHttpEntity(new ProductDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products", HttpMethod.POST, httpEntity, ErrorDTO.class);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Editing the randomly picked product via REST, then reading that product from DB and checking that our edits
     * were persisted.
     */
    @Test
    public void testEditProduct() {
        Product product = getRandomProduct();
        String oldSku = product.getSku();
        String newSku = TestUtils.getRandomString();
        product.setSku(newSku);

        String oldName = product.getName();
        String newName = TestUtils.getRandomString();
        product.setName(newName);

        BigDecimal oldPrice = product.getPrice();
        BigDecimal newPrice = getRandomPrice();
        product.setPrice(newPrice);

        Category oldCategory = product.getCategory();
        Category newCategory = getCategoryOtherThan(product.getCategory());
        product.setCategory(newCategory);

        initOAuthClientWithAdminCredentials();

        ProductDTO returnedProduct = editProduct(product, HttpMethod.PUT, product.getId());
        Assert.assertNotNull(returnedProduct.getId());
        Assert.assertEquals(product.getId(), returnedProduct.getId());
        Assert.assertNotNull(returnedProduct.getSku());
        Assert.assertEquals(product.getSku(), returnedProduct.getSku());
        Assert.assertNotNull(returnedProduct.getName());
        Assert.assertEquals(product.getName(), returnedProduct.getName());
        Assert.assertNotNull(returnedProduct.getPrice());
        Assert.assertEquals(product.getPrice(), returnedProduct.getPrice());
        Assert.assertNotNull(returnedProduct.getCategory());
        Assert.assertEquals(product.getCategory().getId(), returnedProduct.getCategory().getId());

        Product editedProduct = getProductById(product.getId());
        Assert.assertEquals(newSku, editedProduct.getSku());
        Assert.assertEquals(newName, editedProduct.getName());
        Assert.assertEquals(newPrice, editedProduct.getPrice());
        Assert.assertNotNull(editedProduct.getCategory());
        Assert.assertEquals(newCategory.getId(), editedProduct.getCategory().getId());

        // manually rolling back changes
        editedProduct.setSku(oldSku);
        editedProduct.setName(oldName);
        editedProduct.setPrice(oldPrice);
        editedProduct.setCategory(oldCategory);
        productService.edit(editedProduct);
    }

    /**
     * Checking that we are getting expected response when the product is not found.
     */
    @Test
    public void testEditNonExistingProduct() {
        long productId = getNonExistingProductId();

        initOAuthClientWithAdminCredentials();

        HttpEntity<ProductDTO> httpEntity = createHttpEntity(new ProductDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.PUT, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to edit a product.
     */
    @Test
    public void testEditProductWrongUser() {
        initOAuthClientWithUserCredentials();

        long productId = 1; // id doesn't matter
        HttpEntity<ProductDTO> httpEntity = createHttpEntity(new ProductDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.PUT, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Patching the SKU of randomly picked product via REST, then reading that product from DB and checking that our
     * change was persisted (and that nothing else but SKU was changed).
     */
    @Test
    public void testPatchProductSku() {
        Product product = getRandomProduct();
        String oldSku = product.getSku();
        String newSku = TestUtils.getRandomString();
        product.setSku(newSku);

        initOAuthClientWithAdminCredentials();

        ProductDTO returnedProduct = editProduct(product, HttpMethod.PATCH, product.getId());
        Assert.assertNotNull(returnedProduct.getSku());
        Assert.assertEquals(newSku, returnedProduct.getSku());

        Product editedProduct = getProductById(product.getId());
        Assert.assertEquals(newSku, editedProduct.getSku());
        Assert.assertEquals(product.getName(), editedProduct.getName());
        Assert.assertEquals(product.getPrice(), editedProduct.getPrice());
        Assert.assertNotNull(editedProduct.getCategory());
        Assert.assertEquals(product.getCategory().getId(), editedProduct.getCategory().getId());

        // manually rolling back changes
        editedProduct.setSku(oldSku);
        productService.edit(editedProduct);
    }

    /**
     * Patching the price of randomly picked product via REST, then reading that product from DB and checking that our
     * change was persisted (and that nothing else but price was changed).
     */
    @Test
    public void testPatchProductPrice() {
        Product product = getRandomProduct();
        BigDecimal oldPrice = product.getPrice();
        BigDecimal newPrice = getRandomPrice();
        product.setPrice(newPrice);

        initOAuthClientWithAdminCredentials();

        ProductDTO returnedProduct = editProduct(product, HttpMethod.PATCH, product.getId());
        Assert.assertNotNull(returnedProduct.getPrice());
        Assert.assertEquals(newPrice, returnedProduct.getPrice());

        Product editedProduct = getProductById(product.getId());
        Assert.assertEquals(product.getSku(), editedProduct.getSku());
        Assert.assertEquals(product.getName(), editedProduct.getName());
        Assert.assertEquals(newPrice, editedProduct.getPrice());
        Assert.assertNotNull(editedProduct.getCategory());
        Assert.assertEquals(product.getCategory().getId(), editedProduct.getCategory().getId());

        // manually rolling back changes
        editedProduct.setPrice(oldPrice);
        productService.edit(editedProduct);
    }

    /**
     * Patching the category of randomly picked product via REST, then reading that product from DB and checking that our
     * change was persisted (and that nothing else but category was changed).
     */
    @Test
    public void testPatchProductCategory() {
        Product product = getRandomProduct();
        Category oldCategory = product.getCategory();
        Category newCategory = getCategoryOtherThan(product.getCategory());
        product.setCategory(newCategory);

        initOAuthClientWithAdminCredentials();

        ProductDTO returnedProduct = editProduct(product, HttpMethod.PATCH, product.getId());
        Assert.assertNotNull(returnedProduct.getCategory());
        Assert.assertEquals(newCategory.getId(), returnedProduct.getCategory().getId());

        Product editedProduct = getProductById(product.getId());
        Assert.assertEquals(product.getSku(), editedProduct.getSku());
        Assert.assertEquals(product.getName(), editedProduct.getName());
        Assert.assertEquals(product.getPrice(), editedProduct.getPrice());
        Assert.assertNotNull(editedProduct.getCategory());
        Assert.assertEquals(newCategory.getId(), editedProduct.getCategory().getId());

        // manually rolling back changes
        editedProduct.setCategory(oldCategory);
        productService.edit(editedProduct);
    }

    /**
     * Checking that we are getting expected response when the product is not found.
     */
    @Test
    public void testPatchNonExistingProduct() {
        long productId = getNonExistingProductId();

        initOAuthClientWithAdminCredentials();

        HttpEntity<ProductDTO> httpEntity = createHttpEntity(new ProductDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.PATCH, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when the request body is not provided in request.
     */
    @Test
    public void testPatchProductNoBody() {
        long productId = 1; // ID doesn't matter here

        initOAuthClientWithAdminCredentials();

        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<ProductDTO> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.PATCH, httpEntity, ErrorDTO.class, productId);
        // server returns status 500 and an error message saying that request body is missing
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to patch a product.
     */
    @Test
    public void testPatchProductWrongUser() {
        initOAuthClientWithUserCredentials();

        long productId = 1; // id doesn't matter
        HttpEntity<ProductDTO> httpEntity = createHttpEntity(new ProductDTO());
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.PATCH, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Deleting a product via REST and checking that it was deleted from DB.
     */
    @Test
    public void testDeleteProduct() {
        // Creating a new product only to delete it a bit later. Such approach does not modify the number of products
        // in DB after each test run, i.e. we are avoiding a situation when multiple runs of this test deplete the
        // products table, which can cause unexpected failures of other tests. Just rolling back the transaction
        // would be very convenient here, but we can't do that as explained in https://stackoverflow.com/a/46818347
        Product newProduct = new Product();
        newProduct.setSku(TestUtils.getRandomString());
        newProduct.setPrice(getRandomPrice());
        newProduct.setCategory(getRandomCategory());
        Product productToDelete = productRepository.save(newProduct);
        Assert.assertNotNull(productToDelete.getId());

        initOAuthClientWithAdminCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<Void> response = restTemplate.exchange("/products/{id}", HttpMethod.DELETE, httpEntity, Void.class, productToDelete.getId());
        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());

        Optional<Product> deletedProductOptional = productRepository.findById(productToDelete.getId());
        Assert.assertTrue(deletedProductOptional.isEmpty());
    }

    /**
     * Checking that we are getting expected response when the product is not found.
     */
    @Test
    public void testDeleteNonExistingProduct() {
        long productId = getNonExistingProductId();

        initOAuthClientWithAdminCredentials();

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.DELETE, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when non-admin user attempts to delete a product.
     */
    @Test
    public void testDeleteProductWrongUser() {
        initOAuthClientWithUserCredentials();

        long productId = 1; // id doesn't matter
        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/products/{id}", HttpMethod.DELETE, httpEntity, ErrorDTO.class, productId);
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Checking that a search query that should match a single product indeed results in one search hit.
     */
    @Test
    public void testSearchSingleProduct() {
        Product product = getRandomProduct();
        Assert.assertTrue(product.getName().length() > 1);
        // we will be searching by a part of product's name
        String searchQuery = StringUtils.left(product.getName(), product.getName().length() - 1) + "*";

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        PageRequest searchPageRequest = createDefaultSortedPageRequest();
        ResponseEntity<PageDTO<ProductDTO>> response = restTemplate.exchange(
                "/products/search?" + toQueryParamString(searchPageRequest) + "&name=" + searchQuery,
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<ProductDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        PageDTO<ProductDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);

        List<ProductDTO> restProducts = restResult.getContent();
        Assert.assertNotNull(restProducts);
        Assert.assertEquals(1, restProducts.size()); // we expect our query to match only one product

        ProductDTO restProduct = restProducts.iterator().next();
        Assert.assertNotNull(restProduct);
        Assert.assertEquals(product.getId(), restProduct.getId());
        Assert.assertNotNull(restProduct.getSku());
        Assert.assertEquals(product.getSku(), restProduct.getSku());
        Assert.assertEquals(product.getName(), restProduct.getName());
        Assert.assertNotNull(restProduct.getPrice());
        Assert.assertEquals(product.getPrice(), restProduct.getPrice());
        Assert.assertNotNull(restProduct.getCategory());
        Assert.assertEquals(product.getCategory().getId(), restProduct.getCategory().getId());
    }

    /**
     * Test for a search query that should match all products.
     */
    @Test
    public void testSearchAllProducts() {
        long dbProductCount = productRepository.count();
        String searchQuery = "*";
        validateSearchAllProducts(searchQuery, dbProductCount);
    }

    /**
     * Test for an empty search query. It should match all products.
     */
    @Test
    public void testSearchWithEmptyQuery() {
        long dbProductCount = productRepository.count();
        String searchQuery = "";
        validateSearchAllProducts(searchQuery, dbProductCount);
    }

    private void validateSearchAllProducts(String searchQuery, long dbProductCount) {
        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        PageRequest searchPageRequest = createDefaultSortedPageRequest();
        ResponseEntity<PageDTO<ProductDTO>> response = restTemplate.exchange(
                "/products/search?" + toQueryParamString(searchPageRequest) + "&name=" + searchQuery,
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<ProductDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        PageDTO<ProductDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);
        Assert.assertEquals(dbProductCount, restResult.getTotalElements());

        List<ProductDTO> restProducts = restResult.getContent();
        Assert.assertTrue(CollectionUtils.isNotEmpty(restProducts));
    }

    private long getNonExistingProductId() {
        return -42; // should be fine until we decide to use negative IDs
    }

    private PageRequest createDefaultPageRequest() {
        return PageRequest.of(0, PAGE_SIZE);
    }

    private PageRequest createDefaultSortedPageRequest() {
        return PageRequest.of(0, PAGE_SIZE, Sort.by(Sort.Direction.ASC, SORT_PROPERTY));
    }

    private <T> HttpEntity<T> createHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);

        return new HttpEntity<>(body, headers);
    }

    private void addAuthorizationHeader(HttpHeaders headers) {
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        headers.set(HttpHeaders.AUTHORIZATION, accessToken.getTokenType() + " " + accessToken.getValue());
    }

    private String toQueryParamString(Pageable pageable) {
        return String.format(
                "size=%s&page=%s&sort=%s",
                pageable.getPageSize(), pageable.getPageNumber(), pageable.getSort().toString().replaceAll(": ", ",")
        );
    }

    private Product getRandomProduct() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productsPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productsPage.isEmpty());

        Product dbProduct = TestUtils.getRandomElement(productsPage.getContent());
        Assert.assertNotNull(dbProduct.getId());

        return dbProduct;
    }

    private Category getRandomCategory() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Category> categoryPage = categoryRepository.findAll(pageRequest);
        Assert.assertFalse(categoryPage.isEmpty());

        Category category = TestUtils.getRandomElement(categoryPage.getContent());
        Assert.assertNotNull(category.getId());

        return category;
    }

    private Category getCategoryOtherThan(Category categoryToAvoid) {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Category> categoryPage = categoryRepository.findAll(pageRequest);
        Assert.assertFalse(categoryPage.isEmpty());

        Optional<Category> categoryOptional = categoryPage.get()
                .filter(cat -> !cat.getId().equals(categoryToAvoid.getId()))
                .findAny();
        Assert.assertTrue(categoryOptional.isPresent());

        return categoryOptional.get();
    }

    private BigDecimal getRandomPrice() {
        return BigDecimal.valueOf(100 + new Random().nextInt(100), 2);
    }

    private ProductDTO editProduct(Product product, HttpMethod httpMethod, Long id) {
        ProductDTO productDTO = productDTOMapper.map(product);
        HttpEntity<ProductDTO> httpEntity = createHttpEntity(productDTO);
        ResponseEntity<ProductDTO> response = restTemplate.exchange("/products/{id}", httpMethod, httpEntity, ProductDTO.class, id);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        ProductDTO returnedProduct = response.getBody();
        Assert.assertNotNull(returnedProduct);
        return returnedProduct;
    }

    private Product getProductById(Long id) {
        Optional<Product> editedProductOptional = productRepository.findById(id);
        Assert.assertFalse(editedProductOptional.isEmpty());
        return editedProductOptional.get();
    }

}
