package name.akalosha.orders.rest.controller;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.ErrorDTO;
import name.akalosha.orders.model.dto.rest.UserDTO;
import name.akalosha.orders.model.dto.rest.form.UserRegisterForm;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.repository.elastic.UserSearchRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import name.akalosha.orders.service.UserService;
import name.akalosha.orders.test.util.TestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

/**
 * Integration test for {@link UserRestController}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class UserControllerTest {

    public static final int PAGE_SIZE = 10;
    public static final String SORT_PROPERTY = "id";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSearchRepository userSearchRepository;

    @Autowired
    private UserService userService;


    @Before
    public void initOAuthClientWithAdminCredentials() {
        oauthClient = getClientBuilderWithAdminCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithAdminCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    private void initOAuthClientWithUserCredentials() {
        oauthClient = getClientBuilderWithUserCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithUserCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .setUsername(OAuthClientConfig.NON_ADMIN_USER)
                .setPassword(OAuthClientConfig.NON_ADMIN_USER_PASSWORD)
                .setPort(port);
    }

    /**
     * First we get the currently authenticated user from DB, and then we check that we can get the same user via REST.
     */
    @Test
    public void testGetUser() {
        Optional<User> dbUserOptional = userService.findByUsername(OAuthClientConfig.ADMIN_USER);
        Assert.assertTrue(dbUserOptional.isPresent());
        User dbUser = dbUserOptional.get();

        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<UserDTO> response = restTemplate.exchange("/users/{id}", HttpMethod.GET, httpEntity, UserDTO.class, dbUser.getId());
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        UserDTO restUser = response.getBody();
        Assert.assertNotNull(restUser);

        Assert.assertNotNull(restUser.getId());
        Assert.assertEquals(dbUser.getId(), restUser.getId());
        Assert.assertNotNull(restUser.getUsername());
        Assert.assertEquals(dbUser.getUsername(), restUser.getUsername());
        Assert.assertEquals(dbUser.getRoles(), restUser.getRoles());
    }

    /**
     * Authenticating as user A and trying to get the details of user B. Such access should be forbidden.
     */
    @Test
    public void testGetUserDifferentFromSelf() {
        Optional<User> currentUserOptional = userService.findByUsername(OAuthClientConfig.ADMIN_USER);
        Assert.assertTrue(currentUserOptional.isPresent());
        User currentUser = currentUserOptional.get();

        Page<User> usersPage = userRepository.findAll(createDefaultPageRequest());
        Assert.assertNotNull(usersPage);
        Assert.assertTrue(CollectionUtils.isNotEmpty(usersPage.getContent()));
        Optional<User> otherUserOptional = usersPage.getContent().stream()
                .filter(user -> !user.getId().equals(currentUser.getId()))
                .findAny();
        Assert.assertTrue(otherUserOptional.isPresent());
        User otherUser = otherUserOptional.get();

        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/users/{id}", HttpMethod.GET, httpEntity, ErrorDTO.class, otherUser.getId());
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    /**
     * Checking that we are getting expected response when the user is not found.
     */
    @Test
    public void testGetNonExistingUser() {
        long userId = getNonExistingUserId();
        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange("/users/{id}", HttpMethod.GET, httpEntity, ErrorDTO.class, userId);
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * First we get a random user from DB, and then we check that we can get the same user via REST using
     * user's name instead of id.
     */
    @Test
    public void testGetUserByUsername() {
        User dbUser = getRandomUser();

        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                "/users/get-by-username?username=" + dbUser.getUsername(),
                HttpMethod.POST,
                httpEntity,
                UserDTO.class
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        UserDTO restUser = response.getBody();
        Assert.assertNotNull(restUser);

        Assert.assertNotNull(restUser.getId());
        Assert.assertEquals(dbUser.getId(), restUser.getId());
        Assert.assertNotNull(restUser.getUsername());
        Assert.assertEquals(dbUser.getUsername(), restUser.getUsername());
        Assert.assertEquals(dbUser.getRoles(), restUser.getRoles());
    }

    /**
     * Checking that non-admin users can not use /users/get-by-username endpoint.
     */
    @Test
    public void testGetUserByUsernameNonAdmin() {
        // making sure that we would authenticate as a regular user
        initOAuthClientWithUserCredentials();

        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange(
                "/users/get-by-username?username=whatever",
                HttpMethod.POST,
                httpEntity,
                ErrorDTO.class
        );
        Assert.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
    }

    /**
     * Checking that passing a non-existing username to /users/get-by-username endpoint produces expected response status (404).
     */
    @Test
    public void testGetNonExistingUserByUsername() {
        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                "/users/get-by-username?username=" + getNonExistingUsername(),
                HttpMethod.POST,
                httpEntity,
                UserDTO.class
        );
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Authenticating as admin and checking that /current endpoint returns the same user.
     */
    @Test
    public void testGetCurrentAdminUser() {
        Optional<User> dbUserOptional = userService.findByUsername(OAuthClientConfig.ADMIN_USER);
        Assert.assertTrue(dbUserOptional.isPresent());
        User dbUser = dbUserOptional.get();

        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<UserDTO> response = restTemplate.exchange("/users/current", HttpMethod.GET, httpEntity, UserDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        UserDTO restUser = response.getBody();
        Assert.assertNotNull(restUser);

        Assert.assertNotNull(restUser.getId());
        Assert.assertEquals(dbUser.getId(), restUser.getId());
        Assert.assertNotNull(restUser.getUsername());
        Assert.assertEquals(dbUser.getUsername(), restUser.getUsername());
        Assert.assertEquals(dbUser.getRoles(), restUser.getRoles());
    }

    /**
     * Authenticating as a regular user and checking that /current endpoint returns the same user.
     */
    @Test
    public void testGetCurrentNonAdminUser() {
        Optional<User> dbUserOptional = userService.findByUsername(OAuthClientConfig.NON_ADMIN_USER);
        Assert.assertTrue(dbUserOptional.isPresent());
        User dbUser = dbUserOptional.get();

        // making sure that we would authenticate as a regular user
        initOAuthClientWithUserCredentials();

        HttpEntity<User> httpEntity = createHttpEntity(null);
        ResponseEntity<UserDTO> response = restTemplate.exchange("/users/current", HttpMethod.GET, httpEntity, UserDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        UserDTO restUser = response.getBody();
        Assert.assertNotNull(restUser);

        Assert.assertNotNull(restUser.getId());
        Assert.assertEquals(dbUser.getId(), restUser.getId());
        Assert.assertNotNull(restUser.getUsername());
        Assert.assertEquals(dbUser.getUsername(), restUser.getUsername());
        Assert.assertEquals(dbUser.getRoles(), restUser.getRoles());
    }

    /**
     * First we get a list of users from DB, then we get users from REST (using the same pagination settings)
     * and finally we are checking that both lists contain the same elements.
     */
    @Test
    public void testGetAllUsers() {
        PageRequest pageRequest = createDefaultSortedPageRequest();
        Page<User> usersPage = userRepository.findAll(pageRequest);
        Assert.assertFalse(usersPage.isEmpty());

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        ResponseEntity<PageDTO<UserDTO>> response = restTemplate.exchange(
                "/users?" + toQueryParamString(pageRequest),
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<PageDTO<UserDTO>>() {}
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        PageDTO<UserDTO> restResult = response.getBody();
        Assert.assertNotNull(restResult);

        List<User> dbUsers = usersPage.getContent();
        List<UserDTO> restUsers = restResult.getContent();
        Assert.assertNotNull(restUsers);
        Assert.assertEquals(dbUsers.size(), restUsers.size());

        // checking that we got the same users from DB and from REST API, in the same order
        for (int i = 0; i < dbUsers.size(); i++) {
            User dbUser = dbUsers.get(i);
            UserDTO restUser = restUsers.get(i);
            Assert.assertEquals(dbUser.getId(), restUser.getId());
            Assert.assertEquals(dbUser.getUsername(), restUser.getUsername());
            Assert.assertEquals(dbUser.getRoles(), restUser.getRoles());
        }
    }

    /**
     * Registering a new user and then checking that the user was created in DB.
     */
    @Test
    public void testRegisterNewUser() {
        UserRegisterForm form = new UserRegisterForm();
        String newUsername = TestUtils.getRandomString() + "@test.com";
        form.setUsername(newUsername);
        form.setPassword("whatever".toCharArray());

        // not providing authorization header, because register endpoint should be public
        HttpEntity<UserRegisterForm> httpEntity = new HttpEntity<>(form);
        ResponseEntity<UserDTO> response = restTemplate.exchange("/users/register", HttpMethod.POST, httpEntity, UserDTO.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        UserDTO restUser = response.getBody();
        Assert.assertNotNull(restUser);
        Assert.assertNotNull(restUser.getId());
        Assert.assertEquals(newUsername, restUser.getUsername());
        Assert.assertTrue(CollectionUtils.isNotEmpty(restUser.getRoles()));

        Optional<User> dbUserOptional = userRepository.findByUsername(newUsername);
        Assert.assertTrue(dbUserOptional.isPresent());
        User dbUser = dbUserOptional.get();
        Assert.assertEquals(dbUser.getId(), restUser.getId());
        Assert.assertEquals(dbUser.getUsername(), restUser.getUsername());
        Assert.assertEquals(dbUser.getRoles(), restUser.getRoles());

        // "rolling back" the DB and Elastic to previous state
        userRepository.deleteById(dbUser.getId());
        userSearchRepository.deleteById(dbUser.getId());
    }

    private long getNonExistingUserId() {
        return -42; // should be fine until we decide to use negative IDs
    }

    private PageRequest createDefaultPageRequest() {
        return PageRequest.of(0, PAGE_SIZE);
    }

    private PageRequest createDefaultSortedPageRequest() {
        return PageRequest.of(0, PAGE_SIZE, Sort.by(Sort.Direction.ASC, SORT_PROPERTY));
    }

    private <T> HttpEntity<T> createHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);

        return new HttpEntity<>(body, headers);
    }

    private void addAuthorizationHeader(HttpHeaders headers) {
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        headers.set(HttpHeaders.AUTHORIZATION, accessToken.getTokenType() + " " + accessToken.getValue());
    }

    private String toQueryParamString(Pageable pageable) {
        return String.format(
                "size=%s&page=%s&sort=%s",
                pageable.getPageSize(), pageable.getPageNumber(), pageable.getSort().toString().replaceAll(": ", ",")
        );
    }

    private User getRandomUser() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<User> usersPage = userRepository.findAll(pageRequest);
        Assert.assertFalse(usersPage.isEmpty());

        User dbUser = TestUtils.getRandomElement(usersPage.getContent());
        Assert.assertNotNull(dbUser.getId());

        return dbUser;
    }

    private String getNonExistingUsername() {
        String username;
        do {
            username = TestUtils.getRandomString();
        } while (userRepository.existsByUsername(username));

        return username;
    }

}
