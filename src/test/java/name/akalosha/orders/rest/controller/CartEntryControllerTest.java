package name.akalosha.orders.rest.controller;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.rest.CartOrderEntryDTO;
import name.akalosha.orders.model.dto.rest.ErrorDTO;
import name.akalosha.orders.model.dto.rest.ProductDTO;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import name.akalosha.orders.service.CartOrderEntryService;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.service.SiteService;
import name.akalosha.orders.test.util.TestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Integration test for {@link CartEntryRestController}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class CartEntryControllerTest implements CartOrderTest {

    public static final int PAGE_SIZE = 10;
    public static final String SORT_PROPERTY = "id";

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartOrderEntryRepository cartOrderEntryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private CartOrderEntryService cartOrderEntryService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private SiteService siteService;


    private void initOAuthClientWithAdminCredentials() {
        oauthClient = getClientBuilderWithAdminCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithAdminCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    @Before
    public void initOAuthClientWithUserCredentials() {
        oauthClient = getClientBuilderWithUserCredentials().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilderWithUserCredentials() {
        return oauthClientConfig.new ClientBuilder()
                .setUsername(OAuthClientConfig.NON_ADMIN_USER)
                .setPassword(OAuthClientConfig.NON_ADMIN_USER_PASSWORD)
                .setPort(port);
    }

    @Before
    public void ensureThatWeHaveSomeCarts() {
        if (!existsValidCart()) {
            addDummyCart(OAuthClientConfig.NON_ADMIN_USER);
        }
        Assert.assertTrue(existsValidCart());
    }

    private boolean existsValidCart() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Cart> cartsPage = cartRepository.findAll(pageRequest);
        if (!cartsPage.isEmpty()) {
            return cartsPage.stream()
                    .map(Cart::getId)
                    .map(cartService::findWithEagerFetch)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .anyMatch(this::isValidCartForTesting);
        }
        return false;
    }

    private boolean isValidCartForTesting(Cart cart) {
        return cart != null &&
                cart.getId() != null &&
                cart.getUser() != null &&
                CollectionUtils.isNotEmpty(cart.getEntries()) &&
                cart.getEntries().stream().anyMatch(this::isValidEntry);
    }

    private boolean isValidEntry(CartOrderEntry entry) {
        return entry != null &&
                entry.getId() != null &&
                entry.getQuantity() > 0 &&
                entry.getProduct() != null &&
                entry.getProduct().getId() != null &&
                entry.getProduct().getPrice() != null;
    }

    private Cart addDummyCart(String cartUserName) {
        Cart cart = createEmptyCart(cartUserName);

        Product product1 = getRandomProduct();
        CartOrderEntry entry1 = new CartOrderEntry();
        entry1.setProduct(product1);
        entry1.setQuantity(2);
        cartOrderEntryService.save(cart.getId(), entry1);

        Product product2 = getProductOtherThan(product1);
        CartOrderEntry entry2 = new CartOrderEntry();
        entry2.setProduct(product2);
        entry2.setQuantity(3);
        cartOrderEntryService.save(cart.getId(), entry2);

        Optional<Cart> cartOptional = cartService.findWithEagerFetch(cart.getId());
        Assert.assertTrue(cartOptional.isPresent());
        Assert.assertEquals(2, cartOptional.get().getEntries().size());

        return cartOptional.get();
    }

    /**
     * First we get some entry from DB, and then we check that we can get the same entry via REST.
     */
    @Test
    public void testGetEntry() {
        Cart dbCart = getRandomCart();
        CartOrderEntry dbEntry = getRandomEntry(dbCart);

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        ResponseEntity<CartOrderEntryDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries/{id}",
                HttpMethod.GET,
                httpEntity,
                CartOrderEntryDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                dbEntry.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        CartOrderEntryDTO restEntry = response.getBody();
        Assert.assertNotNull(restEntry);

        Assert.assertNotNull(restEntry.getId());
        Assert.assertEquals(dbEntry.getId(), restEntry.getId());
        Assert.assertTrue(restEntry.getQuantity() > 0);
        Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
        Assert.assertNotNull(restEntry.getCart());
        Assert.assertEquals(dbEntry.getCart().getId(), restEntry.getCart().getId());
        Assert.assertNotNull(restEntry.getProduct());
        Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
        Assert.assertNull(restEntry.getOrder());
    }

    /**
     * We get a cart withs some entries from DB, then we get that cart's entries via REST and finally we are checking
     * that both sets of entries contain the same elements.
     */
    @Test
    public void testGetAllEntries() {
        Cart dbCart = getRandomCartWithEntries();

        HttpEntity<PageRequest> httpEntity = createHttpEntity(null);
        ResponseEntity<Collection<CartOrderEntryDTO>> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries",
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<Collection<CartOrderEntryDTO>>() {},
                dbCart.getUser().getId(),
                dbCart.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Collection<CartOrderEntryDTO> restResult = response.getBody();
        Assert.assertTrue(CollectionUtils.isNotEmpty(restResult));
        Assert.assertEquals(dbCart.getEntries().size(), restResult.size());

        Map<Long, CartOrderEntry> dbEntriesMap = dbCart.getEntries().stream()
                .collect(Collectors.toMap(CartOrderEntry::getId, Function.identity()));

        // checking that we got the same entries from DB and from REST API
        for (CartOrderEntryDTO restEntry : restResult) {
            Assert.assertNotNull(restEntry);
            CartOrderEntry dbEntry = dbEntriesMap.get(restEntry.getId());
            Assert.assertEquals(dbEntry.getId(), restEntry.getId());
            Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
            Assert.assertNotNull(restEntry.getProduct());
            Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
            Assert.assertEquals(dbEntry.getProduct().getName(), restEntry.getProduct().getName());
            Assert.assertEquals(dbEntry.getProduct().getSku(), restEntry.getProduct().getSku());
            Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());
        }
    }

    /**
     * We are adding a new cart entry via REST and checking that the entry was created in DB.
     */
    @Test
    public void testAddEntry() {
        Cart dbCart = createEmptyCart(OAuthClientConfig.NON_ADMIN_USER);

        CartOrderEntryDTO entryDTO = new CartOrderEntryDTO();
        int quantity = 4;
        entryDTO.setQuantity(quantity);
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(getRandomProduct().getId());
        entryDTO.setProduct(productDTO);

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<CartOrderEntryDTO> httpEntity = createHttpEntity(entryDTO);
        ResponseEntity<CartOrderEntryDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries",
                HttpMethod.POST,
                httpEntity,
                CartOrderEntryDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        CartOrderEntryDTO restEntry = response.getBody();
        Assert.assertNotNull(restEntry.getId());
        Assert.assertEquals(quantity, restEntry.getQuantity());
        Assert.assertNotNull(restEntry.getProduct());
        Assert.assertEquals(productDTO.getId(), restEntry.getProduct().getId());

        Optional<CartOrderEntry> dbEntryOptional = cartOrderEntryService.find(dbCart.getId(), restEntry.getId());
        Assert.assertTrue(dbEntryOptional.isPresent());
        CartOrderEntry dbEntry = dbEntryOptional.get();
        Assert.assertEquals(dbEntry.getId(), restEntry.getId());
        Assert.assertEquals(dbEntry.getQuantity(), restEntry.getQuantity());
        Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
        Assert.assertEquals(dbEntry.getProduct().getName(), restEntry.getProduct().getName());
        Assert.assertEquals(dbEntry.getProduct().getSku(), restEntry.getProduct().getSku());
        Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());

        cartService.delete(dbCart.getId());
    }

    /**
     * Checking that we are getting expected response when the request body is not provided in add entry request.
     */
    @Test
    public void testAddProductNoBody() {
        Cart dbCart = createEmptyCart(OAuthClientConfig.NON_ADMIN_USER);

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        HttpEntity<CartOrderEntryDTO> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<ErrorDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries",
                HttpMethod.POST,
                httpEntity,
                ErrorDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId()
        );
        // server returns status 500 and an error message saying that request body is missing
        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());

        cartService.delete(dbCart.getId());
    }

    /**
     * We are changing cart entry quantity via REST and checking that our edit was persisted to DB.
     */
    @Test
    public void testEditEntryQuantity() {
        Cart dbCart = getRandomCartWithEntries();
        CartOrderEntry dbEntry = TestUtils.getRandomElement(dbCart.getEntries());

        int oldQuantity = dbEntry.getQuantity();
        int newQuantity = oldQuantity + 1;
        HttpEntity<Integer> httpEntity = createHttpEntity(newQuantity);
        ResponseEntity<CartOrderEntryDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries/{entryId}/quantity",
                HttpMethod.PUT,
                httpEntity,
                CartOrderEntryDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                dbEntry.getId()
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(response.getBody());

        CartOrderEntryDTO restEntry = response.getBody();
        Assert.assertNotNull(restEntry.getId());
        Assert.assertEquals(dbEntry.getId(), restEntry.getId());
        Assert.assertEquals(newQuantity, restEntry.getQuantity());
        Assert.assertNotNull(restEntry.getProduct());
        Assert.assertEquals(dbEntry.getProduct().getId(), restEntry.getProduct().getId());
        Assert.assertEquals(dbEntry.getProduct().getName(), restEntry.getProduct().getName());
        Assert.assertEquals(dbEntry.getProduct().getSku(), restEntry.getProduct().getSku());
        Assert.assertEquals(dbEntry.getProduct().getPrice(), restEntry.getProduct().getPrice());

        Optional<CartOrderEntry> dbEntryOptional = cartOrderEntryRepository.findById(dbEntry.getId());
        Assert.assertTrue(dbEntryOptional.isPresent());
        dbEntry = dbEntryOptional.get();
        Assert.assertEquals(newQuantity, dbEntry.getQuantity());

        cartOrderEntryService.editQuantity(dbCart.getId(), dbEntry.getId(), oldQuantity);
    }

    /**
     * We are attempting to set an invalid quantity (0 or negative) to entry  and checking that we get an expected response.
     */
    @Test
    public void testEditEntryQuantityInvalidQuantity() {
        testSetInvalidEntryQuantity(0);
        testSetInvalidEntryQuantity(-1);
    }

    private void testSetInvalidEntryQuantity(int newQuantity) {
        Cart dbCart = getRandomCartWithEntries();
        CartOrderEntry dbEntry = TestUtils.getRandomElement(dbCart.getEntries());

        HttpEntity<Integer> httpEntity = createHttpEntity(newQuantity);
        ResponseEntity<CartOrderEntryDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries/{entryId}/quantity",
                HttpMethod.PUT,
                httpEntity,
                CartOrderEntryDTO.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                dbEntry.getId()
        );
        Assert.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    /**
     * We are attempting to change a quantity of entry that doesn't match the cart ID that we pass in request.
     */
    @Test
    public void testEditEntryQuantityWrongCart() {
        Cart dbCart = getRandomCartWithEntries();
        CartOrderEntry dbEntry = TestUtils.getRandomElement(dbCart.getEntries());

        Cart newCart = createEmptyCart(dbCart.getUser().getUsername());

        initOAuthClientForUser(dbCart.getUser().getUsername());

        int newQuantity = 1;
        HttpEntity<Integer> httpEntity = createHttpEntity(newQuantity);
        ResponseEntity<CartOrderEntryDTO> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries/{entryId}/quantity",
                HttpMethod.PUT,
                httpEntity,
                CartOrderEntryDTO.class,
                dbCart.getUser().getId(),
                newCart.getId(), // using id of a cart that does not contain our entry
                dbEntry.getId()
        );
        Assert.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        cartService.delete(newCart.getId());
    }

    /**
     * We are deleting an entry via REST and checking that entry was deleted from DB.
     */
    @Test
    public void testDeleteEntry() {
        Cart dbCart = createEmptyCart(OAuthClientConfig.NON_ADMIN_USER);

        CartOrderEntry dbEntry = new CartOrderEntry();
        int quantity = 4;
        dbEntry.setQuantity(quantity);
        dbEntry.setProduct(getRandomProduct());
        dbEntry = cartOrderEntryService.save(dbCart.getId(), dbEntry);

        initOAuthClientForUser(dbCart.getUser().getUsername());

        HttpEntity<Void> httpEntity = createHttpEntity(null);
        Long dbEntryId = dbEntry.getId();
        ResponseEntity<Void> response = restTemplate.exchange(
                "/users/{userId}/carts/{cartId}/entries/{entryId}",
                HttpMethod.DELETE,
                httpEntity,
                Void.class,
                dbCart.getUser().getId(),
                dbCart.getId(),
                dbEntryId
        );
        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());

        Optional<CartOrderEntry> dbEntryOptional = cartOrderEntryService.find(dbCart.getId(), dbEntryId);
        Assert.assertTrue(dbEntryOptional.isEmpty());

        Optional<Cart> dbCartOptional = cartService.findWithEagerFetch(dbCart.getId());
        Assert.assertTrue(dbCartOptional.isPresent());
        Assert.assertFalse(dbCartOptional.get().getEntries().stream().anyMatch(e -> e.getId().equals(dbEntryId)));

        cartService.delete(dbCart.getId());
    }

    private PageRequest createDefaultPageRequest() {
        return PageRequest.of(0, PAGE_SIZE);
    }

    private <T> HttpEntity<T> createHttpEntity(T body) {
        HttpHeaders headers = new HttpHeaders();
        addAuthorizationHeader(headers);

        return new HttpEntity<>(body, headers);
    }

    private void addAuthorizationHeader(HttpHeaders headers) {
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        headers.set(HttpHeaders.AUTHORIZATION, accessToken.getTokenType() + " " + accessToken.getValue());
    }

    private String toQueryParamString(Pageable pageable) {
        return String.format(
                "size=%s&page=%s&sort=%s",
                pageable.getPageSize(), pageable.getPageNumber(), pageable.getSort().toString().replaceAll(": ", ",")
        );
    }

    private Product getRandomProduct() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productsPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productsPage.isEmpty());

        Product dbProduct = TestUtils.getRandomElement(productsPage.getContent());
        Assert.assertNotNull(dbProduct.getId());

        return dbProduct;
    }

    private Product getProductOtherThan(Product productToAvoid) {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Product> productPage = productRepository.findAll(pageRequest);
        Assert.assertFalse(productPage.isEmpty());

        Optional<Product> productOptional = productPage.get()
                .filter(product -> !product.getId().equals(productToAvoid.getId()))
                .findAny();
        Assert.assertTrue(productOptional.isPresent());

        return productOptional.get();
    }

    private Cart getRandomCart() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Cart> cartPage = cartRepository.findAll(pageRequest);
        Assert.assertFalse(cartPage.isEmpty());

        Cart dbCart = TestUtils.getRandomElement(cartPage.getContent());
        Optional<Cart> cartOptional = cartService.findWithEagerFetch(dbCart.getId());
        Assert.assertTrue(cartOptional.isPresent());
        dbCart = cartOptional.get();
        Assert.assertNotNull(dbCart.getId());
        Assert.assertNotNull(dbCart.getUser());
        Assert.assertNotNull(dbCart.getUser().getId());

        return dbCart;
    }

    private Cart getRandomCartWithEntries() {
        PageRequest pageRequest = createDefaultPageRequest();
        Page<Cart> cartPage = cartRepository.findAll(pageRequest);
        Assert.assertFalse(cartPage.isEmpty());

        List<Cart> validCarts = cartPage.getContent().stream()
                .map(cart -> cartRepository.findByIdWithEagerFetch(cart.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(this::isValidCartForTesting)
                .collect(Collectors.toList());
        Cart dbCart = TestUtils.getRandomElement(validCarts);
        Optional<Cart> cartOptional = cartService.findWithEagerFetch(dbCart.getId());
        Assert.assertTrue(cartOptional.isPresent());
        dbCart = cartOptional.get();
        Assert.assertNotNull(dbCart.getId());
        Assert.assertNotNull(dbCart.getUser());
        Assert.assertNotNull(dbCart.getUser().getId());

        return dbCart;
    }

    private CartOrderEntry getRandomEntry(Cart cart) {
        Assert.assertNotNull(cart);
        Assert.assertTrue(CollectionUtils.isNotEmpty(cart.getEntries()));
        List<CartOrderEntry> entries = cart.getEntries().stream()
                .filter(this::isValidEntry)
                .collect(Collectors.toList());
        Assert.assertFalse(entries.isEmpty());
        return TestUtils.getRandomElement(entries);
    }

    private boolean isAdminUser(String username) {
        Assert.assertNotNull(username);
        return username.equals(OAuthClientConfig.ADMIN_USER);
    }

    private boolean isRegularUser(String username) {
        Assert.assertNotNull(username);
        return username.equals(OAuthClientConfig.NON_ADMIN_USER);
    }

    private void initOAuthClientForUser(String username) {
        if (isAdminUser(username)) {
            initOAuthClientWithAdminCredentials();
        } else if (isRegularUser(username)) {
            initOAuthClientWithUserCredentials();
        } else {
            throw new IllegalStateException("Unable to authenticate unknown user " + username);
        }
    }

    @Override
    public DeliveryMethodRepository getDeliveryMethodRepository() {
        return deliveryMethodRepository;
    }

    @Override
    public CartService getCartService() {
        return cartService;
    }

    @Override
    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Override
    public SiteService getSiteService() {
        return siteService;
    }

}
