package name.akalosha.orders.rest;

import name.akalosha.orders.OrdersApp;
import name.akalosha.orders.config.OAuthClientConfig;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrdersApp.class)
@ActiveProfiles("integration-test")
public class OAuthAuthenticationTest {

    private static final String TOKEN_TYPE = "bearer";

    @LocalServerPort
    private int port;

    @Autowired
    private OAuthClientConfig oauthClientConfig;

    private OAuth2RestOperations oauthClient;

    @Before
    public void initOAuthClient() {
        oauthClient = getClientBuilder().build();
    }

    private OAuthClientConfig.ClientBuilder getClientBuilder() {
        return oauthClientConfig.new ClientBuilder()
                .withAdminCredentials()
                .setPort(port);
    }

    @Test
    public void testPasswordAuthentication() {
        // at this point oAuth2RestOperations is configured with admin credentials and a token request should succeed
        OAuth2AccessToken accessToken = oauthClient.getAccessToken();
        Assert.assertNotNull(accessToken);
        Assert.assertTrue(StringUtils.isNotBlank(accessToken.getValue()));
        Assert.assertTrue(StringUtils.isNotBlank(accessToken.getTokenType()));
        Assert.assertEquals(TOKEN_TYPE, accessToken.getTokenType());
    }

    @Test(expected = OAuth2AccessDeniedException.class)
    public void testPasswordAuthenticationFailure() {
        // here we are using random UUID as a way to get a user name that is guaranteed to not exist
        OAuth2RestOperations oAuth2RestOperations = getClientBuilder()
                .setUsername(UUID.randomUUID().toString())
                .build();
        oAuth2RestOperations.getAccessToken();
    }

}
