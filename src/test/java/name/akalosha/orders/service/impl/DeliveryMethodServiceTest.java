package name.akalosha.orders.service.impl;

import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.service.DeliveryMethodService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class DeliveryMethodServiceTest {

    private DeliveryMethodService deliveryMethodService;

    @Mock
    private DeliveryMethodRepository deliveryMethodRepository;

    @Before
    public void before() {
        deliveryMethodService = new DeliveryMethodServiceImpl(deliveryMethodRepository);
    }

    @Test
    public void testGetDeliveryMethodsForSite() {
        Site site = mock(Site.class);
        when(deliveryMethodRepository.findBySite(site)).thenReturn(Collections.singletonList(mock(DeliveryMethod.class)));

        List<DeliveryMethod> deliveryMethods = deliveryMethodService.getDeliveryMethodsForSite(site);
        Assert.assertTrue(CollectionUtils.isNotEmpty(deliveryMethods));
    }

    @Test
    public void testNoDeliveryMethodsForSite() {
        Site site = mock(Site.class);
        when(deliveryMethodRepository.findBySite(site)).thenReturn(Collections.emptyList());

        List<DeliveryMethod> deliveryMethods = deliveryMethodService.getDeliveryMethodsForSite(site);
        Assert.assertNotNull(deliveryMethods);
        Assert.assertTrue(deliveryMethods.isEmpty());
    }

}
