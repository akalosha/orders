package name.akalosha.orders.service.impl;

import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.repository.jpa.SiteRepository;
import name.akalosha.orders.service.SiteService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class SiteServiceTest {

    private SiteService siteService;

    @Mock
    private SiteRepository siteRepository;

    @Before
    public void before() {
        siteService = new SiteServiceImpl(siteRepository);
    }

    @Test
    public void testResolveCurrentSiteFromRequestAttribute() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        Site site = mock(Site.class);
        when(request.getAttribute(SiteService.CURRENT_SITE_ATTR)).thenReturn(site);

        Site resolvedSite = siteService.resolveCurrentSite(request);
        Assert.assertNotNull(resolvedSite);
        Assert.assertEquals(site, resolvedSite);
    }

    @Test
    public void testResolveCurrentSiteFromHostName() {
        testResolveCurrentSiteFromUrl("https://1.example.com/", "site=42Site");
    }

    @Test
    public void testResolveCurrentSiteFromQueryParams() {
        testResolveCurrentSiteFromUrl("https://42.example.com/", "site=1Site");
    }

    private void testResolveCurrentSiteFromUrl(String requestUrl, String queryString) {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getAttribute(SiteService.CURRENT_SITE_ATTR)).thenReturn(null);
        when(request.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        when(request.getQueryString()).thenReturn(queryString);

        Site site1 = mock(Site.class);
        when(site1.getName()).thenReturn("1Site");
        when(site1.getUrlPatterns()).thenReturn(Collections.singletonList("https?://1.example.com.*|.*site=1Site.*"));
        Site site2 = mock(Site.class);
        when(site2.getName()).thenReturn("2Site");
        when(site2.getUrlPatterns()).thenReturn(Collections.singletonList("https?://2.example.com.*|.*site=2Site.*"));
        when(siteRepository.findAll()).thenReturn(Arrays.asList(site1, site2));

        Site resolvedSite = siteService.resolveCurrentSite(request);
        Assert.assertNotNull(resolvedSite);
        Assert.assertEquals(site1, resolvedSite);
    }

    @Test(expected = IllegalStateException.class)
    public void testFailedSiteResolution() {
        testResolveCurrentSiteFromUrl("https://42.example.com/", "site=42Site");
    }

    @Test
    public void testFindSiteByName() {
        Site site = mock(Site.class);
        String siteName = "site1";
        when(site.getName()).thenReturn(siteName);
        when(siteRepository.findSiteByName(siteName)).thenReturn(Optional.of(site));

        Optional<Site> siteOptional = siteService.findSiteByName(siteName);
        Assert.assertTrue(siteOptional.isPresent());
        Assert.assertEquals(siteName, siteOptional.get().getName());
    }

    @Test
    public void testFindSiteByNameNoResult() {
        Site site = mock(Site.class);
        String siteName = "site1";
        when(site.getName()).thenReturn(siteName);
        when(siteRepository.findSiteByName(siteName)).thenReturn(Optional.of(site));

        Optional<Site> siteOptional = siteService.findSiteByName("site42");
        Assert.assertTrue(siteOptional.isEmpty());
    }

}
