package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.UserDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.UserUpdater;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.UserRegisterForm;
import name.akalosha.orders.model.dto.search.UserDoc;
import name.akalosha.orders.repository.elastic.UserSearchRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class UserServiceTest {

    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserSearchRepository userSearchRepository;

    @Mock
    private UserDocMapper userDocMapper;

    @Mock
    private UserUpdater userUpdater;

    @Mock
    private Supplier<org.springframework.security.core.userdetails.User> authenticatedUserSupplier;

    @Mock
    private ObjectProvider<PasswordEncoder> passwordEncoderProvider;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void before() {
        when(passwordEncoderProvider.getIfAvailable()).thenReturn(passwordEncoder);
        when(passwordEncoder.encode(any())).thenAnswer((Answer<String>) invocation -> {
            CharSequence argument = invocation.getArgument(0);
            // emulating some encoding done to a plain password
            return encodePassword(argument.toString());
        });

        userService = new UserServiceImpl(
                userRepository,
                userSearchRepository,
                userDocMapper,
                userUpdater,
                authenticatedUserSupplier,
                passwordEncoderProvider
        );
    }

    private static String encodePassword(String password) {
        return Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSaveUser() {
        userService.save(mock(User.class));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testEditUser() {
        userService.edit(mock(User.class));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testPatchUser() {
        userService.patch(mock(User.class));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testDeleteUser() {
        userService.delete(1L);
    }

    @Test
    public void testFindUserById() {
        Long userId = 1L;
        User userToReturn = mock(User.class);
        when(userRepository.findById(userId)).thenReturn(Optional.of(userToReturn));
        when(userToReturn.getId()).thenReturn(userId);

        Optional<User> userOptional = userService.find(userId);
        Assert.assertNotNull(userOptional);
        Assert.assertTrue(userOptional.isPresent());
        Assert.assertEquals(userId, userOptional.get().getId());
    }

    @Test
    public void testFindUserByNotExistingId() {
        Long userId = -1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        Optional<User> userOptional = userService.find(userId);
        Assert.assertNotNull(userOptional);
        Assert.assertTrue(userOptional.isEmpty());
    }

    @Test
    public void testFindByUsername() {
        String userName = "whoever";
        User userToReturn = mock(User.class);
        when(userRepository.findByUsername(userName)).thenReturn(Optional.of(userToReturn));
        when(userToReturn.getUsername()).thenReturn(userName);

        Optional<User> userOptional = userService.findByUsername(userName);
        Assert.assertNotNull(userOptional);
        Assert.assertTrue(userOptional.isPresent());
        Assert.assertEquals(userName, userOptional.get().getUsername());
    }

    @Test()
    public void testFindByNotExistingUsername() {
        Optional<User> userOptional = userService.findByUsername("whoever");
        Assert.assertNotNull(userOptional);
        Assert.assertTrue(userOptional.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByNullUsername() {
        userService.findByUsername(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByEmptyUsername() {
        userService.findByUsername("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByBlankUsername() {
        userService.findByUsername(" ");
    }

    @Test
    public void testGetCurrentUser() {
        String userName = "whoever";
        org.springframework.security.core.userdetails.User springUser = mock(org.springframework.security.core.userdetails.User.class);
        when(springUser.getUsername()).thenReturn(userName);
        when(authenticatedUserSupplier.get()).thenReturn(springUser);

        User userToReturn = mock(User.class);
        when(userRepository.findByUsername(userName)).thenReturn(Optional.of(userToReturn));
        when(userToReturn.getUsername()).thenReturn(userName);

        User currentUser = userService.getCurrentUser();
        Assert.assertNotNull(currentUser);
        Assert.assertEquals(userName, currentUser.getUsername());
    }

    @Test
    public void testFindAllUsers() {
        UserDoc userDoc1 = mock(UserDoc.class);
        UserDoc userDoc2 = mock(UserDoc.class);
        List<UserDoc> userDocs = Arrays.asList(userDoc1, userDoc2);
        Page<UserDoc> searchResult = new PageImpl<>(userDocs, mock(Pageable.class), userDocs.size());
        when(userSearchRepository.findAll(any(Pageable.class))).thenReturn(searchResult);

        Page<User> users = userService.findAll(mock(Pageable.class));
        Assert.assertFalse(users.isEmpty());
        Assert.assertFalse(CollectionUtils.isEmpty(users.getContent()));
        Assert.assertEquals(userDocs.size(), users.getContent().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterNewUserNullForm() {
        userService.register(null);
    }

    @Test
    public void testRegisterNewUser() {
        UserRegisterForm form = new UserRegisterForm();
        String username = "whoever";
        form.setUsername(username);
        String password = "password";
        form.setPassword(password.toCharArray());

        when(userRepository.save(any(User.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        User registeredUser = userService.register(form);
        Assert.assertNotNull(registeredUser);
        Assert.assertEquals(username, registeredUser.getUsername());
        Assert.assertEquals(password, decodePassword(registeredUser.getPassword()));
        Assert.assertEquals(UserServiceImpl.DEFAULT_ROLES, registeredUser.getRoles());
    }

    private static String decodePassword(String password) {
        return new String(Base64.getDecoder().decode(password));
    }

}
