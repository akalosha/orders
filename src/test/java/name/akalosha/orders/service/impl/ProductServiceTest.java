package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.ProductDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.ProductUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.search.ProductDoc;
import name.akalosha.orders.repository.elastic.ProductSearchRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import name.akalosha.orders.service.CategoryService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class ProductServiceTest {

    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductSearchRepository productSearchRepository;

    @Mock
    private ProductDocMapper productDocMapper;

    @Mock
    private ProductUpdater productUpdater;
    
    @Mock
    private CategoryService categoryService;

    @Before
    public void before() {
        when(productDocMapper.map(any(ProductDoc.class))).thenAnswer(invocation -> {
            ProductDoc productDoc = invocation.getArgument(0);
            Product product = new Product();
            product.setId(productDoc.getId());
            return product;
        });

        productService = new ProductServiceImpl(
                productRepository, 
                productSearchRepository, 
                productDocMapper, 
                productUpdater, 
                categoryService
        );
    }

    @Test
    public void testFindProductById() {
        Long productId = 1L;
        Product productToReturn = mock(Product.class);
        when(productRepository.findById(productId)).thenReturn(Optional.of(productToReturn));
        when(productToReturn.getId()).thenReturn(productId);

        Optional<Product> productOptional = productService.find(productId);
        Assert.assertNotNull(productOptional);
        Assert.assertTrue(productOptional.isPresent());
        Assert.assertEquals(productId, productOptional.get().getId());
    }

    @Test
    public void testFindProductByNotExistingId() {
        Long productId = -1L;
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        Optional<Product> productOptional = productService.find(productId);
        Assert.assertNotNull(productOptional);
        Assert.assertTrue(productOptional.isEmpty());
    }

    @Test
    public void testFindAllProducts() {
        ProductDoc productDoc1 = mock(ProductDoc.class);
        when(productDoc1.getId()).thenReturn(Long.valueOf(1));
        ProductDoc productDoc2 = mock(ProductDoc.class);
        when(productDoc2.getId()).thenReturn(Long.valueOf(2));
        List<ProductDoc> productDocs = Arrays.asList(productDoc1, productDoc2);
        Page<ProductDoc> searchResult = new PageImpl<>(productDocs, mock(Pageable.class), productDocs.size());
        when(productSearchRepository.findAll(any(Pageable.class))).thenReturn(searchResult);

        Page<Product> productPage = productService.findAll(mock(Pageable.class));
        Assert.assertNotNull(productPage);
        Assert.assertFalse(productPage.isEmpty());
        Assert.assertFalse(CollectionUtils.isEmpty(productPage.getContent()));
        Assert.assertEquals(productDocs.size(), productPage.getContent().size());

        for (int i = 0; i < productDocs.size(); i++) {
            ProductDoc productDoc = productDocs.get(i);
            Product product = productPage.getContent().get(i);
            Assert.assertNotNull(product);
            Assert.assertEquals(productDoc.getId(), product.getId());
        }
    }

    @Test
    public void testAddNewProduct() {
        String productName = "product";
        Product productToSave = mock(Product.class);
        when(productToSave.getName()).thenReturn(productName);
        when(productRepository.save(productToSave)).thenAnswer(invocation -> {
            Product argument = invocation.getArgument(0);
            argument.setId(1L);
            return argument;
        });

        Long categoryId = 1L;
        Category category = mock(Category.class);
        when(category.getId()).thenReturn(categoryId);
        when(productToSave.getCategory()).thenReturn(category);
        when(categoryService.find(categoryId)).thenReturn(Optional.of(category));

        Product savedProduct = productService.save(productToSave);
        Assert.assertNotNull(savedProduct);
        Assert.assertEquals(productName, savedProduct.getName());
        Assert.assertNotNull(savedProduct.getId());
    }

    @Test
    public void testEditProduct() {
        Mockito.doAnswer(
                AdditionalAnswers.answerVoid(
                        (Product argument0, Product argument1) -> {
                            argument1.setId(argument0.getId());
                            argument1.setSku(argument0.getSku());
                            argument1.setName(argument0.getName());
                            argument1.setPrice(argument0.getPrice());
                            argument1.setCategory(argument0.getCategory());
                        }
                )
        ).when(productUpdater).update(any(Product.class), any(Product.class));
        testEditAndPatch(productService::edit);
    }

    @Test
    public void testPatchProduct() {
        Mockito.doAnswer(
                AdditionalAnswers.answerVoid(
                        (Product argument0, Product argument1) -> {
                            argument1.setId(argument0.getId());
                            argument1.setSku(argument0.getSku());
                            argument1.setName(argument0.getName());
                            argument1.setPrice(argument0.getPrice());
                            argument1.setCategory(argument0.getCategory());
                        }
                )
        ).when(productUpdater).updateNotNull(any(Product.class), any(Product.class));
        testEditAndPatch(productService::patch);
    }

    private void testEditAndPatch(UnaryOperator<Product> editOrPatchFunction) {
        Product productToSave = mock(Product.class);
        Long productId = 1L;
        when(productToSave.getId()).thenReturn(productId);
        String newProductSku = "sku";
        when(productToSave.getSku()).thenReturn(newProductSku);
        String newProductName = "newProduct";
        when(productToSave.getName()).thenReturn(newProductName);
        BigDecimal newProductPrice = new BigDecimal("3.14");
        when(productToSave.getPrice()).thenReturn(newProductPrice);


        String existingProductName = "existingProduct";
        Product productFromDb = mock(Product.class, Mockito.CALLS_REAL_METHODS);
        productFromDb.setId(productId);
        productFromDb.setName(existingProductName);

        Long categoryId = 1L;
        Category category = mock(Category.class);
        when(category.getId()).thenReturn(categoryId);
        when(productToSave.getCategory()).thenReturn(category);
        when(categoryService.find(categoryId)).thenReturn(Optional.of(category));

        when(productRepository.findById(productId)).thenReturn(Optional.of(productFromDb));
        when(productRepository.save(any(Product.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        Product editedProduct = editOrPatchFunction.apply(productToSave);
        Assert.assertNotNull(editedProduct);
        Assert.assertEquals(productId, editedProduct.getId());
        Assert.assertEquals(newProductSku, editedProduct.getSku());
        Assert.assertEquals(newProductName, editedProduct.getName());
        Assert.assertEquals(newProductPrice, editedProduct.getPrice());
        Assert.assertNotNull(editedProduct.getCategory());
        Assert.assertEquals(categoryId, editedProduct.getCategory().getId());
    }

    @Test(expected = NotFoundException.class)
    public void testEditNotExistingProduct() {
        testEditOrPatchNotExistingProduct(productService::edit);
    }

    @Test(expected = NotFoundException.class)
    public void testPatchNotExistingProduct() {
        testEditOrPatchNotExistingProduct(productService::patch);
    }

    private void testEditOrPatchNotExistingProduct(UnaryOperator<Product> editOrPatchFunction) {
        Long productId = 1L;
        Product productToSave = mock(Product.class);
        when(productToSave.getId()).thenReturn(productId);

        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        editOrPatchFunction.apply(productToSave);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEditProductWithNoId() {
        testEditOrPatchProductWithNoId(productService::edit);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPatchProductWithNoId() {
        testEditOrPatchProductWithNoId(productService::patch);
    }

    private void testEditOrPatchProductWithNoId(UnaryOperator<Product> editOrPatchFunction) {
        Product productToSave = mock(Product.class);
        when(productToSave.getId()).thenReturn(null);

        editOrPatchFunction.apply(productToSave);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEditProductWithNoCategory() {
        testEditOrPatchProductWithNoCategory(productService::edit);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPatchProductWithNoCategory() {
        testEditOrPatchProductWithNoCategory(productService::patch);
    }

    private void testEditOrPatchProductWithNoCategory(UnaryOperator<Product> editOrPatchFunction) {
        Product productToSave = mock(Product.class);
        Long productId = 1L;
        when(productToSave.getId()).thenReturn(productId);

        Product productFromDb = mock(Product.class, Mockito.CALLS_REAL_METHODS);

        when(productToSave.getCategory()).thenReturn(null);

        when(productRepository.findById(productId)).thenReturn(Optional.of(productFromDb));
        Mockito.doAnswer(
                AdditionalAnswers.answerVoid(
                        (Product argument0, Product argument1) -> {
                            argument1.setCategory(argument0.getCategory());
                        }
                )
        ).when(productUpdater).update(any(Product.class), any(Product.class));

        editOrPatchFunction.apply(productToSave);
    }

    @Test(expected = NotFoundException.class)
    public void testEditProductWithNotExistingCategory() {
        Mockito.doAnswer(
                AdditionalAnswers.answerVoid(
                        (Product argument0, Product argument1) -> {
                            argument1.setCategory(argument0.getCategory());
                        }
                )
        ).when(productUpdater).update(any(Product.class), any(Product.class));
        testEditOrPatchProductWithNotExistingCategory(productService::edit);
    }

    @Test(expected = NotFoundException.class)
    public void testPatchProductWithNotExistingCategory() {
        Mockito.doAnswer(
                AdditionalAnswers.answerVoid(
                        (Product argument0, Product argument1) -> {
                            argument1.setCategory(argument0.getCategory());
                        }
                )
        ).when(productUpdater).updateNotNull(any(Product.class), any(Product.class));
        testEditOrPatchProductWithNotExistingCategory(productService::patch);
    }

    private void testEditOrPatchProductWithNotExistingCategory(UnaryOperator<Product> editOrPatchFunction) {
        Product productToSave = mock(Product.class);
        Long productId = 1L;
        when(productToSave.getId()).thenReturn(productId);

        Product productFromDb = mock(Product.class, Mockito.CALLS_REAL_METHODS);

        Long categoryId = 1L;
        Category category = mock(Category.class);
        when(category.getId()).thenReturn(categoryId);
        when(productToSave.getCategory()).thenReturn(category);
        when(categoryService.find(categoryId)).thenReturn(Optional.empty());

        when(productRepository.findById(productId)).thenReturn(Optional.of(productFromDb));
        editOrPatchFunction.apply(productToSave);
    }

    @Test
    public void tetDeleteProduct() {
        long productId = 1L;
        Product product = mock(Product.class);
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        productService.delete(productId);
        verify(productSearchRepository).deleteById(productId);
        verify(productRepository).deleteById(productId);
    }

    @Test(expected = NotFoundException.class)
    public void tetDeleteNotExistingProduct() {
        long productId = 1L;
        productService.delete(productId);
    }

    @Test
    public void testSearchProduct() {
        ProductDoc productDoc1 = mock(ProductDoc.class);
        when(productDoc1.getId()).thenReturn(Long.valueOf(1));
        ProductDoc productDoc2 = mock(ProductDoc.class);
        when(productDoc2.getId()).thenReturn(Long.valueOf(2));
        List<ProductDoc> productDocs = Arrays.asList(productDoc1, productDoc2);
        Page<ProductDoc> searchResult = new PageImpl<>(productDocs, mock(Pageable.class), productDocs.size());
        when(productSearchRepository.findProductDocsByName(any(Pageable.class), any(String.class))).thenReturn(searchResult);

        Page<Product> productPage = productService.findByName(mock(Pageable.class), "whatever");
        Assert.assertNotNull(productPage);
        Assert.assertFalse(productPage.isEmpty());
        Assert.assertFalse(CollectionUtils.isEmpty(productPage.getContent()));
        Assert.assertEquals(productDocs.size(), productPage.getContent().size());

        for (int i = 0; i < productDocs.size(); i++) {
            ProductDoc productDoc = productDocs.get(i);
            Product product = productPage.getContent().get(i);
            Assert.assertNotNull(product);
            Assert.assertEquals(productDoc.getId(), product.getId());
        }
    }

}
