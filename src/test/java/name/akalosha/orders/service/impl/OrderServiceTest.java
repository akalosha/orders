package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.OrderDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.OrderUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.report.RevenueReport;
import name.akalosha.orders.model.dto.search.OrderDoc;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.elastic.OrderSearchRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.OrderRepository;
import name.akalosha.orders.util.DateTimeUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class OrderServiceTest {

    private static final int PAGE_SIZE = 5;
    public static final int PAGE_NUMBER = 1;

    private OrderServiceImpl orderService;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private CartRepository cartRepository;

    @MockBean
    private CartOrderEntryRepository cartOrderEntryRepository;

    @MockBean
    private OrderSearchRepository orderSearchRepository;

    @MockBean
    private CartSearchRepository cartSearchRepository;

    @MockBean
    private OrderDocMapper orderDocMapper;

    @MockBean
    private OrderUpdater orderUpdater;

    @MockBean
    private Order order;

    @MockBean
    private OrderDoc orderDoc;

    @MockBean
    private Cart cart;

    @MockBean
    private User user;

    @Before
    public void before() {
        orderService = new OrderServiceImpl(
                orderRepository,
                cartRepository,
                orderSearchRepository,
                cartSearchRepository,
                orderDocMapper,
                cartOrderEntryRepository,
                orderUpdater
        );

        when(orderRepository.findById(ArgumentMatchers.longThat((arg) -> arg > 0))).thenReturn(Optional.of(order));
        when(orderRepository.save(ArgumentMatchers.argThat((arg) -> arg != null && arg.getId() != null))).thenReturn(order); // update order case

        when(orderDocMapper.mapReverse(any(Order.class))).thenReturn(orderDoc);
        when(orderDocMapper.map(any(OrderDoc.class))).thenReturn(order);
    }

    @Test
    public void testFindOrderById() {
        Long orderId = 1L;
        when(order.getId()).thenReturn(orderId);
        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));

        Optional<Order> orderOptional = orderService.find(orderId);
        Assert.assertTrue(orderOptional.isPresent());
        Assert.assertEquals(orderId, orderOptional.get().getId());
    }

    @Test
    public void testFindAllOrders() {
        OrderDoc orderDoc1 = mock(OrderDoc.class);
        OrderDoc orderDoc2 = mock(OrderDoc.class);
        List<OrderDoc> orderDocs = Arrays.asList(orderDoc1, orderDoc2);
        Page<OrderDoc> searchResult = new PageImpl<>(orderDocs, mock(Pageable.class), orderDocs.size());
        when(orderSearchRepository.findAll(any(Pageable.class))).thenReturn(searchResult);

        Page<Order> orders = orderService.findAll(mock(Pageable.class));
        Assert.assertFalse(orders.isEmpty());
        Assert.assertFalse(CollectionUtils.isEmpty(orders.getContent()));
        Assert.assertEquals(orderDocs.size(), orders.getContent().size());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCreateOrder() {
        orderService.save(order);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testEditOrder() {
        orderService.edit(order);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testPatchOrder() {
        orderService.patch(order);
    }

    @Test
    public void tetDeleteOrder() {
        long orderId = 1L;
        orderService.delete(orderId);
        verify(orderSearchRepository).deleteById(orderId);
        verify(orderRepository).deleteById(orderId);
    }

    @Test(expected = NotFoundException.class)
    public void tetDeleteNotExistingOrder() {
        orderService.delete(-1L);
    }

    @Test(expected = NotFoundException.class)
    public void testPlaceNotExistingOrder() {
        orderService.placeOrder(-1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPlaceOrderNoItems() {
        when(cartRepository.findById(1L)).thenReturn(Optional.of(cart));
        when(order.getEntries()).thenReturn(Collections.emptyList());
        orderService.placeOrder(1L);
    }

    @Test
    public void testPlaceOrder() {
        String username = "test@example.com";
        when(user.getUsername()).thenReturn(username);
        when(cart.getUser()).thenReturn(user);
        when(cart.getEntries()).thenReturn(Collections.singletonList(mock(CartOrderEntry.class)));
        long cartId = 1L;
        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cart));
        when(cart.getShippingAddress()).thenReturn(mock(Address.class));
        when(cart.getDeliveryMethod()).thenReturn(mock(DeliveryMethod.class));

        Order order = orderService.placeOrder(cartId);

        Assert.assertNotNull(order);
        Assert.assertNotNull(order.getDatePlaced());
        Assert.assertNotNull(order.getUser());
        Assert.assertEquals(username, order.getUser().getUsername());

        verify(cartRepository).deleteById(cartId);
        verify(cartSearchRepository).deleteById(cartId);
        verify(orderRepository).save(order);
        verify(orderSearchRepository).save(any(OrderDoc.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPlaceOrderNoShippingAddress() {
        when(cart.getEntries()).thenReturn(Collections.singletonList(mock(CartOrderEntry.class)));
        long cartId = 1L;
        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cart));

       orderService.placeOrder(cartId);
    }

    @Test
    public void testRevenueReport() {
        TemporalAccessor dateTime = DateTimeUtil.DATE_TIME_FORMATTER.parse("2019-03-23T11:19:04.356+00:00");

        Order order1 = mock(Order.class);
        when(order1.getDatePlaced()).thenReturn(DateTimeUtil.toTimestamp(ZonedDateTime.from(dateTime)));
        when(order1.getTotal()).thenReturn(new BigDecimal(6));

        Order order2 = mock(Order.class);
        when(order2.getDatePlaced()).thenReturn(DateTimeUtil.toTimestamp(ZonedDateTime.from(dateTime)));
        when(order2.getTotal()).thenReturn(new BigDecimal(94));

        when(orderRepository.findByDatePlacedAfter(any(Timestamp.class))).thenReturn(Arrays.asList(order1, order2));

        LocalDate sinceDate = LocalDate.from(dateTime);
        RevenueReport report = orderService.generateRevenueReport(sinceDate);

        Assert.assertNotNull(report);
        Assert.assertTrue(MapUtils.isNotEmpty(report.getRevenueByDay()));
        Assert.assertEquals(1, report.getRevenueByDay().size());
        Assert.assertTrue(report.getRevenueByDay().entrySet().stream().allMatch(e -> e.getKey().isEqual(sinceDate) || e.getKey().isAfter(sinceDate)));
        Assert.assertEquals(new BigDecimal(100), report.getRevenueByDay().entrySet().iterator().next().getValue());
    }

    @Test
    public void testFindOrdersByProductName() {
        OrderDoc orderDoc1 = mock(OrderDoc.class);
        OrderDoc orderDoc2 = mock(OrderDoc.class);
        List<OrderDoc> orderDocs = Arrays.asList(orderDoc1, orderDoc2);
        Page<OrderDoc> orderDocPage = mockPaginatedResponse(orderDocs);

        when(orderSearchRepository.findAllByProductNameContaining(any(Pageable.class), any(String.class)))
                .thenReturn(orderDocPage);

        String productName = "whatever";
        Pageable pageable = mock(Pageable.class);
        Page<Order> resultingOrderPage = orderService.findOrdersByProductName(pageable, productName);

        Assert.assertNotNull(resultingOrderPage);
        Assert.assertTrue(CollectionUtils.isNotEmpty(resultingOrderPage.getContent()));
        Assert.assertEquals(orderDocs.size(), resultingOrderPage.getNumberOfElements());
        Assert.assertEquals(orderDocs.size(), resultingOrderPage.getContent().size());
        verify(orderSearchRepository).findAllByProductNameContaining(pageable, productName);
    }

    private Page<OrderDoc> mockPaginatedResponse(List<OrderDoc> orderDocs) {
        return new PageImpl<>(orderDocs, PageRequest.of(PAGE_NUMBER, PAGE_SIZE, Sort.unsorted()), orderDocs.size());
    }

    @Test
    public void testOrderHistory() {
        OrderDoc orderDoc1 = mock(OrderDoc.class);
        OrderDoc orderDoc2 = mock(OrderDoc.class);
        List<OrderDoc> orderDocs = Arrays.asList(orderDoc1, orderDoc2);
        Page<OrderDoc> orderDocPage = mockPaginatedResponse(orderDocs);

        when(orderSearchRepository.findAllByUser(any(Pageable.class), any(String.class))).thenReturn(orderDocPage);

        String userName = "whoever";
        User user = mock(User.class);
        when(user.getUsername()).thenReturn(userName);
        Pageable pageable = mock(Pageable.class);
        Page<Order> resultingOrderPage = orderService.getOrderHistory(pageable, user);

        Assert.assertNotNull(resultingOrderPage);
        Assert.assertTrue(CollectionUtils.isNotEmpty(resultingOrderPage.getContent()));
        Assert.assertEquals(orderDocs.size(), resultingOrderPage.getNumberOfElements());
        Assert.assertEquals(orderDocs.size(), resultingOrderPage.getContent().size());
        verify(orderSearchRepository).findAllByUser(pageable, userName);
    }

}
