package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.CategoryDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.CategoryUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.search.CategoryDoc;
import name.akalosha.orders.repository.elastic.CategorySearchRepository;
import name.akalosha.orders.repository.jpa.CategoryRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class CategoryServiceTest {

    private CategoryServiceImpl categoryService;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private CategorySearchRepository categorySearchRepository;

    @Mock
    private CategoryDocMapper categoryDocMapper;

    @Mock
    private CategoryUpdater categoryUpdater;

    @Before
    public void before() {
        categoryService = new CategoryServiceImpl(categoryRepository, categorySearchRepository, categoryDocMapper, categoryUpdater);
    }

    @Test
    public void testFindCategoryById() {
        Long categoryId = 1L;
        Category categoryToReturn = mock(Category.class);
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(categoryToReturn));
        when(categoryToReturn.getId()).thenReturn(categoryId);

        Optional<Category> categoryOptional = categoryService.find(categoryId);
        Assert.assertNotNull(categoryOptional);
        Assert.assertTrue(categoryOptional.isPresent());
        Assert.assertEquals(categoryId, categoryOptional.get().getId());
    }

    @Test
    public void testFindCategoryByNotExistingId() {
        Long categoryId = -1L;
        when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());

        Optional<Category> categoryOptional = categoryService.find(categoryId);
        Assert.assertNotNull(categoryOptional);
        Assert.assertTrue(categoryOptional.isEmpty());
    }

    @Test
    public void testFindAllCategories() {
        CategoryDoc categoryDoc1 = mock(CategoryDoc.class);
        CategoryDoc categoryDoc2 = mock(CategoryDoc.class);
        List<CategoryDoc> categoryDocs = Arrays.asList(categoryDoc1, categoryDoc2);
        Page<CategoryDoc> searchResult = new PageImpl<>(categoryDocs, mock(Pageable.class), categoryDocs.size());
        when(categorySearchRepository.findAll(any(Pageable.class))).thenReturn(searchResult);

        Page<Category> categories = categoryService.findAll(mock(Pageable.class));
        Assert.assertFalse(categories.isEmpty());
        Assert.assertFalse(CollectionUtils.isEmpty(categories.getContent()));
        Assert.assertEquals(categoryDocs.size(), categories.getContent().size());
    }

    @Test
    public void testAddNewCategory() {
        String categoryName = "category";
        Category categoryToSave = mock(Category.class);
        when(categoryToSave.getName()).thenReturn(categoryName);
        when(categoryRepository.save(categoryToSave)).thenAnswer(invocation -> {
            Category argument = invocation.getArgument(0);
            argument.setId(1L);
            return argument;
        });

        Category savedCategory = categoryService.save(categoryToSave);
        Assert.assertNotNull(savedCategory);
        Assert.assertEquals(categoryName, savedCategory.getName());
        Assert.assertNotNull(savedCategory.getId());
    }

    @Test
    public void testEditCategory() {
        Long categoryId = 1L;
        String newCategoryName = "newCategory";
        Category categoryToSave = mock(Category.class);
        when(categoryToSave.getId()).thenReturn(categoryId);
        when(categoryToSave.getName()).thenReturn(newCategoryName);

        String existingCategoryName = "existingCategory";
        Category categoryFromDb = mock(Category.class, Mockito.CALLS_REAL_METHODS);
        categoryFromDb.setId(categoryId);
        categoryFromDb.setName(existingCategoryName);

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(categoryFromDb));
        Mockito.doAnswer(
                AdditionalAnswers.answerVoid(
                        (Category argument0, Category argument1) -> {
                            argument1.setId(argument0.getId());
                            argument1.setName(argument0.getName());
                        }
                )
        ).when(categoryUpdater).update(any(Category.class), any(Category.class));
        when(categoryRepository.save(any(Category.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        Category editedCategory = categoryService.edit(categoryToSave);
        Assert.assertNotNull(editedCategory);
        Assert.assertEquals(categoryId, editedCategory.getId());
        Assert.assertEquals(newCategoryName, editedCategory.getName());
    }

    @Test(expected = NotFoundException.class)
    public void testEditNotExistingCategory() {
        Long categoryId = 1L;
        Category categoryToSave = mock(Category.class);
        when(categoryToSave.getId()).thenReturn(categoryId);

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());

        categoryService.edit(categoryToSave);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testPatchCategory() {
        categoryService.patch(mock(Category.class));
    }

}
