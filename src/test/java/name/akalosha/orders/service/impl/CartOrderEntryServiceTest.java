package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.CartDocMapper;
import name.akalosha.orders.converters.mappers.search.CartOrderEntryDocMapper;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.search.CartDoc;
import name.akalosha.orders.model.dto.search.CartOrderEntryDoc;
import name.akalosha.orders.repository.elastic.CartOrderEntrySearchRepository;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.service.CartOrderEntryService;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.ProductService;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class CartOrderEntryServiceTest {

    private static final int MAX_CART_ENTRIES = 9;

    private CartOrderEntryService cartOrderEntryService;

    @Mock
    private CartOrderEntryRepository cartOrderEntryRepository;

    @Mock
    private CartService cartService;

    @Mock
    private ProductService productService;

    @Mock
    private CartOrderEntrySearchRepository cartOrderEntrySearchRepository;

    @Mock
    private CartSearchRepository cartSearchRepository;

    @Mock
    private CartOrderEntryDocMapper cartOrderEntryDocMapper;

    @Mock
    private CartDocMapper cartDocMapper;
    
    @Before
    public void before() {
        initCartOrderEntryService(MAX_CART_ENTRIES);
    }

    private void initCartOrderEntryService(int maxEntries) {
        cartOrderEntryService = new CartOrderEntryServiceImpl(
                cartOrderEntryRepository,
                cartService,
                productService,
                cartOrderEntrySearchRepository,
                cartSearchRepository,
                cartOrderEntryDocMapper,
                cartDocMapper,
                maxEntries
        );
    }

    @Test
    public void testFindCartEntryById() {
        Long cartId = 1L;
        Cart cart = mockCart(cartId);
        CartOrderEntry entryToReturn = mock(CartOrderEntry.class);
        when(entryToReturn.getCart()).thenReturn(cart);
        Long entryId = 2L;
        when(entryToReturn.getId()).thenReturn(entryId);
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(entryToReturn));

        Optional<CartOrderEntry> entryOptional = cartOrderEntryService.find(cartId, entryId);
        Assert.assertNotNull(entryOptional);
        Assert.assertTrue(entryOptional.isPresent());
        Assert.assertEquals(entryId, entryOptional.get().getId());
        Assert.assertNotNull(entryOptional.get().getCart());
        Assert.assertEquals(cartId, entryOptional.get().getCart().getId());
    }

    @Test
    public void testFindCartEntryByNotExistingId() {
        Long entryId = -1L;
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.empty());
        Long cartId = 1L;

        Optional<CartOrderEntry> entryOptional = cartOrderEntryService.find(cartId, entryId);
        Assert.assertNotNull(entryOptional);
        Assert.assertTrue(entryOptional.isEmpty());
    }

    @Test
    public void testFindCartEntryWithNotMatchingCartId() {
        Long entryCartId = 1L;
        Cart cart = mockCart(entryCartId);
        CartOrderEntry entryToReturn = mock(CartOrderEntry.class);
        when(entryToReturn.getCart()).thenReturn(cart);
        Long entryId = 2L;
        when(entryToReturn.getId()).thenReturn(entryId);
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(entryToReturn));
        Long cartIdToFind = 3L;

        Optional<CartOrderEntry> entryOptional = cartOrderEntryService.find(cartIdToFind, entryId);
        Assert.assertNotNull(entryOptional);
        Assert.assertTrue(entryOptional.isEmpty());
    }

    @Test
    public void testFindAllCartEntries() {
        CartOrderEntryDoc cartEntryDoc1 = mock(CartOrderEntryDoc.class);
        CartOrderEntryDoc cartEntryDoc2 = mock(CartOrderEntryDoc.class);
        List<CartOrderEntryDoc> cartDocs = Arrays.asList(cartEntryDoc1, cartEntryDoc2);
        Page<CartOrderEntryDoc> searchResult = new PageImpl<>(cartDocs, mock(Pageable.class), cartDocs.size());
        Long cartId = 1L;
        when(cartOrderEntrySearchRepository.findAllByCartId(cartId)).thenReturn(searchResult);

        Collection<CartOrderEntry> entries = cartOrderEntryService.findAll(cartId);
        Assert.assertTrue(CollectionUtils.isNotEmpty(entries));
        Assert.assertEquals(entries.size(), cartDocs.size());
    }

    @Test
    public void testAddNewCartEntry() {
        Long productId = 1L;
        Product product = mockProduct(productId);
        when(productService.find(productId)).thenReturn(Optional.of(product));

        Long cartId = 2L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));
        Mockito.doCallRealMethod().when(cart).setEntries(any());
        Mockito.doCallRealMethod().when(cart).getEntries();

        CartOrderEntry entryToSave = mock(CartOrderEntry.class);
        when(entryToSave.getProduct()).thenReturn(product);
        int entryQuantity = 4;
        when(entryToSave.getQuantity()).thenReturn(entryQuantity);
        Mockito.doCallRealMethod().when(entryToSave).setProduct(any(Product.class));
        Mockito.doCallRealMethod().when(entryToSave).setCart(any(Cart.class));
        Mockito.doCallRealMethod().when(entryToSave).getCart();
        Mockito.doAnswer(AdditionalAnswers.returnsFirstArg()).when(cartOrderEntryRepository).save(entryToSave);

        CartOrderEntry savedEntry = cartOrderEntryService.save(cartId, entryToSave);
        Assert.assertNotNull(savedEntry);
        Assert.assertNotNull(savedEntry.getCart());
        Assert.assertNotNull(savedEntry.getProduct());
        Assert.assertEquals(cartId, savedEntry.getCart().getId());
        Assert.assertEquals(productId, savedEntry.getProduct().getId());
        Assert.assertEquals(entryQuantity, savedEntry.getQuantity());
        Assert.assertTrue(cart.getEntries().contains(savedEntry));
    }

    @Test
    public void testAddCartEntryWithMerge() {
        Long productId = 1L;
        Product product = mockProduct(productId);
        when(productService.find(productId)).thenReturn(Optional.of(product));

        Long cartId = 2L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        CartOrderEntry existingEntry = mock(CartOrderEntry.class);
        when(existingEntry.getCart()).thenReturn(cart);
        when(existingEntry.getProduct()).thenReturn(product);
        int existingEntryQuantity = 1;
        Mockito.doCallRealMethod().when(existingEntry).setQuantity(anyInt());
        existingEntry.setQuantity(existingEntryQuantity);
        when(cart.getEntries()).thenReturn(Collections.singletonList(existingEntry));
        Mockito.doCallRealMethod().when(existingEntry).getQuantity();
        Mockito.doAnswer(AdditionalAnswers.returnsFirstArg()).when(cartOrderEntryRepository).save(existingEntry);

        CartOrderEntry entryToSave = mock(CartOrderEntry.class);
        when(entryToSave.getProduct()).thenReturn(product);
        int entryQuantity = 4;
        when(entryToSave.getQuantity()).thenReturn(entryQuantity);
        Mockito.doCallRealMethod().when(entryToSave).setProduct(any(Product.class));
        Mockito.doCallRealMethod().when(entryToSave).setCart(any(Cart.class));


        CartOrderEntry savedEntry = cartOrderEntryService.save(cartId, entryToSave);
        Assert.assertNotNull(savedEntry);
        Assert.assertNotNull(savedEntry.getCart());
        Assert.assertNotNull(savedEntry.getProduct());
        Assert.assertEquals(cartId, savedEntry.getCart().getId());
        Assert.assertEquals(productId, savedEntry.getProduct().getId());
        Assert.assertEquals(existingEntryQuantity + entryQuantity, savedEntry.getQuantity());
    }

    @Test(expected = IllegalStateException.class)
    public void testAddNewCartEntryOverTheLimit() {
        Long productId = 1L;
        Product product = mockProduct(productId);
        when(productService.find(productId)).thenReturn(Optional.of(product));

        Long cartId = 2L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        CartOrderEntry entryToSave = mock(CartOrderEntry.class);
        when(entryToSave.getProduct()).thenReturn(product);
        when(cart.getEntries()).thenReturn(Collections.singletonList(mock(CartOrderEntry.class)));

        initCartOrderEntryService(1);

        // The max number of cart entries was set to 1. Cart already contains one entry, so an attempt to add a second one should fail.
        cartOrderEntryService.save(cartId, entryToSave);
    }

    @Test(expected = NotFoundException.class)
    public void testAddCartEntryWithNotExistingCart() {
        Long productId = 1L;
        Product product = mockProduct(productId);

        Long cartId = 2L;
        when(cartService.find(cartId)).thenReturn(Optional.empty());

        CartOrderEntry entryToSave = mock(CartOrderEntry.class);
        when(entryToSave.getProduct()).thenReturn(product);

        cartOrderEntryService.save(cartId, entryToSave);
    }

    @Test(expected = NotFoundException.class)
    public void testAddCartEntryWithNotExistingProduct() {
        Long productId = 1L;
        Product product = mockProduct(productId);
        when(productService.find(productId)).thenReturn(Optional.empty());

        Long cartId = 2L;
        Cart cart = mock(Cart.class);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        CartOrderEntry entryToSave = mock(CartOrderEntry.class);
        when(entryToSave.getCart()).thenReturn(cart);
        when(entryToSave.getProduct()).thenReturn(product);

        cartOrderEntryService.save(cartId, entryToSave);
    }

    @Test
    public void testEditCartEntryQuantity() {
        Long cartId = 1L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        Long entryId = 2L;
        CartOrderEntry existingEntry = mock(CartOrderEntry.class);
        when(existingEntry.getId()).thenReturn(entryId);
        when(existingEntry.getCart()).thenReturn(cart);
        int existingEntryQuantity = 1;
        Mockito.doCallRealMethod().when(existingEntry).setQuantity(anyInt());
        existingEntry.setQuantity(existingEntryQuantity);
        Mockito.doCallRealMethod().when(existingEntry).getQuantity();
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(existingEntry));
        Mockito.doAnswer(AdditionalAnswers.returnsFirstArg()).when(cartOrderEntryRepository).save(existingEntry);

        int newQuantity = 4;
        CartOrderEntry savedEntry = cartOrderEntryService.editQuantity(cartId, entryId, newQuantity);
        Assert.assertNotNull(savedEntry);
        Assert.assertNotNull(savedEntry.getCart());
        Assert.assertEquals(cartId, savedEntry.getCart().getId());
        Assert.assertEquals(newQuantity, savedEntry.getQuantity());
    }

    @Test(expected = NotFoundException.class)
    public void testEditQuantityOfNotExistingCartEntry() {
        Long entryId = 2L;
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.empty());

        cartOrderEntryService.editQuantity(1L, entryId, 1);
    }

    @Test(expected = NotFoundException.class)
    public void testEditCartEntryQuantityNotMatchingCart() {
        Long cartId = 1L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        Long entryCartId = 2L;
        Cart entryCart = mockCart(entryCartId);

        Long entryId = 3L;
        CartOrderEntry existingEntry = mock(CartOrderEntry.class);
        when(existingEntry.getCart()).thenReturn(entryCart);
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(existingEntry));

        cartOrderEntryService.editQuantity(cartId, entryId, 1);
    }

    @Test
    public void tetDeleteCartEntry() {
        Long cartId = 1L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        Long entryId = 2L;
        CartOrderEntry existingEntry = mock(CartOrderEntry.class);
        when(existingEntry.getCart()).thenReturn(cart);
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(existingEntry));

        when(cartDocMapper.mapReverse(any())).thenReturn(mock(CartDoc.class));

        cartOrderEntryService.delete(cartId, entryId);
        verify(cartOrderEntrySearchRepository).deleteById(entryId);
        verify(cartOrderEntryRepository).deleteById(entryId);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteNotExistingCartEntry() {
        Long entryId = 2L;
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.empty());

        cartOrderEntryService.delete(1L, entryId);
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteCartEntryNotMatchingCart() {
        Long cartId = 1L;
        Cart cart = mockCart(cartId);
        when(cartService.find(cartId)).thenReturn(Optional.of(cart));

        Long entryCartId = 2L;
        Cart entryCart = mockCart(entryCartId);

        Long entryId = 3L;
        CartOrderEntry existingEntry = mock(CartOrderEntry.class);
        when(existingEntry.getCart()).thenReturn(entryCart);
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(existingEntry));

        cartOrderEntryService.delete(cartId, entryId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteOrderEntry() {
        Long entryId = 1L;
        CartOrderEntry existingEntry = mock(CartOrderEntry.class);
        when(cartOrderEntryRepository.findById(entryId)).thenReturn(Optional.of(existingEntry));
        when(existingEntry.getOrder()).thenReturn(mock(Order.class));

        cartOrderEntryService.delete(1L, entryId);
    }

    private Cart mockCart(Long cartId) {
        Cart cart = mock(Cart.class);
        when(cart.getId()).thenReturn(cartId);
        return cart;
    }

    private Product mockProduct(Long productId) {
        Product product = mock(Product.class);
        when(product.getId()).thenReturn(productId);
        return product;
    }

}
