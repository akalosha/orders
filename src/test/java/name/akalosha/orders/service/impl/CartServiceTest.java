package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.form.AddressFormMapper;
import name.akalosha.orders.converters.mappers.search.AddressDocMapper;
import name.akalosha.orders.converters.mappers.search.CartDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.CartUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.model.dto.search.AddressDoc;
import name.akalosha.orders.model.dto.search.CartDoc;
import name.akalosha.orders.repository.elastic.AddressSearchRepository;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.jpa.AddressRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.service.UserService;
import name.akalosha.orders.service.delivery.DeliveryMethodHandlerResolver;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RunWith(SpringRunner.class)
public class CartServiceTest {

    private CartServiceImpl cartService;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private CartSearchRepository cartSearchRepository;

    @Mock
    private CartOrderEntryRepository cartOrderEntryRepository;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private DeliveryMethodRepository deliveryMethodRepository;

    @Mock
    private CartDocMapper cartDocMapper;

    @Mock
    private CartUpdater cartUpdater;
    
    @Mock
    private UserService userService;

    @Mock
    private AddressFormMapper addressFormMapper;

    @Mock
    private AddressSearchRepository addressSearchRepository;

    @Mock
    private AddressDocMapper addressDocMapper;

    @Mock
    private DeliveryMethodHandlerResolver deliveryMethodHandlerResolver;

    @Before
    public void before() {
        cartService = new CartServiceImpl(
                userService,
                cartRepository,
                deliveryMethodRepository,
                cartSearchRepository,
                cartOrderEntryRepository,
                addressRepository,
                cartDocMapper,
                cartUpdater, 
                addressFormMapper,
                addressSearchRepository,
                addressDocMapper,
                deliveryMethodHandlerResolver);
    }

    @Test
    public void testFindCartById() {
        Long cartId = 1L;
        Cart cartToReturn = mock(Cart.class);
        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cartToReturn));
        when(cartToReturn.getId()).thenReturn(cartId);

        Optional<Cart> cartOptional = cartService.find(cartId);
        Assert.assertNotNull(cartOptional);
        Assert.assertTrue(cartOptional.isPresent());
        Assert.assertEquals(cartId, cartOptional.get().getId());
    }

    @Test
    public void testFindCartByNotExistingId() {
        Long cartId = -1L;
        when(cartRepository.findById(cartId)).thenReturn(Optional.empty());

        Optional<Cart> cartOptional = cartService.find(cartId);
        Assert.assertNotNull(cartOptional);
        Assert.assertTrue(cartOptional.isEmpty());
    }

    @Test
    public void testFindAllCarts() {
        CartDoc cartDoc1 = mock(CartDoc.class);
        CartDoc cartDoc2 = mock(CartDoc.class);
        List<CartDoc> cartDocs = Arrays.asList(cartDoc1, cartDoc2);
        Page<CartDoc> searchResult = new PageImpl<>(cartDocs, mock(Pageable.class), cartDocs.size());
        when(cartSearchRepository.findAll(any(Pageable.class))).thenReturn(searchResult);

        Page<Cart> carts = cartService.findAll(mock(Pageable.class));
        Assert.assertFalse(carts.isEmpty());
        Assert.assertFalse(CollectionUtils.isEmpty(carts.getContent()));
        Assert.assertEquals(cartDocs.size(), carts.getContent().size());
    }

    @Test
    public void testAddNewCart() {
        String userName = "user";
        User user = mock(User.class);
        when(user.getUsername()).thenReturn(userName);
        Cart cartToSave = mock(Cart.class);
        when(cartToSave.getUser()).thenReturn(user);
        when(userService.findByUsername(userName)).thenReturn(Optional.of(user));
        when(cartRepository.save(cartToSave)).thenAnswer(invocation -> {
            Cart argument = invocation.getArgument(0);
            when(argument.getId()).thenReturn(1L);
            return argument;
        });

        Cart savedCart = cartService.save(cartToSave);
        Assert.assertNotNull(savedCart);
        Assert.assertNotNull(savedCart.getUser());
        Assert.assertEquals(userName, savedCart.getUser().getUsername());
        Assert.assertNotNull(savedCart.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddNewCartNullUser() {
        Cart cartToSave = mock(Cart.class);
        when(cartToSave.getUser()).thenReturn(null);

        cartService.save(cartToSave);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddNewCartNullUsername() {
        User user = mock(User.class);
        when(user.getUsername()).thenReturn(null);
        Cart cartToSave = mock(Cart.class);
        when(cartToSave.getUser()).thenReturn(user);

        cartService.save(cartToSave);
    }

    @Test(expected = NotFoundException.class)
    public void testAddNewCartNotExistingUser() {
        String userName = "user";
        User user = mock(User.class);
        when(user.getUsername()).thenReturn(userName);
        Cart cartToSave = mock(Cart.class);
        when(cartToSave.getUser()).thenReturn(user);
        when(userService.findByUsername(userName)).thenReturn(Optional.empty());

        cartService.save(cartToSave);
    }

    @Test
    public void tetDeleteCart() {
        long cartId = 1L;
        Cart cart = mock(Cart.class);
        when(cartRepository.findById(cartId)).thenReturn(Optional.of(cart));

        cartService.delete(cartId);
        verify(cartSearchRepository).deleteById(cartId);
        verify(cartRepository).deleteById(cartId);
    }

    @Test(expected = NotFoundException.class)
    public void tetDeleteNotExistingCart() {
        long cartId = 1L;
        cartService.delete(cartId);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testEditCart() {
        cartService.edit(mock(Cart.class));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testPatchCart() {
        cartService.patch(mock(Cart.class));
    }

    @Test
    public void testAddNewShippingAddress() {
        Long cartId = 1L;
        Cart cart = mock(Cart.class);
        when(cartRepository.findByIdWithEagerFetch(cartId)).thenReturn(Optional.of(cart));

        AddressForm shippingAddressForm = mock(AddressForm.class);
        Address shippingAddress = mock(Address.class);
        when(addressFormMapper.map(shippingAddressForm)).thenReturn(shippingAddress);

        Long addressId = 1L;
        when(addressRepository.save(shippingAddress)).thenAnswer(invocation -> {
            Address argument = invocation.getArgument(0);
            when(argument.getId()).thenReturn(addressId);
            return argument;
        });
        AddressDoc addressDoc = mock(AddressDoc.class);
        when(addressDocMapper.mapReverse(shippingAddress)).thenReturn(addressDoc);

        doCallRealMethod().when(cart).setShippingAddress(shippingAddress);
        doCallRealMethod().when(cart).getShippingAddress();
        when(cartRepository.save(cart)).thenReturn(cart);
        when(cartDocMapper.mapReverse(cart)).thenReturn(mock(CartDoc.class));

        cart = cartService.setShippingAddress(cartId, shippingAddressForm);

        Assert.assertNotNull(cart);
        Assert.assertNotNull(cart.getShippingAddress());
        Assert.assertNotNull(cart.getShippingAddress().getId());
        Assert.assertEquals(addressId, cart.getShippingAddress().getId());
        Assert.assertEquals(shippingAddress, cart.getShippingAddress());

        verify(addressRepository).save(shippingAddress);
        verify(addressSearchRepository).save(addressDoc);
        verify(cartRepository).save(cart);
        verify(cartSearchRepository).save(any());
    }

    @Test(expected = NotFoundException.class)
    public void testAddShippingAddressToNotExistingCart() {
        long cartId = -1L;
        when(cartRepository.findByIdWithEagerFetch(cartId)).thenReturn(Optional.empty());
        cartService.setShippingAddress(cartId, mock(AddressForm.class));
    }

    @Test
    public void testReplaceShippingAddress() {
        Long cartId = 1L;
        Cart cart = mock(Cart.class);
        when(cartRepository.findByIdWithEagerFetch(cartId)).thenReturn(Optional.of(cart));

        AddressForm newShippingAddressForm = mock(AddressForm.class);
        Address newShippingAddress = mock(Address.class);
        when(addressFormMapper.map(newShippingAddressForm)).thenReturn(newShippingAddress);

        doCallRealMethod().when(cart).setShippingAddress(any(Address.class));
        doCallRealMethod().when(cart).getShippingAddress();

        Address existingShippingAddress = mock(Address.class);
        cart.setShippingAddress(existingShippingAddress);
        AddressDoc existingAddressDoc = mock(AddressDoc.class);
        when(addressDocMapper.mapReverse(existingShippingAddress)).thenReturn(existingAddressDoc);

        Long addressId = 1L;
        when(addressRepository.save(newShippingAddress)).thenAnswer(invocation -> {
            Address argument = invocation.getArgument(0);
            when(argument.getId()).thenReturn(addressId);
            return argument;
        });
        AddressDoc newAddressDoc = mock(AddressDoc.class);
        when(addressDocMapper.mapReverse(newShippingAddress)).thenReturn(newAddressDoc);

        when(cartRepository.save(cart)).thenReturn(cart);
        when(cartDocMapper.mapReverse(cart)).thenReturn(mock(CartDoc.class));

        cart = cartService.setShippingAddress(cartId, newShippingAddressForm);

        Assert.assertNotNull(cart);
        Assert.assertNotNull(cart.getShippingAddress());
        Assert.assertNotNull(cart.getShippingAddress().getId());
        Assert.assertEquals(addressId, cart.getShippingAddress().getId());
        Assert.assertEquals(newShippingAddress, cart.getShippingAddress());

        verify(addressRepository).save(newShippingAddress);
        verify(addressSearchRepository).save(newAddressDoc);
        verify(addressRepository).delete(existingShippingAddress);
        verify(addressSearchRepository).delete(existingAddressDoc);
        verify(cartRepository).save(cart);
        verify(cartSearchRepository).save(any());
    }

}
