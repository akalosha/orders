package name.akalosha.orders.converters.updaters;

import org.mapstruct.MappingTarget;

/**
 * Maps all fields from source to target.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface FullUpdater<T> {

    void update(T src, @MappingTarget T dst);

}
