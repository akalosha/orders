package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.domain.UserFullUpdater;
import name.akalosha.orders.converters.updaters.domain.UserPartialUpdater;
import name.akalosha.orders.model.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link User} updater.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class UserUpdater extends BaseUpdater<User> {

    @Autowired
    public UserUpdater(UserFullUpdater fullUpdater, UserPartialUpdater partialUpdater) {
        super(fullUpdater, partialUpdater);
    }

}
