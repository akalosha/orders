package name.akalosha.orders.converters.updaters;

import org.mapstruct.MappingTarget;

/**
 * Maps only not null fields from source to target.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface PartialUpdater<T> {

    void updateNotNull(T src, @MappingTarget T dst);

}
