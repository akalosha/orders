package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.FullUpdater;
import name.akalosha.orders.model.domain.Category;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Full updater for {@link Category}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
public interface CategoryFullUpdater extends FullUpdater<Category> {
}
