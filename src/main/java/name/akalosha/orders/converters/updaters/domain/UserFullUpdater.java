package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.FullUpdater;
import name.akalosha.orders.model.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Full updater for {@link User}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
public interface UserFullUpdater extends FullUpdater<User> {
}
