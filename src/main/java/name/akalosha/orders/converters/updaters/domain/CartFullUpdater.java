package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.FullUpdater;
import name.akalosha.orders.model.domain.Cart;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Full updater for {@link Cart}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
public interface CartFullUpdater extends FullUpdater<Cart> {
}
