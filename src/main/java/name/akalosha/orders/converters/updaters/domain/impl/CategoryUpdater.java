package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.domain.CategoryFullUpdater;
import name.akalosha.orders.converters.updaters.domain.CategoryPartialUpdater;
import name.akalosha.orders.model.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link Category} updater.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class CategoryUpdater extends BaseUpdater<Category> {

    @Autowired
    public CategoryUpdater(CategoryFullUpdater fullUpdater, CategoryPartialUpdater partialUpdater) {
        super(fullUpdater, partialUpdater);
    }

}
