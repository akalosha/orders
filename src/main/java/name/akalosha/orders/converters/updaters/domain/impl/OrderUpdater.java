package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.domain.OrderFullUpdater;
import name.akalosha.orders.converters.updaters.domain.OrderPartialUpdater;
import name.akalosha.orders.model.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link Order} updater.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class OrderUpdater extends BaseUpdater<Order> {

    @Autowired
    public OrderUpdater(OrderFullUpdater fullUpdater, OrderPartialUpdater partialUpdater) {
        super(fullUpdater, partialUpdater);
    }

}
