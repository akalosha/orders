package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.domain.CartFullUpdater;
import name.akalosha.orders.converters.updaters.domain.CartPartialUpdater;
import name.akalosha.orders.model.domain.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link Cart} updater.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class CartUpdater extends BaseUpdater<Cart> {

    @Autowired
    public CartUpdater(CartFullUpdater fullUpdater, CartPartialUpdater partialUpdater) {
        super(fullUpdater, partialUpdater);
    }

}
