package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.PartialUpdater;
import name.akalosha.orders.model.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Partial updater for {@link User}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserPartialUpdater extends PartialUpdater<User> {
}
