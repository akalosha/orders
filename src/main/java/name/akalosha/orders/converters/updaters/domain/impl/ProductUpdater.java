package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.domain.ProductFullUpdater;
import name.akalosha.orders.converters.updaters.domain.ProductPartialUpdater;
import name.akalosha.orders.model.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link Product} updater.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class ProductUpdater extends BaseUpdater<Product> {

    @Autowired
    public ProductUpdater(ProductFullUpdater fullUpdater, ProductPartialUpdater partialUpdater) {
        super(fullUpdater, partialUpdater);
    }

}
