package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.CombinedUpdater;
import name.akalosha.orders.converters.updaters.FullUpdater;
import name.akalosha.orders.converters.updaters.PartialUpdater;
import org.springframework.util.Assert;

/**
 * Generic implementation of {@link CombinedUpdater}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
abstract class BaseUpdater<T> implements CombinedUpdater<T> {

    protected final FullUpdater<T> fullUpdater;
    protected final PartialUpdater<T> partialUpdater;

    public BaseUpdater(FullUpdater<T> fullUpdater, PartialUpdater<T> partialUpdater) {
        Assert.notNull(fullUpdater, "Full updater should not be null");
        Assert.notNull(partialUpdater, "Partial updater should not be null");

        this.fullUpdater = fullUpdater;
        this.partialUpdater = partialUpdater;
    }

    @Override
    public void update(T src, T dst) {
        Assert.notNull(src, "Source object should not be null");
        Assert.notNull(src, "Target object should not be null");

        fullUpdater.update(src, dst);
    }

    @Override
    public void updateNotNull(T src, T dst) {
        Assert.notNull(src, "Source object should not be null");
        Assert.notNull(src, "Target object should not be null");

        partialUpdater.updateNotNull(src, dst);
    }
}
