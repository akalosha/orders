package name.akalosha.orders.converters.updaters.domain.impl;

import name.akalosha.orders.converters.updaters.domain.CartOrderEntryFullUpdater;
import name.akalosha.orders.converters.updaters.domain.CartOrderEntryPartialUpdater;
import name.akalosha.orders.model.domain.CartOrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link CartOrderEntry} updater.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class CartOrderEntryUpdater extends BaseUpdater<CartOrderEntry> {

    @Autowired
    public CartOrderEntryUpdater(CartOrderEntryFullUpdater fullUpdater, CartOrderEntryPartialUpdater partialUpdater) {
        super(fullUpdater, partialUpdater);
    }

}
