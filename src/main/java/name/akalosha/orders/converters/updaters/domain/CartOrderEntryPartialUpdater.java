package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.PartialUpdater;
import name.akalosha.orders.model.domain.CartOrderEntry;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Partial updater for {@link CartOrderEntry}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CartOrderEntryPartialUpdater extends PartialUpdater<CartOrderEntry> {
}
