package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.PartialUpdater;
import name.akalosha.orders.model.domain.Cart;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Partial updater for {@link Cart}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CartPartialUpdater extends PartialUpdater<Cart> {
}
