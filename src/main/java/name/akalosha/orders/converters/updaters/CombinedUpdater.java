package name.akalosha.orders.converters.updaters;

/**
 * Updated that can do both full and partial mapping.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CombinedUpdater<T> extends FullUpdater<T>, PartialUpdater<T> {
}
