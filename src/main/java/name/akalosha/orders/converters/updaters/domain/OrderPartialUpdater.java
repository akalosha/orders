package name.akalosha.orders.converters.updaters.domain;

import name.akalosha.orders.converters.updaters.PartialUpdater;
import name.akalosha.orders.model.domain.Order;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

/**
 * Partial updater for {@link Order}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderPartialUpdater extends PartialUpdater<Order> {
}
