package name.akalosha.orders.converters.mappers.util;

import name.akalosha.orders.converters.mappers.Mapper;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.model.dto.rest.pagination.SortDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class PaginationMappingUtil {

    /**
     * This should be converted to a MapStruct mapper after MapStruct version 1.5 is out. Before 1.5 it is not possible
     * to map an iterable (Page) to a non-iterable object (PageDTO). PR that fixes this - https://github.com/mapstruct/mapstruct/pull/2210
     */
    public static <SRC, DST> PageDTO<DST> mapPage(Page<SRC> page, Mapper<SRC, DST> contentMapper, Mapper<Sort, SortDTO> sortMapper) {
        if (page == null) {
            return null;
        }
        Assert.notNull(contentMapper, "Mapper should not be null");

        PageDTO<DST> pageDTO = new PageDTO<>();
        pageDTO.setCurrentPage(page.getNumber());
        pageDTO.setPageSize(page.getSize());
        pageDTO.setTotalPages(page.getTotalPages());
        pageDTO.setTotalElements(page.getTotalElements());
        pageDTO.setElementsOnCurrentPage(page.getNumberOfElements());
        pageDTO.setSort(sortMapper.map(page.getSort()));

        Collection<DST> mappedContent = CollectionMapper.map(page.getContent(), contentMapper);
        pageDTO.setContent(new ArrayList<>(mappedContent));

        return pageDTO;
    }

}
