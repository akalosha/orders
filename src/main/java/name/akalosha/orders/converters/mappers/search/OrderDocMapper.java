package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.TimestampMapper;
import name.akalosha.orders.converters.mappers.util.UsernameMapper;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.dto.search.OrderDoc;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * {@link OrderDoc} - {@link Order} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(
        componentModel = "spring",
        uses = {
                CartOrderEntryDocMapper.class, AddressDocMapper.class, DeliveryMethodDocMapper.class, SiteDocMapper.class,
                UsernameMapper.class, TimestampMapper.class
        }
)
public interface OrderDocMapper extends BidirectionalMapper<OrderDoc, Order> {

    /**
     * {@link OrderDoc} - {@link Order} bidirectional mapper that maps only minimal set of order's field.
     */
    @Mapper(componentModel = "spring", uses = {CartOrderEntryDocMapper.class, TimestampMapper.class})
    interface MinimalOrderDocMapper extends BidirectionalMapper<OrderDoc, Order> {

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        OrderDoc mapReverse(Order order);

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        Order map(OrderDoc orderDoc);

    }

}
