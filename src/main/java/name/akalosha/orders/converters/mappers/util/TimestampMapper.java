package name.akalosha.orders.converters.mappers.util;

import org.mapstruct.Mapper;

import java.sql.Timestamp;

/**
 * Mapper to convert between {@link Long} and {@link Timestamp}. By extending this interface,
 * other mappers gain ability to convert Timestamps.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface TimestampMapper {

    default Long mapTimestampToLong(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.getTime();
    }

    default Timestamp mapLongToTimestamp(Long epochMilli) {
        if (epochMilli == null) {
            return null;
        }
        return new Timestamp(epochMilli);
    }

}
