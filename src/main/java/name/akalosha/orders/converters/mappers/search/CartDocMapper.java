package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.UsernameMapper;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.dto.search.CartDoc;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * {@link CartDoc} - {@link Cart} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(
        componentModel = "spring",
        uses = {
                CartOrderEntryDocMapper.class, AddressDocMapper.class, DeliveryMethodDocMapper.class, SiteDocMapper.class,
                UsernameMapper.class
        }
)
public interface CartDocMapper extends BidirectionalMapper<CartDoc, Cart> {

    /**
     * {@link CartDoc} - {@link Cart} bidirectional mapper that maps only minimal set of cart's field.
     */
    @Mapper(componentModel = "spring")
    interface MinimalCartDocMapper extends BidirectionalMapper<CartDoc, Cart> {

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        CartDoc mapReverse(Cart cart);

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        Cart map(CartDoc cartDoc);
    }

}
