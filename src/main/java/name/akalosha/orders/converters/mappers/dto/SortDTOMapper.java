package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.Mapper;
import name.akalosha.orders.converters.mappers.util.PaginationMappingUtil;
import name.akalosha.orders.model.dto.rest.pagination.SortDTO;
import name.akalosha.orders.model.dto.rest.pagination.SortOrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * A mapper that converts {@link Sort} and {@link Sort.Order} types to their DTO representations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@org.mapstruct.Mapper(componentModel = "spring")
public interface SortDTOMapper extends Mapper<Sort, SortDTO> {

    /**
     * Manual mapping is implemented here because of an issue described in javadoc for method
     * {@link PaginationMappingUtil#mapPage(Page, Mapper, Mapper)}
     */
    @Override
    default SortDTO map(Sort sort) {
        if (sort == null) {
            return null;
        }
        SortDTO sortDTO = new SortDTO();
        sortDTO.setOrders(mapOrders(sort.toList()));
        return sortDTO;
    }

    List<SortOrderDTO> mapOrders(List<Sort.Order> orders);

}
