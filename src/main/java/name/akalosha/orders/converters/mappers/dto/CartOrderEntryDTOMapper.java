package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.dto.rest.CartOrderEntryDTO;
import org.mapstruct.Mapper;

/**
 * {@link CartOrderEntry} - {@link CartOrderEntryDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(
        componentModel = "spring",
        uses = {ProductDTOMapper.class, CartDTOMapper.MinimalCartDTOMapper.class, OrderDTOMapper.MinimalOrderDTOMapper.class}
)
public interface CartOrderEntryDTOMapper extends BidirectionalMapper<CartOrderEntry, CartOrderEntryDTO> {

    @Override
    CartOrderEntryDTO map(CartOrderEntry entry);

    @Override
    CartOrderEntry mapReverse(CartOrderEntryDTO entryDTO);

}
