package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.dto.search.SiteDoc;
import org.mapstruct.Mapper;

/**
 * {@link SiteDoc} - {@link Site} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface SiteDocMapper extends BidirectionalMapper<SiteDoc, Site> {
}
