package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.rest.ProductDTO;
import org.mapstruct.Mapper;

/**
 * {@link Product} - {@link ProductDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", uses = CategoryDTOMapper.class)
public interface ProductDTOMapper extends BidirectionalMapper<Product, ProductDTO> {

    ProductDTO map(Product product);

}
