package name.akalosha.orders.converters.mappers.util;

import name.akalosha.orders.converters.mappers.Mapper;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Utility for mapping collections.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public final class CollectionMapper {

    private CollectionMapper() {
    }

    public static <SRC, DST> Collection<DST> map(Collection<SRC> srcCollection, Mapper<SRC, DST> mapper) {
        return CollectionUtils.emptyIfNull(srcCollection).stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

}
