package name.akalosha.orders.converters.mappers;

/**
 * Extension of a {@link Mapper} that allows bidirectional mapping.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface BidirectionalMapper<SRC, DST> extends Mapper<SRC, DST> {

    SRC mapReverse(DST dst);

    default BidirectionalMapper<DST, SRC> reverseMapper() {
        return new BidirectionalMapper<>() {
            @Override
            public DST mapReverse(SRC src) {
                return BidirectionalMapper.this.map(src);
            }

            @Override
            public SRC map(DST dst) {
                return BidirectionalMapper.this.mapReverse(dst);
            }
        };
    }

}
