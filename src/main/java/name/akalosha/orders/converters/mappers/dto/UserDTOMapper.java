package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.UserDTO;
import org.mapstruct.Mapper;

/**
 * {@link User} - {@link UserDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface UserDTOMapper extends BidirectionalMapper<User, UserDTO> {
}
