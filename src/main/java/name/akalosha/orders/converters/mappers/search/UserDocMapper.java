package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.search.UserDoc;
import org.mapstruct.Mapper;

/**
 * {@link UserDoc} - {@link User} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface UserDocMapper extends BidirectionalMapper<UserDoc, User> {
}
