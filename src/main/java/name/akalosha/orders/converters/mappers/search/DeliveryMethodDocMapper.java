package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.TimestampMapper;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.dto.search.DeliveryMethodDoc;
import org.mapstruct.Mapper;

/**
 * {@link DeliveryMethodDoc} - {@link DeliveryMethod} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", uses = {SiteDocMapper.class, TimestampMapper.class})
public interface DeliveryMethodDocMapper extends BidirectionalMapper<DeliveryMethodDoc, DeliveryMethod> {
}
