package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.search.CategoryDoc;
import org.mapstruct.Mapper;

/**
 * {@link CategoryDoc} - {@link Category} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface CategoryDocMapper extends BidirectionalMapper<CategoryDoc, Category> {
}
