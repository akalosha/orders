package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.dto.search.CartOrderEntryDoc;
import org.mapstruct.Mapper;

/**
 * {@link CartOrderEntryDoc} - {@link CartOrderEntry} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(
        componentModel = "spring",
        uses = {
                ProductDocMapper.class,
                CartDocMapper.MinimalCartDocMapper.class,
                OrderDocMapper.MinimalOrderDocMapper.class
        }
)
public interface CartOrderEntryDocMapper extends BidirectionalMapper<CartOrderEntryDoc, CartOrderEntry> {

    @Override
    CartOrderEntry map(CartOrderEntryDoc cartOrderEntryDoc);

    @Override
    CartOrderEntryDoc mapReverse(CartOrderEntry entry);

}
