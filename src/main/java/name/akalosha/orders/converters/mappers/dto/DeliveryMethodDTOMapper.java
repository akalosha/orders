package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.search.SiteDocMapper;
import name.akalosha.orders.converters.mappers.util.TimestampMapper;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.dto.rest.DeliveryMethodDTO;
import org.mapstruct.Mapper;

/**
 * {@link DeliveryMethod} - {@link DeliveryMethodDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", uses = {SiteDocMapper.class, TimestampMapper.class})
public interface DeliveryMethodDTOMapper extends BidirectionalMapper<DeliveryMethod, DeliveryMethodDTO> {
}
