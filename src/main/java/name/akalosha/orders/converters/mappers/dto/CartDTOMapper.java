package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.UsernameMapper;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.dto.rest.CartDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * {@link Cart} - {@link CartDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(
        componentModel = "spring",
        uses = {
                CartOrderEntryDTOMapper.class, AddressDTOMapper.class, DeliveryMethodDTOMapper.class, SiteDTOMapper.class,
                UsernameMapper.class
        }
)
public interface CartDTOMapper extends BidirectionalMapper<Cart, CartDTO> {

    /**
     * {@link Cart} - {@link CartDTO} bidirectional mapper that maps only minimal set of cart's field.
     */
    @Mapper(componentModel = "spring", uses = CartOrderEntryDTOMapper.class)
    interface MinimalCartDTOMapper extends BidirectionalMapper<Cart, CartDTO> {

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        Cart mapReverse(CartDTO cartDTO);

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        CartDTO map(Cart cart);
    }

}
