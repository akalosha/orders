package name.akalosha.orders.converters.mappers;

/**
 * Unidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface Mapper<SRC, DST> {

    DST map(SRC src);

}
