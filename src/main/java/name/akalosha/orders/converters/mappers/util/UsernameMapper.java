package name.akalosha.orders.converters.mappers.util;

import name.akalosha.orders.model.domain.User;
import org.mapstruct.Mapper;

/**
 * Mapper that converts between {@link User} and a string username.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface UsernameMapper {

    default User mapUser(String user) {
        User u = new User();
        u.setUsername(user);
        return u;
    }

    default String mapUser(User user) {
        return user.getUsername();
    }

}
