package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.dto.rest.SiteDTO;
import org.mapstruct.Mapper;

/**
 * {@link Site} - {@link SiteDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface SiteDTOMapper extends BidirectionalMapper<Site, SiteDTO> {
}
