package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.rest.CategoryDTO;
import org.mapstruct.Mapper;

/**
 * {@link Category} - {@link CategoryDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface CategoryDTOMapper extends BidirectionalMapper<Category, CategoryDTO> {
}
