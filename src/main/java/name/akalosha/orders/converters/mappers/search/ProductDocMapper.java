package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.search.ProductDoc;
import org.mapstruct.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * {@link ProductDoc} - {@link Product} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring", uses = CategoryDocMapper.class)
public interface ProductDocMapper extends BidirectionalMapper<ProductDoc, Product> {

    Logger LOG = LoggerFactory.getLogger(ProductDocMapper.class);

    // price mapping
    default BigDecimal mapStringToBigDecimal(String str) {
        try {
            return new BigDecimal(str);
        } catch (NumberFormatException ex) {
            LOG.error("Failed to convert indexed price: " + str, ex);
        }
        return null;
    }

    // price mapping
    default String mapBigDecimalToString(BigDecimal bigDecimal) {
        return bigDecimal.toPlainString();
    }

}
