package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.TimestampMapper;
import name.akalosha.orders.converters.mappers.util.UsernameMapper;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.dto.rest.OrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * {@link Order} - {@link OrderDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(
        componentModel = "spring",
        uses = {
                CartOrderEntryDTOMapper.class, AddressDTOMapper.class, DeliveryMethodDTOMapper.class, SiteDTOMapper.class,
                UsernameMapper.class, TimestampMapper.class
        }
)
public interface OrderDTOMapper extends BidirectionalMapper<Order, OrderDTO> {

    /**
     * {@link Order} - {@link OrderDTO} bidirectional mapper that maps only minimal set of order's field.
     */
    @Mapper(componentModel = "spring", uses = CartOrderEntryDTOMapper.class)
    interface MinimalOrderDTOMapper extends BidirectionalMapper<Order, OrderDTO> {
        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        Order mapReverse(OrderDTO orderDTO);

        @Mapping(target = "entries", ignore = true)
        @Mapping(target = "user", ignore = true)
        @Mapping(target = "shippingAddress", ignore = true)
        @Mapping(target = "deliveryMethod", ignore = true)
        @Mapping(target = "site", ignore = true)
        @Override
        OrderDTO map(Order order);

    }

}
