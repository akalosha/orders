package name.akalosha.orders.converters.mappers.form;

import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import org.mapstruct.Mapper;

/**
 * {@link AddressForm} - {@link Address} unidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface AddressFormMapper extends name.akalosha.orders.converters.mappers.Mapper<AddressForm, Address> {
}
