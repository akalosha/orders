package name.akalosha.orders.converters.mappers.dto;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.dto.rest.AddressDTO;
import org.mapstruct.Mapper;

/**
 * {@link Address} - {@link AddressDTO} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface AddressDTOMapper extends BidirectionalMapper<Address, AddressDTO> {
}
