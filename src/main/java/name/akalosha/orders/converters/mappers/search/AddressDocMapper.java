package name.akalosha.orders.converters.mappers.search;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.dto.search.AddressDoc;
import org.mapstruct.Mapper;

/**
 * {@link AddressDoc} - {@link Address} bidirectional mapper.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Mapper(componentModel = "spring")
public interface AddressDocMapper extends BidirectionalMapper<AddressDoc, Address> {
}
