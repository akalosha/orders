package name.akalosha.orders.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    public WebSecurityConfig(@Qualifier("userDetailServiceImpl") UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        // Using Argon2 because:
        // 1. It is rater new, meaning its designers had a chance to avoid the shortcomings of older hashing functions.
        // 2. It has won the 2015 Password Hashing Competition, which should count for something. That also means it was
        // reviewed by many experts.
        // 3. It doesn't impose the restriction on password length.
        // The second choice would be bcrypt, which is still good, but has limited configurability and a limited
        // password length (72 characters).
        //
        // Salt/hash lengths are picked based on the paper by Argon2 authors: https://www.password-hashing.net/argon2-specs.pdf
        // (recommended minimum for salt and hash is 16 bytes). Other parameters are picked with the goal to make hashing
        // take ~0.5 seconds on target hardware (which is my PC at the moment). Basically one should know what hardware
        // config they would be running in production and then tune the parameters for hashing function in such way that
        // computing a hash would take some target amount of time X.
        int saltLength = 16;
        int hashLength = 32;
        int parallelism = 4; // parallelism parameter has no effect in current Spring implementation
        int memory = (int) Math.pow(2, 14); // 16,384 KiB / 16 MiB
        int iterations = 35;
        return new Argon2PasswordEncoder(saltLength, hashLength, parallelism, memory, iterations);
    }

    @Bean
    public UserDetailsService getUserDetailsService() {
        return userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(getUserDetailsService())
                .passwordEncoder(getPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        // allow access to Swagger UI URLs without authentication
        web.ignoring().antMatchers("/swagger-ui*", "/webjars/**", "/swagger-resources/**", "/v2/api-docs", "/users/register", "/sites/**");
    }

    // password encoder is not getting picked up without this
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
