package name.akalosha.orders.config;

import name.akalosha.orders.exception.ServerException;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class ElasticsearchClientConfig {

    private static final Logger LOG = LoggerFactory.getLogger(ElasticsearchClientConfig.class);

    @Bean
    public Client client(
            @Value("${elastic.cluster-name}") String clusterName,
            @Value("${elastic.host}") String host,
            @Value("${elastic.port}") int port
    ) {
        LOG.trace("Setting up Elasticsearch client with following params: clusterName={}, host={}, port={}", clusterName, host, port);
        Settings elasticsearchSettings = Settings.builder()
                .put("cluster.name", clusterName)
                .build();
        TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);

        InetAddress address;
        try {
            address = InetAddress.getByName(host);
            LOG.trace("Resolved host IP address to {}", address);
        } catch (UnknownHostException ex) {
            throw new ServerException("Failed to get Elasticsearch host address", ex);
        }
        client.addTransportAddress(new TransportAddress(address, port));

        return client;
    }

}
