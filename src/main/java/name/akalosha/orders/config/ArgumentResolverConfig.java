package name.akalosha.orders.config;

import name.akalosha.orders.rest.resolver.SortArgumentResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Configuration for customizing argument resolvers.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class ArgumentResolverConfig implements WebMvcConfigurer {

    private static final Logger LOG = LoggerFactory.getLogger(ArgumentResolverConfig.class);

    private final SortArgumentResolver sortArgumentResolver;

    @Autowired
    public ArgumentResolverConfig(SortArgumentResolver sortArgumentResolver) {
        this.sortArgumentResolver = sortArgumentResolver;
    }

    /**
     * Adding a custom sort argument resolver. The reason why we need this is described at {@link SortArgumentResolver}.
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        PageableHandlerMethodArgumentResolver resolver = new PageableHandlerMethodArgumentResolver(sortArgumentResolver);
        resolvers.add(resolver);
        LOG.trace("Added PageableHandlerMethodArgumentResolver to method argument resolvers list");
    }
}
