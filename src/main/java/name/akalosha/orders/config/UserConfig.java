package name.akalosha.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.Assert;

import java.util.function.Supplier;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class UserConfig {

    @Bean
    public Supplier<User> authenticatedUserSupplierBean() {
        return this::getAuthenticatedUser;
    }

    private User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Assert.notNull(authentication, "Security context does not contain authentication");

        Object principal = authentication.getPrincipal();
        Assert.notNull(principal, "Authentication does not contain principal");
        Assert.isInstanceOf(
                org.springframework.security.core.userdetails.User.class,
                principal,
                "Unexpected principal type: " + principal.getClass().getCanonicalName()
        );

        return (User) principal;
    }

}
