package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.CartDocMapper;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.dto.search.CartDoc;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

/**
 * Indexes {@link Cart} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class CartIndexInitializer extends IndexInitializer<Long, Cart, CartDoc> {

    @Autowired
    public CartIndexInitializer(
            CartRepository persistenceRepository,
            CartSearchRepository searchRepository,
            CartDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Transactional
    @Override
    public void index() {
        super.index(Cart.class);
    }
}
