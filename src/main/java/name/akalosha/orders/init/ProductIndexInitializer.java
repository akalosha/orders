package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.ProductDocMapper;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.search.ProductDoc;
import name.akalosha.orders.repository.elastic.ProductSearchRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

/**
 * Indexes {@link Product} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class ProductIndexInitializer extends IndexInitializer<Long, Product, ProductDoc> {

    @Autowired
    public ProductIndexInitializer(
            ProductRepository persistenceRepository,
            ProductSearchRepository searchRepository,
            ProductDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Override
    public void index() {
        super.index(Product.class);
    }
}
