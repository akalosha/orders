package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.OrderDocMapper;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.dto.search.OrderDoc;
import name.akalosha.orders.repository.elastic.OrderSearchRepository;
import name.akalosha.orders.repository.jpa.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

/**
 * Indexes {@link Order} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class OrderIndexInitializer extends IndexInitializer<Long, Order, OrderDoc> {

    @Autowired
    public OrderIndexInitializer(
            OrderRepository persistenceRepository,
            OrderSearchRepository searchRepository,
            OrderDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Transactional
    @Override
    public void index() {
        super.index(Order.class);
    }
}
