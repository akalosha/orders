package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.CategoryDocMapper;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.search.CategoryDoc;
import name.akalosha.orders.repository.elastic.CategorySearchRepository;
import name.akalosha.orders.repository.jpa.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

/**
 * Indexes {@link Category} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class CategoryIndexInitializer extends IndexInitializer<Long, Category, CategoryDoc> {

    @Autowired
    public CategoryIndexInitializer(
            CategoryRepository persistenceRepository,
            CategorySearchRepository searchRepository,
            CategoryDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Override
    public void index() {
        super.index(Category.class);
    }
}
