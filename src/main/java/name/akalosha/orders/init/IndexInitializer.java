package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.CollectionMapper;
import name.akalosha.orders.model.domain.Identifiable;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.List;

/**
 * Base class for index initializers. Implements indexing of all instances of given entity type.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
abstract class IndexInitializer<ID, PERSISTENCE_TYPE extends Identifiable<ID>, INDEXED_TYPE> {

    private static final Logger LOG = LoggerFactory.getLogger(IndexInitializer.class);

    private final CrudRepository<PERSISTENCE_TYPE, ID> persistenceRepository;
    private final CrudRepository<INDEXED_TYPE, ID> searchRepository;
    private final BidirectionalMapper<INDEXED_TYPE, PERSISTENCE_TYPE> mapper;

    protected IndexInitializer(
            CrudRepository<PERSISTENCE_TYPE, ID> persistenceRepository,
            CrudRepository<INDEXED_TYPE, ID> searchRepository,
            BidirectionalMapper<INDEXED_TYPE, PERSISTENCE_TYPE> mapper) {
        Assert.notNull(persistenceRepository, "Persistence repository should not be null");
        Assert.notNull(searchRepository, "Search repository should not be null");
        Assert.notNull(mapper, "Mapper should not be null");

        this.persistenceRepository = persistenceRepository;
        this.searchRepository = searchRepository;
        this.mapper = mapper;
    }

    protected void index(Class<? extends Identifiable<?>> persistenceType) {
        LOG.trace("Initializing search index for type {}", persistenceType.getSimpleName());
        List<PERSISTENCE_TYPE> persistenceEntities = IterableUtils.toList(persistenceRepository.findAll());
        LOG.trace("Found {} persistence entities to index", persistenceEntities.size());
        Collection<INDEXED_TYPE> indexedEntities = CollectionMapper.map(persistenceEntities, mapper.reverseMapper());
        searchRepository.deleteAll();
        if (CollectionUtils.isNotEmpty(persistenceEntities)) {
            searchRepository.saveAll(indexedEntities);
        }
        LOG.debug("Indexed {} entities of type {}", indexedEntities.size(), persistenceType.getSimpleName());
    }

    protected abstract void index();

}
