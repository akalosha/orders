package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.UserDocMapper;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.search.UserDoc;
import name.akalosha.orders.repository.elastic.UserSearchRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

/**
 * Indexes {@link User} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class UserIndexInitializer extends IndexInitializer<Long, User, UserDoc> {

    @Autowired
    public UserIndexInitializer(
            UserRepository persistenceRepository,
            UserSearchRepository searchRepository,
            UserDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Override
    public void index() {
        super.index(User.class);
    }
}
