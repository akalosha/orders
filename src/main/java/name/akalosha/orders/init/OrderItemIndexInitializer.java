package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.CartOrderEntryDocMapper;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.dto.search.CartOrderEntryDoc;
import name.akalosha.orders.repository.elastic.CartOrderEntrySearchRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

/**
 * Indexes {@link CartOrderEntry} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class OrderItemIndexInitializer extends IndexInitializer<Long, CartOrderEntry, CartOrderEntryDoc> {

    @Autowired
    public OrderItemIndexInitializer(
            CartOrderEntryRepository persistenceRepository,
            CartOrderEntrySearchRepository searchRepository,
            CartOrderEntryDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Transactional
    @Override
    public void index() {
        super.index(CartOrderEntry.class);
    }
}
