package name.akalosha.orders.init;

import name.akalosha.orders.converters.mappers.search.AddressDocMapper;
import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.dto.search.AddressDoc;
import name.akalosha.orders.repository.elastic.AddressSearchRepository;
import name.akalosha.orders.repository.jpa.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

/**
 * Indexes {@link Address} instances.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Configuration
public class AddressIndexInitializer extends IndexInitializer<Long, Address, AddressDoc> {

    @Autowired
    public AddressIndexInitializer(
            AddressRepository persistenceRepository,
            AddressSearchRepository searchRepository,
            AddressDocMapper mapper) {
        super(persistenceRepository, searchRepository, mapper);
    }

    @EventListener(ContextRefreshedEvent.class)
    @Override
    public void index() {
        super.index(Address.class);
    }
}
