package name.akalosha.orders.model.domain;

import name.akalosha.orders.util.DateTimeUtil;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Entity
@Table(name="delivery_methods")
public class DeliveryMethod implements Identifiable<Long> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int hoursToDeliver;

    @ManyToOne(optional = false)
    @JoinColumn(name="site_id", nullable=false)
    private Site site;

    private String handlerBean;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public int getHoursToDeliver() {
        return hoursToDeliver;
    }

    public void setHoursToDeliver(int hoursToDeliver) {
        this.hoursToDeliver = hoursToDeliver;
    }

    public Timestamp getEstimatedDeliveryDate() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime deliveryDate = now.plus(hoursToDeliver, ChronoUnit.HOURS);

        return DateTimeUtil.toTimestamp(deliveryDate);
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getHandlerBean() {
        return handlerBean;
    }

    public void setHandlerBean(String handlerBean) {
        this.handlerBean = handlerBean;
    }

}
