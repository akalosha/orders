package name.akalosha.orders.model.domain;

import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.util.List;

/**
 * Base class for entities like cart and order.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@MappedSuperclass
public abstract class AbstractOrder {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    protected abstract List<CartOrderEntry> getEntries();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "shipping_address_id")
    private Address shippingAddress;

    @OneToOne
    @JoinColumn(name = "delivery_method_id")
    private DeliveryMethod deliveryMethod;

    @ManyToOne(optional = false)
    @JoinColumn(name="site_id", nullable=false, updatable=false)
    private Site site;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getTotal() {
        return calculateTotal();
    }

    /**
     * Returns total price of all items in this order.
     *
     * @return total price
     */
    private BigDecimal calculateTotal() {
        return CollectionUtils.emptyIfNull(getEntries()).stream()
                .filter(item -> item.getProduct() != null && item.getProduct().getPrice() != null)
                .map(item -> item.getProduct().getPrice().multiply(new BigDecimal(item.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public DeliveryMethod getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

}
