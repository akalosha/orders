package name.akalosha.orders.model.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.List;

/**
 * Persistence model of cart.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Entity
@Table(name="carts")
public class Cart extends AbstractOrder implements Identifiable<Long> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    @JoinColumn(name = "cart_id")
    @OrderBy("id asc")
    private List<CartOrderEntry> entries;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public List<CartOrderEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<CartOrderEntry> items) {
        this.entries = items;
    }
}
