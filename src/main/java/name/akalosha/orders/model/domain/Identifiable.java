package name.akalosha.orders.model.domain;

/**
 * Represent some entity that has a unique identifier.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface Identifiable<T> {

    T getId();

    void setId(T id);

}
