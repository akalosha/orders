package name.akalosha.orders.model.domain;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Persistence model of user.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Entity
@Table(name="users")
public class User implements Identifiable<Long> {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private String roles;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        if (StringUtils.isBlank(roles)) {
            return Collections.emptySet();
        }
        return Arrays.stream(roles.split(","))
                .collect(Collectors.toSet());
    }

    public void setRoles(Set<String> roles) {
        if (CollectionUtils.isEmpty(roles)) {
            this.roles = null;
        } else {
            this.roles = String.join(",", roles);
        }
    }
}
