package name.akalosha.orders.model.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.List;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Entity
@Table(name="sites")
public class Site implements Identifiable<Long> {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String urlPatterns;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUrlPatterns() {
        return Arrays.asList(urlPatterns.split("\\|"));
    }

    public void setUrlPatterns(List<String> urlPatterns) {
        this.urlPatterns = String.join("|", urlPatterns);
    }

}
