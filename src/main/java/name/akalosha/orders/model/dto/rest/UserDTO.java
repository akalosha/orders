package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * DTO model of user.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class UserDTO {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String username;

    @JsonProperty
    private Set<String> roles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
