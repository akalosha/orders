package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Model of an indexed site.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
// site is saved as a nested object and doesn't have its own index
public class SiteDoc extends DocBase {

    @Field(type = FieldType.Keyword)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
