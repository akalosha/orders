package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Model of indexed cart/order item.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Document(indexName = "cart_order_entries", type = "cart_order_entry")
public class CartOrderEntryDoc extends DocBase {

    @Field(type = FieldType.Long)
    private long quantity;

    @Field(type = FieldType.Nested)
    private ProductDoc product;

    @Field(type = FieldType.Nested, ignoreFields = "entries")
    private CartDoc cart;

    @Field(type = FieldType.Nested, ignoreFields = "entries")
    private OrderDoc order;


    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public ProductDoc getProduct() {
        return product;
    }

    public void setProduct(ProductDoc product) {
        this.product = product;
    }

    public CartDoc getCart() {
        return cart;
    }

    public void setCart(CartDoc cart) {
        this.cart = cart;
    }

    public OrderDoc getOrder() {
        return order;
    }

    public void setOrder(OrderDoc order) {
        this.order = order;
    }
}
