package name.akalosha.orders.model.dto.rest.pagination;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class SortDTO {

    @JsonProperty
    private List<SortOrderDTO> orders;

    public List<SortOrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<SortOrderDTO> orders) {
        this.orders = orders;
    }
}
