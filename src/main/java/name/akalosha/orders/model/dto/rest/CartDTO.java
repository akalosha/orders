package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO model of cart.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class CartDTO {

    @JsonProperty
    private Long id;

    @JsonProperty
    private BigDecimal total;

    @JsonProperty
    private List<CartOrderEntryDTO> entries = new ArrayList<>();

    @JsonProperty
    private String user;

    @JsonProperty
    private AddressDTO shippingAddress;

    @JsonProperty
    private DeliveryMethodDTO deliveryMethod;

    @JsonProperty
    private SiteDTO site;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CartOrderEntryDTO> getEntries() {
        return entries;
    }

    public void setEntries(List<CartOrderEntryDTO> entries) {
        this.entries = entries;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public AddressDTO getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AddressDTO shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public DeliveryMethodDTO getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethodDTO deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public SiteDTO getSite() {
        return site;
    }

    public void setSite(SiteDTO site) {
        this.site = site;
    }

}
