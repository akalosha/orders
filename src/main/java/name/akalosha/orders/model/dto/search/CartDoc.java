package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

/**
 * Model of an indexed cart.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Document(indexName = "carts", type = "cart")
public class CartDoc extends DocBase {

    @Field(type = FieldType.Keyword)
    private String total;

    @Field(type = FieldType.Nested)
    private List<CartOrderEntryDoc> entries;

    @Field(type = FieldType.Keyword)
    private String user; // no need to have full UserDoc object here

    @Field(type = FieldType.Nested)
    private AddressDoc shippingAddress;

    @Field(type = FieldType.Nested)
    private DeliveryMethodDoc deliveryMethod;

    @Field(type = FieldType.Nested)
    private SiteDoc site;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<CartOrderEntryDoc> getEntries() {
        return entries;
    }

    public void setEntries(List<CartOrderEntryDoc> entries) {
        this.entries = entries;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public AddressDoc getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(AddressDoc shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public DeliveryMethodDoc getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethodDoc deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public SiteDoc getSite() {
        return site;
    }

    public void setSite(SiteDoc site) {
        this.site = site;
    }

}
