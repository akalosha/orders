package name.akalosha.orders.model.dto.rest.pagination;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Page DTO model. Intended use: return type for controller methods that return paginated collections.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class PageDTO<T> {

    @JsonProperty
    private long totalElements;

    @JsonProperty
    private int totalPages;

    @JsonProperty
    private int currentPage;

    @JsonProperty
    private int pageSize;

    @JsonProperty
    private int elementsOnCurrentPage;

    @JsonProperty
    private SortDTO sort;

    @JsonProperty
    private List<T> content;

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getElementsOnCurrentPage() {
        return elementsOnCurrentPage;
    }

    public void setElementsOnCurrentPage(int elementsOnCurrentPage) {
        this.elementsOnCurrentPage = elementsOnCurrentPage;
    }

    public SortDTO getSort() {
        return sort;
    }

    public void setSort(SortDTO sort) {
        this.sort = sort;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }
}
