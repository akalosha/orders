package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO model of a site.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class SiteDTO {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
