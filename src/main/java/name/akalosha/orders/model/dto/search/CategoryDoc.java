package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Model of indexed category.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Document(indexName = "categories", type = "category")
public class CategoryDoc extends DocBase {

    @Field(type = FieldType.Text)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
