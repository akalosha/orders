package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Model of indexed order.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Document(indexName = "orders", type = "order")
public class OrderDoc extends CartDoc {

    @Field(type = FieldType.Long)
    private Long datePlaced;

    public Long getDatePlaced() {
        return datePlaced;
    }

    public void setDatePlaced(Long datePlaced) {
        this.datePlaced = datePlaced;
    }

}
