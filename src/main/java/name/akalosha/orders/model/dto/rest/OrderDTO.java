package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import name.akalosha.orders.rest.json.TimestampDeserializer;
import name.akalosha.orders.rest.json.TimestampSerializer;

import java.sql.Timestamp;

/**
 * DTO model of order.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class OrderDTO extends CartDTO {

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp datePlaced;

    public Timestamp getDatePlaced() {
        return datePlaced;
    }

    public void setDatePlaced(Timestamp datePlaced) {
        this.datePlaced = datePlaced;
    }
}
