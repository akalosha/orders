package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO model of cart or order entry.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class CartOrderEntryDTO {

    @JsonProperty
    private Long id;

    @JsonProperty
    private long quantity;

    @JsonProperty
    private ProductDTO product;

    @JsonProperty
    private CartDTO cart;

    @JsonProperty
    private OrderDTO order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public CartDTO getCart() {
        return cart;
    }

    public void setCart(CartDTO cart) {
        this.cart = cart;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }
}
