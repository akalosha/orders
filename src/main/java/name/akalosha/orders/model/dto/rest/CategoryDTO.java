package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * DTO model of category.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class CategoryDTO {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonProperty
    private List<ProductDTO> products;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }
}
