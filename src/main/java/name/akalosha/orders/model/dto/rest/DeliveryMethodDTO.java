package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import name.akalosha.orders.rest.json.TimestampSerializer;

import java.sql.Timestamp;

/**
 * DTO model of delivery method.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class DeliveryMethodDTO {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String name;

    @JsonIgnore
    private int hoursToDeliver;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonSerialize(using = TimestampSerializer.class)
    private Timestamp estimatedDeliveryDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHoursToDeliver() {
        return hoursToDeliver;
    }

    public void setHoursToDeliver(int hoursToDeliver) {
        this.hoursToDeliver = hoursToDeliver;
    }

    public Timestamp getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(Timestamp estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }
}
