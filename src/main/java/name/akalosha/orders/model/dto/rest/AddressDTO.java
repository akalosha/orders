package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO model of an address.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class AddressDTO {

    @JsonProperty
    private String state;

    @JsonProperty
    private String city;

    @JsonProperty
    private String line1;

    @JsonProperty
    private String line2;

    @JsonProperty
    private String phone;

    @JsonProperty
    private String email;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
