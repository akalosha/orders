package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Model of an indexed delivery method.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
// delivery method is saved as a nested object and doesn't have its own index
public class DeliveryMethodDoc extends DocBase {

    @Field(type = FieldType.Keyword)
    private String name;

    @Field(type = FieldType.Integer)
    private int hoursToDeliver;

    @Field(type = FieldType.Long)
    private Long estimatedDeliveryDate;

    @Field(type = FieldType.Nested)
    private SiteDoc site;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHoursToDeliver() {
        return hoursToDeliver;
    }

    public void setHoursToDeliver(int hoursToDeliver) {
        this.hoursToDeliver = hoursToDeliver;
    }

    public Long getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(Long estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    public SiteDoc getSite() {
        return site;
    }

    public void setSite(SiteDoc site) {
        this.site = site;
    }

}
