package name.akalosha.orders.model.dto.rest.report;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

/**
 * DTO model of revenue report.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class RevenueReport {

    @JsonProperty
    private Map<LocalDate, BigDecimal> revenueByDay;

    public Map<LocalDate, BigDecimal> getRevenueByDay() {
        return revenueByDay;
    }

    public void setRevenueByDay(Map<LocalDate, BigDecimal> revenueByDay) {
        this.revenueByDay = revenueByDay;
    }
}
