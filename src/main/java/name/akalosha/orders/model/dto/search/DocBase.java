package name.akalosha.orders.model.dto.search;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;

/**
 * Due to the issue with Elasticsearch sorting we need to have an additional field 'idNumeric' for all doc classes. By
 * declaring that field in a base class we are reducing the code duplication, i.e. we don't have to declare that field
 * in each and every doc class.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public abstract class DocBase {

    @Id
    @Field(type = FieldType.Long)
    private long id;

    /**
     * This field is used as a workaround for a sorting issue with Elasticsearch. Field 'id' is mapped to a 'keyword'
     * type in Elastic (that's an implementation detail of Spring Data Elasticsearch). The issue is that 'keyword'
     * fields are sorted alphabetically instead of numerically, and that produces undesired results.
     * Example: unsorted [8, 10, 11, 9], alphabetical sort [10, 11, 8, 9], numeric sort [8, 9, 10, 11].
     * So in order to get a correct sorting, we are sorting by this field instead of 'id' field.
     */
    @Field(type = FieldType.Long)
    private Long idNumeric;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdNumeric() {
        return id; // intended - idNumeric should be equal to id
    }

}
