package name.akalosha.orders.model.dto.rest.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class UserRegisterForm {

    @NotBlank
    // OOB email validation, since I do not want to do anything fancy here. If this does not satisfy the requirements,
    // then a custom ConstraintValidator can be created to implement any custom email validation rules.
    @Email
    @Size(min=1, max = 2048, message = "Username length should be between 1 and 2048 characters")
    private String username;

    @NotEmpty
    @Size(min=8, max = 2048, message = "Password length should be between 8 and 2048 characters")
    private char[] password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }
}
