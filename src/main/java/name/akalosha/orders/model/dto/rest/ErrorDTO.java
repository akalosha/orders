package name.akalosha.orders.model.dto.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * DTO model of error message.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class ErrorDTO {

    @JsonProperty
    private String message;

    @JsonProperty
    private String requestURI;

    @JsonProperty
    private String requestMethod;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestURI() {
        return requestURI;
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    @JsonProperty
    public long getTimestamp() {
        return Instant.now().toEpochMilli();
    }

    @JsonProperty
    public String getFormattedDateTime() {
        return DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(OffsetDateTime.now(ZoneOffset.UTC));
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }
}
