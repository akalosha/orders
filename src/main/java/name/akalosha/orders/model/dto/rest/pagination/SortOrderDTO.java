package name.akalosha.orders.model.dto.rest.pagination;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Sort;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class SortOrderDTO {

    @JsonProperty
    private Sort.Direction direction;

    @JsonProperty
    private String property;

    public Sort.Direction getDirection() {
        return direction;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
}
