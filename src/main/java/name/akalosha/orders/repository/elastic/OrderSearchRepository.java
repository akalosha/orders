package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.OrderDoc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for order operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface OrderSearchRepository extends ElasticsearchRepository<OrderDoc, Long> {

    @Query("{\n" +
            "    \"nested\": {\n" +
            "        \"path\": \"entries\",\n" +
            "        \"score_mode\": \"avg\",\n" +
            "        \"query\": {\n" +
            "            \"nested\": {\n" +
            "                \"path\": \"entries.product\",\n" +
            "                \"query\": {\n" +
            "                    \"wildcard\": {\n" +
            "                        \"entries.product.name\": \"*?0*\"\n" +
            "                    }\n" +
            "                }\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "}")
    Page<OrderDoc> findAllByProductNameContaining(Pageable pageable, String productNamePart);

    Page<OrderDoc> findAllByUser(Pageable pageable, String user);

}
