package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.CategoryDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for category operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CategorySearchRepository extends ElasticsearchRepository<CategoryDoc, Long> {
}
