package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.ProductDoc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for product operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface ProductSearchRepository extends ElasticsearchRepository<ProductDoc, Long> {

    /**
     * Returns a list of products with names matching a given pattern. Wildcards (*) are allowed.
     *
     * @param pageable pagination parameters
     * @param name name pattern
     * @return a list of products with matching names
     */
    Page<ProductDoc> findProductDocsByName(Pageable pageable, String name);

}
