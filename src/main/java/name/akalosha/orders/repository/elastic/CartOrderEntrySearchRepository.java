package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.CartOrderEntryDoc;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for cart/order entry operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CartOrderEntrySearchRepository extends ElasticsearchRepository<CartOrderEntryDoc, Long> {

    @Query("{\n" +
            "  \"nested\": {\n" +
            "    \"path\": \"cart\",\n" +
            "    \"score_mode\": \"avg\",\n" +
            "    \"query\": {\n" +
            "      \"term\": {\n" +
            "        \"cart.id\": \"?0\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}")
    Iterable<CartOrderEntryDoc> findAllByCartId(Long cartId);

}
