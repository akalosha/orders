package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.UserDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for user operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface UserSearchRepository extends ElasticsearchRepository<UserDoc, Long> {
}
