package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.AddressDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for address operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface AddressSearchRepository extends ElasticsearchRepository<AddressDoc, Long> {
}
