package name.akalosha.orders.repository.elastic;

import name.akalosha.orders.model.dto.search.CartDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Elasticsearch repository for cart operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CartSearchRepository extends ElasticsearchRepository<CartDoc, Long> {
}
