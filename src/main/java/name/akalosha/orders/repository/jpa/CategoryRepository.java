package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for {@link Category} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
