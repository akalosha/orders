package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for {@link Product} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
