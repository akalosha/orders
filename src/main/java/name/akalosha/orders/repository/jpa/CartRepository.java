package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data repository for {@link Cart} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {

    /**
     * Gets the cart by id, additionally fetching cart entries. This is useful when we want to access cart entries
     * outside of Hibernate session (this can happen in integration tests, for example). Doing so usually will result in
     * {@link org.hibernate.LazyInitializationException}, because by default entries would be fetched lazily and lazy
     * fetch can not work without a session. Fetching the entries eagerly avoids that exception.
     *
     * @param id id of a cart to fetch
     * @return {@link Cart}
     */
    @Query("SELECT c FROM Cart c LEFT JOIN FETCH c.entries WHERE c.id = :id")
    Optional<Cart> findByIdWithEagerFetch(Long id);

}
