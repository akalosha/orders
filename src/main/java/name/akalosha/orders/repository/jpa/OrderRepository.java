package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data repository for {@link Order} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByDatePlacedAfter(Timestamp dateAfter);

    /**
     * Gets the order by id, additionally fetching order entries. This is useful when we want to access order entries
     * outside of Hibernate session (this can happen in integration tests, for example). Doing so usually will result in
     * {@link org.hibernate.LazyInitializationException}, because by default entries would be fetched lazily and lazy
     * fetch can not work without a session. Fetching the entries eagerly avoids that exception.
     *
     * @param id id of an order to fetch
     * @return {@link Order}
     */
    @Query("SELECT o FROM Order o LEFT JOIN FETCH o.entries WHERE o.id = :id")
    Optional<Order> findByIdWithEagerFetch(Long id);

}
