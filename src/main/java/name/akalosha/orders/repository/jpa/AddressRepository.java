package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for {@link Address} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
