package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.Site;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data repository for {@link Site} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface SiteRepository extends JpaRepository<Site, Long> {

    Optional<Site> findSiteByName(String siteName);

}
