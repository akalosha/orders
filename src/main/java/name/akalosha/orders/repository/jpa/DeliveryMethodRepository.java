package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Site;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data repository for {@link DeliveryMethod} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface DeliveryMethodRepository extends JpaRepository<DeliveryMethod, Long> {

    List<DeliveryMethod> findBySite(Site site);

    Optional<DeliveryMethod> findByName(String name);

}
