package name.akalosha.orders.repository.jpa;

import name.akalosha.orders.model.domain.CartOrderEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for {@link CartOrderEntry} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Repository
public interface CartOrderEntryRepository extends JpaRepository<CartOrderEntry, Long> {
}
