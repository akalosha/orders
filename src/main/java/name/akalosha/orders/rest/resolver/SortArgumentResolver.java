package name.akalosha.orders.rest.resolver;

import name.akalosha.orders.model.dto.search.DocBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This resolver ensures that sorting is done using a correct field name. If it finds that a sorting is done using
 * "wrong" property, it would change that to a "correct" property name (i.e. it can change "sort=id,asc" to
 * "sort=idNumeric,asc").
 *
 * See comments to {@link DocBase#idNumeric} class contain more details on why this resolver is needed.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class SortArgumentResolver extends SortHandlerMethodArgumentResolver {

    private static final Logger LOG = LoggerFactory.getLogger(SortArgumentResolver.class);

    private static final String SOURCE_PROP_NAME = "id";
    private static final String TARGET_PROP_NAME = "idNumeric";

    @Override
    public Sort resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        LOG.trace("Resolving sort argument");
        Sort sort = super.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
        LOG.debug("Initial sort value: {}", sort);

        List<Sort.Order> orders = sort.get().collect(Collectors.toList());
        List<Sort.Order> modifiedOrders = new ArrayList<>(orders.size());
        for (Sort.Order order : orders) {
            if (SOURCE_PROP_NAME.equals(order.getProperty())) {
                LOG.debug("Found a sort order that should be modified: {}", order);
                Sort.Order modifiedOrder = new Sort.Order(order.getDirection(), TARGET_PROP_NAME);
                LOG.debug("Modified sort order value: {}", modifiedOrder);
                modifiedOrders.add(modifiedOrder);
            } else {
                modifiedOrders.add(order);
            }
        }

        Sort modifiedSort = Sort.by(modifiedOrders);
        LOG.debug("Modified sort value: {}", modifiedSort);

        return modifiedSort;
    }
}
