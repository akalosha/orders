package name.akalosha.orders.rest.handler;

import name.akalosha.orders.rest.util.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Handler for exceptions that can be thrown in controllers. Overrides default Spring implementation
 * to provide custom response format that is uniform for all exceptions.
 * Exceptions thrown from filters are handled by {@link name.akalosha.orders.rest.filter.ExceptionHandlingFilter}.
 *
 * @see name.akalosha.orders.rest.filter.ExceptionHandlingFilter
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex,
            Object body,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        LOG.error("Exception caught by handler: ", ex);
        return ResponseBuilder.errorResponse(getHttpServletRequest(request), ex);
    }

    private HttpServletRequest getHttpServletRequest(WebRequest webRequest) {
        if (webRequest instanceof ServletWebRequest) {
            ServletWebRequest servletWebRequest = (ServletWebRequest) webRequest;
            return servletWebRequest.getRequest();
        }
        return null;
    }

}
