package name.akalosha.orders.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import name.akalosha.orders.converters.mappers.dto.CartOrderEntryDTOMapper;
import name.akalosha.orders.converters.mappers.util.CollectionMapper;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.dto.rest.CartOrderEntryDTO;
import name.akalosha.orders.rest.util.ResponseBuilder;
import name.akalosha.orders.service.CartOrderEntryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

/**
 * REST controller containing endpoints for managing cart entries.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@RequestMapping(path = "/users/{userId}/carts/{cartId}/entries")
@Api(tags = "Cart Entries", description = "Cart entry endpoints")
public class CartEntryRestController {

    private static final Logger LOG = LoggerFactory.getLogger(CartEntryRestController.class);

    private final CartOrderEntryService cartOrderEntryService;
    private final CartOrderEntryDTOMapper cartOrderEntryDTOMapper;

    @Autowired
    public CartEntryRestController(CartOrderEntryService cartOrderEntryService, CartOrderEntryDTOMapper cartOrderEntryDTOMapper) {
        this.cartOrderEntryService = cartOrderEntryService;
        this.cartOrderEntryDTOMapper = cartOrderEntryDTOMapper;
    }

    /**
     * Returns cart entry with given ID.
     *
     * @param cartId ID of cart containing an entry
     * @param entryId ID of entry
     * @return cart entry instance
     */
    @RequestMapping(
            path = "/{entryId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a single cart entry by ID")
    public ResponseEntity<CartOrderEntryDTO> getCartEntry(
            @ApiParam("ID of entry's cart") @PathVariable("cartId") Long cartId,
            @ApiParam("ID of entry to get") @PathVariable("entryId") Long entryId
    ) {
        LOG.trace("Getting an entry with ID={} for a cart with ID={}", entryId, cartId);
        Optional<CartOrderEntry> entryOptional = cartOrderEntryService.find(cartId, entryId);
        Optional<CartOrderEntryDTO> entryDTOOptional = entryOptional.map(cartOrderEntryDTOMapper::map);
        return ResponseBuilder.responseForGet(entryDTOOptional);
    }

    /**
     * Returns all cart entries for a given cart.
     *
     * @param cartId ID of cart containing an entry
     * @return cart entry list
     */
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets all entries of a cart")
    public ResponseEntity<Collection<CartOrderEntryDTO>> getAllEntries(
            @ApiParam("ID of cart to get entries from") @PathVariable("cartId") Long cartId) {
        LOG.trace("Getting all entries for a cart with ID={}", cartId);
        Collection<CartOrderEntry> entries = cartOrderEntryService.findAll(cartId);
        LOG.debug("Found {} entries for a cart with ID={}", entries.size(), cartId);
        Collection<CartOrderEntryDTO> entryDTOs = CollectionMapper.map(entries, cartOrderEntryDTOMapper);
        return ResponseBuilder.responseForGet(entryDTOs);
    }

    /**
     * Creates new cart entry for a given cart.
     *
     * @param cartId ID of cart to add entry to
     * @param entryDTO entry to create
     * @return created entry
     */
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Creates a new entry in cart")
    public ResponseEntity<CartOrderEntryDTO> addEntry(
            @ApiParam("ID of cart entry is added to") @PathVariable("cartId") Long cartId,
            @ApiParam("Entry to add") @RequestBody CartOrderEntryDTO entryDTO
    ) {
        LOG.trace("Adding new entry for a cart with ID={}", cartId);
        CartOrderEntry entry = cartOrderEntryDTOMapper.mapReverse(entryDTO);
        CartOrderEntry createdEntry = cartOrderEntryService.save(cartId, entry);
        CartOrderEntryDTO createdEntryDTO = cartOrderEntryDTOMapper.map(createdEntry);
        LOG.debug("Added a new entry with ID={} for a cart with ID={}", createdEntryDTO.getId(), cartId);
        return ResponseBuilder.responseForAdd(createdEntryDTO);
    }

    /**
     * Adjusts quantity of existing cart entry.
     *
     * @param cartId ID of cart containing an entry
     * @param entryId ID of entry to edit
     * @param qty new quantity for cart entry
     * @return edited entry
     */
    @RequestMapping(
            path = "/{entryId}/quantity",
            method = {RequestMethod.PUT},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Changes quantity of cart entry")
    public ResponseEntity<CartOrderEntryDTO> editEntryQuantity(
            @ApiParam("ID of cart containing an entry") @PathVariable("cartId") Long cartId,
            @ApiParam("ID of entry to edit") @PathVariable("entryId") Long entryId,
            @ApiParam("New entry quantity") @RequestBody int qty) {
        LOG.trace("Updating an entry with ID={} for a cart with ID={}", entryId, cartId);
        CartOrderEntry editedItem = cartOrderEntryService.editQuantity(cartId, entryId, qty);
        CartOrderEntryDTO editedCartEntryDTO = cartOrderEntryDTOMapper.map(editedItem);
        return ResponseBuilder.responseForEdit(editedCartEntryDTO);
    }

    /**
     * Deletes an entry from a cart.
     *
     * @param cartId ID of cart to delete entry from
     * @param entryId ID of entry to delete
     */
    @RequestMapping(
            path = "/{entryId}",
            method = RequestMethod.DELETE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Deletes cart entry")
    public ResponseEntity deleteEntry(
            @ApiParam("ID of cart to delete entry from") @PathVariable("cartId") Long cartId,
            @ApiParam("ID of entry to delete") @PathVariable("entryId") Long entryId
    ) {
        LOG.trace("Deleting an entry with ID={} for a cart with ID={}", entryId, cartId);
        cartOrderEntryService.delete(cartId, entryId);
        return ResponseBuilder.responseForDelete();
    }

}
