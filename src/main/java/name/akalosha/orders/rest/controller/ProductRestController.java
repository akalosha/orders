package name.akalosha.orders.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import name.akalosha.orders.annotation.PagedApi;
import name.akalosha.orders.converters.mappers.dto.ProductDTOMapper;
import name.akalosha.orders.converters.mappers.dto.SortDTOMapper;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.rest.ProductDTO;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.rest.util.ResponseBuilder;
import name.akalosha.orders.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;

/**
 * REST controller containing {@link Product}-related endpoints.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@RequestMapping(path = "/products")
@Api(tags = "Products", description = "Product endpoints")
public class ProductRestController extends BaseRestController<Product, ProductDTO, Long> {

    private static final Logger LOG = LoggerFactory.getLogger(ProductRestController.class);

    private final ProductService productService;

    @Autowired
    public ProductRestController(
            ProductService productService,
            ProductDTOMapper productMapper,
            SortDTOMapper sortDTOMapper) {
        super(productService, productMapper, sortDTOMapper);
        this.productService = productService;
    }

    /**
     * Returns product with given ID.
     *
     * @param productId ID of product
     * @return product instance
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a single product by ID")
    public ResponseEntity<ProductDTO> getProduct(
            @ApiParam("ID of product to get") @PathVariable("id") Long productId) {
        return get(productId);
    }

    /**
     * Returns all products.
     *
     * @param pageable pagination parameters decoded from query params. Example: page=0&size=5&sort=id,desc
     * @return products list
     */
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets all available products")
    @PagedApi
    public ResponseEntity<PageDTO<ProductDTO>> getAllProducts(@ApiIgnore @Autowired Pageable pageable) {
        return getAll(pageable);
    }

    /**
     * Creates new product.
     *
     * @param product product to create
     * @return created product
     */
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Adds new product")
    public ResponseEntity<ProductDTO> addProduct(@ApiParam("Product to create") @RequestBody @NotNull ProductDTO product) {
        return add(product);
    }

    /**
     * Edits existing product.
     *
     * @param productId ID of product to edit
     * @param product product with edited properties
     * @return edited product
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation(
            value = "Updates existing product's properties",
            notes = "Properties that are not provided in payload will be set to null."
    )
    public ResponseEntity<ProductDTO> editProduct(
            @ApiParam("ID of product to update") @PathVariable("id") Long productId,
            @ApiParam("Product with updated properties") @RequestBody ProductDTO product
    ) {
        product.setId(productId);
        return edit(product);
    }

    /**
     * Edits existing product, but only properties that are provided in request body are changed.
     *
     * @param productId ID of product to edit
     * @param product product with edited properties
     * @return edited product
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation(
            value = "Selectively updates product's properties",
            notes = "Only not null property values are updated. If payload does not contain some property, " +
                    "or if property is null, it will not be updated.")
    public ResponseEntity<ProductDTO> patchProduct(
            @ApiParam("ID of product to update") @PathVariable("id") Long productId, @RequestBody @NonNull ProductDTO product) {
        product.setId(productId);
        return patch(product);
    }

    /**
     * Deletes existing product.
     *
     * @param productId ID of product to delete
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.DELETE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Deletes existing product")
    public ResponseEntity deleteProduct(@ApiParam("ID of product to delete") @PathVariable("id") Long productId) {
        return delete(productId);
    }

    /**
     * Searches products by name.
     *
     * @param nameQuery query to use for search
     * @param pageable pagination parameters decoded from query params. Example: page=0&size=5&sort=id,desc
     * @return products list
     */
    @RequestMapping(
            path = "/search",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a list of products with name matching a given query")
    @PagedApi
    public ResponseEntity<PageDTO<ProductDTO>> searchProductsByName(
            @ApiParam("Product name query") @RequestParam(value = "name", defaultValue = "*") String nameQuery,
            @ApiIgnore @Autowired Pageable pageable) {
        LOG.trace("Getting products with names that math query {}", nameQuery);
        Page<Product> productsPage = productService.findByName(pageable, nameQuery);
        PageDTO<ProductDTO> mappedPage = mapPaginatedCollection(productsPage);
        LOG.debug("Found {} products with names that math query {}", mappedPage.getTotalElements(), nameQuery);
        return ResponseBuilder.responseForGet(mappedPage);
    }

}
