package name.akalosha.orders.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import name.akalosha.orders.annotation.PagedApi;
import name.akalosha.orders.converters.mappers.dto.OrderDTOMapper;
import name.akalosha.orders.converters.mappers.dto.SortDTOMapper;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.OrderDTO;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.model.dto.rest.report.RevenueReport;
import name.akalosha.orders.rest.util.ResponseBuilder;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * REST controller containing {@link OrderDTO}-related endpoints.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@Api(tags = "Orders", description = "Order endpoints")
public class OrderRestController extends BaseRestController<Order, OrderDTO, Long> {

    private static final Logger LOG = LoggerFactory.getLogger(OrderRestController.class);

    private final OrderService orderService;
    private final UserService userService;

    @Autowired
    public OrderRestController(
            OrderService orderService,
            OrderDTOMapper orderDTOMapper,
            SortDTOMapper sortDTOMapper,
            UserService userService) {
        super(orderService, orderDTOMapper, sortDTOMapper);
        this.orderService = orderService;
        this.userService = userService;
    }

    /**
     * Returns order with given ID.
     *
     * @param orderId ID of order
     * @return order instance
     */
    @RequestMapping(
            path = "/users/{userId}/orders/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a single order by ID")
    public ResponseEntity<OrderDTO> getOrder(@ApiParam("ID of order to get") @PathVariable("id") Long orderId) {
        return get(orderId);
    }

    /**
     * Returns all orders.
     *
     * @param pageable pagination parameters decoded from query params. Example: page=0&size=5&sort=id,desc
     * @return orders list
     */
    @RequestMapping(
            path = "/orders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Gets all available orders")
    @PagedApi
    public ResponseEntity<PageDTO<OrderDTO>> getAllOrders(@ApiIgnore @Autowired Pageable pageable) {
        return getAll(pageable);
    }

    /**
     * Generates daily revenue report for orders placed after given date.
     *
     * @param dateStr date since which the report should be generated.
     */
    @RequestMapping(
            path = "/orders/revenue-report",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation(
            value = "Returns a daily revenue report",
            notes = "Report contains revenue for each day since given date."
    )
    public ResponseEntity<RevenueReport> revenueReport(
            @ApiParam(value = "Date starting from which the report should be generated. Format is ISO-8601. Examples: '2011-12-03' or '2011-12-03+01:00'")
            @RequestParam(value = "since", required = true) String dateStr
    ) {
        LOG.trace("Generating a revenue report from date {}", dateStr);
        LocalDate dateSince;
        try {
            dateSince = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE);
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("Unable to parse date.  Check that it is in ISO-8601 format", ex);
        }
        RevenueReport report = orderService.generateRevenueReport(dateSince);
        return ResponseBuilder.responseForGet(report);
    }

    /**
     * Returns list of orders containing products with matching name.
     *
     * @param productName part of product name to search for
     * @return list of matching orders
     */
    // Why POST? IMO this endpoint looks like a controller and not resource (speaking in terms of REST). And controllers should use POST.
    @RequestMapping(
            path = "/orders/search-by-product",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation(value = "Returns orders which contain items which contain products with matching name")
    @PagedApi
    public ResponseEntity<PageDTO<OrderDTO>> searchByProduct(
            @ApiParam(value = "Product name (or part of it) to search for. Case-insensitive.")
            @RequestParam(name = "productName", required = true) String productName,
            @ApiIgnore @Autowired Pageable pageable
    ) {
        LOG.trace("Searching for orders containing products with name {}", productName);
        Page<Order> orderPage = orderService.findOrdersByProductName(pageable, productName.toLowerCase());
        PageDTO<OrderDTO> orderDTOPage = mapPaginatedCollection(orderPage);
        LOG.debug("Found {} orders containing products with name {}", orderDTOPage.getTotalElements() ,productName);
        return ResponseBuilder.responseForGet(orderDTOPage);
    }

    /**
     * Returns order history for current user.
     *
     * @param pageable pagination/sort parameters
     * @return order history
     */
    @RequestMapping(
            path = "/users/{userId}/orders/history",
            method = {RequestMethod.GET}
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Returns order history for current user")
    @PagedApi
    public ResponseEntity<PageDTO<OrderDTO>> getOrderHistory(@ApiIgnore @Autowired Pageable pageable) {
        User currentUser = userService.getCurrentUser();
        LOG.trace("Getting order history for user {}", currentUser.getUsername());
        Page<Order> orderHistory = orderService.getOrderHistory(pageable, currentUser);
        PageDTO<OrderDTO> orderDTOPage = mapPaginatedCollection(orderHistory);
        LOG.debug("Found {} orders for user {}", orderDTOPage.getTotalElements(), currentUser.getUsername());
        return ResponseBuilder.responseForGet(orderDTOPage);
    }

}
