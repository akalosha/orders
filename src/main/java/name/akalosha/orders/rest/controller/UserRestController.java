package name.akalosha.orders.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import name.akalosha.orders.annotation.PagedApi;
import name.akalosha.orders.converters.mappers.dto.SortDTOMapper;
import name.akalosha.orders.converters.mappers.dto.UserDTOMapper;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.UserDTO;
import name.akalosha.orders.model.dto.rest.form.UserRegisterForm;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.rest.util.ResponseBuilder;
import name.akalosha.orders.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Optional;

/**
 * REST controller containing {@link UserDTO}-related endpoints.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@RequestMapping(path = "/users")
@Api(tags = "Users", description = "User endpoints")
public class UserRestController extends BaseRestController<User, UserDTO, Long> {

    private static final Logger LOG = LoggerFactory.getLogger(UserRestController.class);

    private final UserService userService;
    private final UserDTOMapper userMapper;

    @Autowired
    public UserRestController(UserService userService, UserDTOMapper userMapper, SortDTOMapper sortDTOMapper) {
        super(userService, userMapper, sortDTOMapper);
        this.userService = userService;
        this.userMapper = userMapper;
    }

    /**
     * Returns user with given ID.
     *
     * @param userId ID of user to get
     * @return user instance
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a single user by ID")
    public ResponseEntity<UserDTO> getUserById(@ApiParam("ID of user to get") @PathVariable("id") Long userId) {
        return get(userId);
    }

    /**
     * Returns user with given username.
     *
     * @param username username of user to get
     * @return user instance
     */
    @RequestMapping(
            path = "/get-by-username",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Gets a single user by username")
    public ResponseEntity<UserDTO> getUserByUsername(@ApiParam("Username of user to get") @RequestParam(name = "username") @NotBlank String username) {
        LOG.trace("Getting a user by username {}", username);
        Optional<User> user = userService.findByUsername(username);
        Optional<UserDTO> userDTO = user.map(userMapper::map);
        return ResponseBuilder.responseForGet(userDTO);
    }

    /**
     * Returns details of currently authenticated user.
     *
     * @return user instance
     */
    @RequestMapping(
            path = "/current",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets details of currently authenticated user")
    public ResponseEntity<UserDTO> getCurrentUser() {
        LOG.trace("Getting current user");
        User user = userService.getCurrentUser();
        UserDTO userDTO = userMapper.map(user);
        return ResponseBuilder.responseForGet(userDTO);
    }

    /**
     * Returns all users.
     *
     * @param pageable pagination parameters decoded from query params. Example: page=0&size=5&sort=id,desc
     * @return users list
     */
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured("ROLE_ADMIN")
    @ApiOperation("Gets all available users")
    @PagedApi
    public ResponseEntity<PageDTO<UserDTO>> getAllUsers(@ApiIgnore @Autowired Pageable pageable) {
        return getAll(pageable);
    }

    /**
     * Registers a new user.
     *
     * @param registerForm form that contains user data
     * @return created user
     */
    @RequestMapping(
            path = "/register",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(value = "Registers new user", notes = "This method would create only regular users, it can not create admin users.")
    public ResponseEntity<UserDTO> register(
            @ApiParam("Form that contains user data") @RequestBody @Valid UserRegisterForm registerForm) {
        LOG.trace("Registering a new user with username {}", registerForm.getUsername());
        User user = userService.register(registerForm);
        UserDTO userDTO = userMapper.map(user);
        LOG.debug("Registered a new user with username={} and ID={}", userDTO.getUsername(), userDTO.getId());
        return ResponseBuilder.responseForAdd(userDTO);
    }

}
