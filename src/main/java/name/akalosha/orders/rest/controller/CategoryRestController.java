package name.akalosha.orders.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import name.akalosha.orders.annotation.PagedApi;
import name.akalosha.orders.converters.mappers.dto.CategoryDTOMapper;
import name.akalosha.orders.converters.mappers.dto.SortDTOMapper;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.rest.CategoryDTO;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * REST controller containing {@link CategoryDTO}-related endpoints.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@RequestMapping(path = "/categories")
@Api(tags = "Categories", description = "Category endpoints")
public class CategoryRestController extends BaseRestController<Category, CategoryDTO, Long> {

    @Autowired
    public CategoryRestController(
            CategoryService categoryService,
            CategoryDTOMapper categoryMapper,
            SortDTOMapper sortDTOMapper) {
        super(categoryService, categoryMapper, sortDTOMapper);
    }

    /**
     * Returns category with given ID.
     *
     * @param categoryId ID of category
     * @return category instance
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a single category by ID")
    public ResponseEntity<CategoryDTO> getCategory(@ApiParam("ID of category to get") @PathVariable("id") Long categoryId) {
        return get(categoryId);
    }

    /**
     * Returns all categories.
     *
     * @param pageable pagination parameters decoded from query params. Example: page=0&size=5&sort=id,desc
     * @return categories list
     */
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets all available categories")
    @PagedApi
    public ResponseEntity<PageDTO<CategoryDTO>> getAllCategories(@ApiIgnore @Autowired Pageable pageable) {
        return getAll(pageable);
    }

    /**
     * Creates new category.
     *
     * @param category category to create
     * @return created category
     */
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Adds new category")
    public ResponseEntity<CategoryDTO> addCategory(@ApiParam("Category to create") @RequestBody CategoryDTO category) {
        return add(category);
    }

    /**
     * Edits existing category.
     *
     * @param categoryId ID of category to edit
     * @param category category with edited properties
     * @return edited category
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation(
            value = "Updates existing category's properties",
            notes = "Properties that are not provided in payload will be set to null."
    )
    public ResponseEntity<CategoryDTO> editCategory(
            @ApiParam("ID of category to update") @PathVariable("id") Long categoryId,
            @ApiParam("Category with updated properties") @RequestBody CategoryDTO category
    ) {
        category.setId(categoryId);
        return edit(category);
    }

    /**
     * Deletes existing category.
     *
     * @param categoryId ID of category to delete
     */
    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.DELETE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Deletes existing category")
    public ResponseEntity deleteCategory(@ApiParam("ID of category to delete") @PathVariable("id") Long categoryId) {
        return delete(categoryId);
    }

}
