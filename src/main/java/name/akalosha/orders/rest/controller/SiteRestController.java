package name.akalosha.orders.rest.controller;

import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * REST controller containing {@link Product}-related endpoints.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@RequestMapping(path = "/sites")
public class SiteRestController { // TODO remove this

    private final SiteService siteService;

    @Autowired
    public SiteRestController(SiteService siteService) {
        this.siteService = siteService;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Void> getCurrentSite(HttpServletRequest request) {
        siteService.resolveCurrentSite(request);
        return ResponseEntity.noContent().build();
    }

}
