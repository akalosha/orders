package name.akalosha.orders.rest.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import name.akalosha.orders.annotation.PagedApi;
import name.akalosha.orders.converters.mappers.dto.CartDTOMapper;
import name.akalosha.orders.converters.mappers.dto.DeliveryMethodDTOMapper;
import name.akalosha.orders.converters.mappers.dto.OrderDTOMapper;
import name.akalosha.orders.converters.mappers.dto.SiteDTOMapper;
import name.akalosha.orders.converters.mappers.dto.SortDTOMapper;
import name.akalosha.orders.converters.mappers.util.CollectionMapper;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.model.dto.rest.CartDTO;
import name.akalosha.orders.model.dto.rest.DeliveryMethodDTO;
import name.akalosha.orders.model.dto.rest.OrderDTO;
import name.akalosha.orders.model.dto.rest.SiteDTO;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.rest.util.ResponseBuilder;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.DeliveryMethodService;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.service.SiteService;
import name.akalosha.orders.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

/**
 * REST controller containing {@link CartDTO}-related endpoints.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@RestController
@Api(tags = "Carts", description = "Cart endpoints")
public class CartRestController extends BaseRestController<Cart, CartDTO, Long> {

    private static final Logger LOG = LoggerFactory.getLogger(CartRestController.class);

    private final OrderService orderService;
    private final UserService userService;
    private final CartService cartService;
    private final SiteService siteService;
    private final DeliveryMethodService deliveryMethodService;
    private final OrderDTOMapper orderDTOMapper;
    private final DeliveryMethodDTOMapper deliveryMethodDTOMapper;
    private final SiteDTOMapper siteDTOMapper;

    @Autowired
    public CartRestController(
            OrderService orderService,
            UserService userService,
            CartService cartService,
            SiteService siteService,
            DeliveryMethodService deliveryMethodService,
            CartDTOMapper cartDTOMapper,
            OrderDTOMapper orderDTOMapper,
            DeliveryMethodDTOMapper deliveryMethodDTOMapper,
            SortDTOMapper sortDTOMapper,
            SiteDTOMapper siteDTOMapper) {
        super(cartService, cartDTOMapper, sortDTOMapper);
        this.orderService = orderService;
        this.userService = userService;
        this.cartService = cartService;
        this.siteService = siteService;
        this.deliveryMethodService = deliveryMethodService;
        this.orderDTOMapper = orderDTOMapper;
        this.deliveryMethodDTOMapper = deliveryMethodDTOMapper;
        this.siteDTOMapper = siteDTOMapper;
    }

    /**
     * Returns cart with given ID.
     *
     * @param cartId ID of cart
     * @return cart instance
     */
    @RequestMapping(
            path = "/users/{userId}/carts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Gets a single cart by ID")
    public ResponseEntity<CartDTO> getCart(@ApiParam("ID of cart to get") @PathVariable("id") Long cartId) {
        return get(cartId);
    }

    /**
     * Returns all carts.
     *
     * @param pageable pagination parameters decoded from query params. Example: page=0&size=5&sort=id,desc
     * @return carts list
     */
    @RequestMapping(
            path = "/carts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_ADMIN"})
    @ApiOperation("Gets all available carts")
    @PagedApi
    public ResponseEntity<PageDTO<CartDTO>> getAllCarts(@ApiIgnore @Autowired Pageable pageable) {
        return getAll(pageable);
    }

    /**
     * Creates a new cart.
     *
     * @return created cart
     */
    @RequestMapping(
            path = "/users/{userId}/carts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation(value = "Creates a new cart", notes = "This endpoint does not require a payload.")
    public ResponseEntity<CartDTO> addCart(@Autowired @ApiIgnore HttpServletRequest request) {
        // Cart has no properties that can be set buy user, so we are creating CartDTO in place
        // instead of getting it from request payload. Also, current user is set as a cart owner.
        CartDTO cartDTO = new CartDTO();
        String username = userService.getCurrentUser().getUsername();
        cartDTO.setUser(username);

        Site site = siteService.resolveCurrentSite(request);
        SiteDTO siteDTO = siteDTOMapper.map(site);
        cartDTO.setSite(siteDTO);

        return add(cartDTO);
    }

    /**
     * Sets shipping address to a cart.
     *
     * @param cartId ID of cart to set a shipping address to
     */
    @RequestMapping(
            path = "/users/{userId}/carts/{id}/shipping-address",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Sets shipping address to a cart with a given ID")
    public ResponseEntity<CartDTO> setShippingAddress(
            @ApiParam("ID of a cart to set a shipping address to") @PathVariable("id") Long cartId,
            @ApiParam("Shipping address to set") @RequestBody @Valid AddressForm address) {
        LOG.trace("Setting shipping address to a cart with ID={}", cartId);
        Cart cartWithAddress = cartService.setShippingAddress(cartId, address);
        LOG.debug("Set a shipping address with ID={} to a cart with ID={}", cartWithAddress.getShippingAddress().getId() ,cartId);
        CartDTO cartDTO = getPersistenceToDtoMapper().map(cartWithAddress);
        return ResponseBuilder.responseForAdd(cartDTO);
    }

    /**
     * Returns a list of available delivery methods for cart.
     *
     * @param cartId ID of cart to get delivery methods for
     */
    @RequestMapping(
            path = "/users/{userId}/carts/{id}/delivery-methods",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Returns a list of available delivery methods for cart with given ID")
    public ResponseEntity<Collection<DeliveryMethodDTO>> getDeliveryMethods(
            @ApiParam("ID of cart to get delivery methods for") @PathVariable("id") Long cartId,
            @Autowired @ApiIgnore HttpServletRequest request) {
        LOG.trace("Getting delivery methods for a cart with ID={}", cartId);
        Site site = siteService.resolveCurrentSite(request);
        // Currently we are not using cartId when selecting available delivery methods. However, cartId can be used at
        // later stages in case if we would need to implement some logic to filter delivery methods based on cart attributes.
        List<DeliveryMethod> deliveryMethods = deliveryMethodService.getDeliveryMethodsForSite(site);
        LOG.debug("Found {} shipping address for a cart with ID={}", deliveryMethods.size(), cartId);
        Collection<DeliveryMethodDTO> deliveryMethodDTOs = CollectionMapper.map(deliveryMethods, deliveryMethodDTOMapper);
        return ResponseBuilder.responseForGet(deliveryMethodDTOs);
    }

    /**
     * Sets delivery method to a cart.
     *
     * @param cartId ID of cart to set a delivery method to
     */
    @RequestMapping(
            path = "/users/{userId}/carts/{id}/delivery-method",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Sets delivery method to a cart with a given ID")
    public ResponseEntity<CartDTO> setDeliveryMethod(
            @ApiParam("ID of a cart to set a delivery method to") @PathVariable("id") Long cartId,
            @ApiParam("ID of a delivery method to set") @RequestParam Long deliveryMethodId) {
        LOG.trace("Setting a delivery method with ID={} to a cart with ID={}", deliveryMethodId, cartId);
        Cart cartWithDeliveryMethod = cartService.setDeliveryMethod(cartId, deliveryMethodId);
        CartDTO cartDTO = getPersistenceToDtoMapper().map(cartWithDeliveryMethod);
        return ResponseBuilder.responseForAdd(cartDTO);
    }

    /**
     * Places an order for cart.
     *
     * @param cartId ID of cart to create an order from
     */
    @RequestMapping(
            path = "/users/{userId}/carts/{id}/place-order",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @ApiOperation("Places an order for cart with given ID")
    public ResponseEntity<OrderDTO> placeOrder(@ApiParam("ID of cart to create an order from") @PathVariable("id") Long cartId) {
        LOG.trace("Placing an order for a cart with ID={}", cartId);
        Order placedOrder = orderService.placeOrder(cartId);
        OrderDTO orderDTO = orderDTOMapper.map(placedOrder);
        LOG.debug("Placed an order with ID={}", orderDTO.getId());
        return ResponseBuilder.responseForGet(orderDTO);
    }

}