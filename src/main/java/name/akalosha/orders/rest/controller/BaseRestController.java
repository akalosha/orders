package name.akalosha.orders.rest.controller;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.dto.SortDTOMapper;
import name.akalosha.orders.converters.mappers.util.PaginationMappingUtil;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Identifiable;
import name.akalosha.orders.model.dto.rest.pagination.PageDTO;
import name.akalosha.orders.rest.util.ResponseBuilder;
import name.akalosha.orders.service.CRUDService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.function.Function;

/**
 * Generic CRUD controller that is meant to be extended.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
abstract class BaseRestController<PERSISTENCE_TYPE extends Identifiable<ID>, DTO_TYPE, ID> {

    private static final Logger LOG = LoggerFactory.getLogger(BaseRestController.class);

    private final CRUDService<PERSISTENCE_TYPE, ID> crudService;
    private final BidirectionalMapper<PERSISTENCE_TYPE, DTO_TYPE> persistenceToDtoMapper;
    private final SortDTOMapper sortDTOMapper;

    BaseRestController(
            CRUDService<PERSISTENCE_TYPE, ID> crudService,
            BidirectionalMapper<PERSISTENCE_TYPE, DTO_TYPE> persistenceToDtoMapper,
            SortDTOMapper sortDTOMapper) {
        Assert.notNull(crudService, "CRUD service should not be null");
        Assert.notNull(persistenceToDtoMapper, "Persistence to DTO mapper should not be null");
        Assert.notNull(sortDTOMapper, "Sort mapper should not be null");

        this.crudService = crudService;
        this.persistenceToDtoMapper = persistenceToDtoMapper;
        this.sortDTOMapper = sortDTOMapper;
    }

    protected ResponseEntity<DTO_TYPE> get(ID id) {
        Assert.notNull(id, "ID should not be null");
        LOG.trace("Getting entity with ID={}", id);
        Optional<PERSISTENCE_TYPE> persistenceEntity = crudService.find(id);
        if (persistenceEntity.isPresent()) {
            LOG.debug("Found {} entity with ID={}", persistenceEntity.get().getClass().getSimpleName(), id);
        }  else {
            LOG.debug("No entity with ID={} was found", id);
        }
        Optional<DTO_TYPE> dtoEntity = persistenceEntity.map(persistenceToDtoMapper::map);
        return ResponseBuilder.responseForGet(dtoEntity);
    }

    protected ResponseEntity<PageDTO<DTO_TYPE>> getAll(Pageable pageable) {
        Assert.notNull(pageable, "Pageable should not be null");
        LOG.trace("Getting all entities");
        Page<PERSISTENCE_TYPE> searchPage = crudService.findAll(pageable);
        if (!searchPage.isEmpty()) {
            LOG.debug("Number of {} entities found: {}", searchPage.stream().findFirst().get().getClass().getSimpleName(), searchPage.getTotalElements());
        }  else {
            LOG.debug("No entities were found");
        }
        PageDTO<DTO_TYPE> dtoPage = mapPaginatedCollection(searchPage);
        return ResponseBuilder.responseForGet(dtoPage);
    }

    protected PageDTO<DTO_TYPE> mapPaginatedCollection(Page<PERSISTENCE_TYPE> srcPage) {
        return PaginationMappingUtil.mapPage(srcPage, persistenceToDtoMapper, sortDTOMapper);
    }

    protected ResponseEntity<DTO_TYPE> add(DTO_TYPE entity) {
        Assert.notNull(entity, "Entity should not be null");
        LOG.trace("Adding new entity of type {}", entity.getClass().getSimpleName());
        PERSISTENCE_TYPE entityToAdd = persistenceToDtoMapper.mapReverse(entity);
        PERSISTENCE_TYPE addedEntity = crudService.save(entityToAdd);
        LOG.debug("Created new entity of type {} with ID={}", entity.getClass().getSimpleName(), addedEntity.getId());
        DTO_TYPE dtoEntity = persistenceToDtoMapper.map(addedEntity);
        return ResponseBuilder.responseForAdd(dtoEntity);
    }

    private ResponseEntity<DTO_TYPE> update(DTO_TYPE entity, Function<PERSISTENCE_TYPE, PERSISTENCE_TYPE> editor) {
        Assert.notNull(entity, "Entity should not be null");
        Assert.notNull(editor, "Editor function should not be null");

        PERSISTENCE_TYPE entityToEdit = persistenceToDtoMapper.mapReverse(entity);
        PERSISTENCE_TYPE editedEntity = editor.apply(entityToEdit);
        LOG.debug("Updated an entity of type {} with ID={}", entity.getClass().getSimpleName(), editedEntity.getId());
        DTO_TYPE dtoEntity = persistenceToDtoMapper.map(editedEntity);
        return ResponseBuilder.responseForEdit(dtoEntity);
    }

    protected ResponseEntity<DTO_TYPE> edit(DTO_TYPE entity) {
        LOG.trace("Updating an entity of type {}", entity.getClass().getSimpleName());
        return update(entity, crudService::edit);
    }

    protected ResponseEntity<DTO_TYPE> patch(DTO_TYPE entity) {
        LOG.trace("Patching an entity of type {}", entity.getClass().getSimpleName());
        return update(entity, crudService::patch);
    }

    protected ResponseEntity delete(ID id) {
        Assert.notNull(id, "ID should not be null");
        LOG.trace("Deleting an entity with ID={}", id);
        Optional<PERSISTENCE_TYPE> entity = crudService.find(id);
        if (entity.isEmpty()) {
            throw new NotFoundException();
        }
        crudService.delete(id);
        LOG.debug("Deleted an entity with type {} and ID={}", entity.get().getClass().getSimpleName(), id);
        return ResponseBuilder.responseForDelete();
    }

    public CRUDService<PERSISTENCE_TYPE, ID> getCrudService() {
        return crudService;
    }

    public BidirectionalMapper<PERSISTENCE_TYPE, DTO_TYPE> getPersistenceToDtoMapper() {
        return persistenceToDtoMapper;
    }
}
