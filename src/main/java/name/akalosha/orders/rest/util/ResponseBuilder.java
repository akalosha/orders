package name.akalosha.orders.rest.util;

import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.exception.ServerException;
import name.akalosha.orders.model.dto.rest.ErrorDTO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Common response builder for REST controllers. Allows responses of all controllers to be generated in uniform way.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public final class ResponseBuilder {

    private ResponseBuilder() throws IllegalAccessException {
        throw new IllegalAccessException();
    }

    /**
     * Builds response for a GET request. Status is either 200 (entity was found) or 404 (entity was not found).
     *
     * @param entity requested entity, possibly null
     * @param <T> entity type
     * @return response object ready to be returned from controller
     */
    public static <T> ResponseEntity<T> responseForGet(T entity) {
        if (entity == null) {
            return notFound();
        } else {
            return ResponseEntity.ok(entity);
        }
    }

    /**
     * Builds response for a GET request. Status is either 200 (entity was found) or 404 (entity was not found).
     *
     * @param entity Optional holding the requested entity, possibly empty
     * @param <T> entity type
     * @return response object ready to be returned from controller
     */
    public static <T> ResponseEntity<T> responseForGet(Optional<T> entity) {
        return entity.map(ResponseEntity::ok).orElseGet(ResponseBuilder::notFound);
    }

    /**
     * Builds response for a POST request that creates some entity. Status is either 200 (entity was created)
     * or 500 (failed to create an entity).
     *
     * @param entity created entity, possibly null
     * @param <T> entity type
     * @return response object ready to be returned from controller
     */
    public static <T> ResponseEntity<T> responseForAdd(T entity) {
        if (entity == null) {
            throw new ServerException("Failed to add entity");
        }
        return ResponseEntity.ok(entity);
    }

    /**
     * Builds response for a PUT/PATCH request. Status is either 200 (edit was successful) or 500 (edit was not successful).
     *
     * @param entity edited entity, possibly null
     * @param <T> entity type
     * @return response object ready to be returned from controller
     */
    public static <T> ResponseEntity<T> responseForEdit(T entity) {
        if (entity == null) {
            throw new ServerException("Failed to edit entity");
        }
        return ResponseEntity.ok(entity);
    }

    /**
     * Builds response for a DELETE request. Status is 204 (entity was deleted).
     *
     * @return response object ready to be returned from controller
     */
    public static ResponseEntity responseForDelete() {
        return ResponseEntity.noContent().build();
    }

    /**
     * Builds "entity not found" response. Status is 404.
     *
     * @param <T> entity type
     * @return response object ready to be returned from controller
     */
    private static <T> ResponseEntity<T> notFound() {
        // exception will be caught by ExceptionHandlingFilter which will build a proper response.
        // This way we are keeping error response building in one place.
        throw new NotFoundException();
    }

    /**
     * Creates an error message for given exception.
     *
     * @param request request for which the exception was thrown
     * @param ex exception
     * @return response object ready to be returned
     */
    public static ResponseEntity<Object> errorResponse(HttpServletRequest request, Exception ex) {
        ErrorDTO entity = new ErrorDTO();
        if (request != null) {
            entity.setRequestURI(request.getRequestURI());
            entity.setRequestMethod(request.getMethod());
        }

        String message = StringUtils.trimToNull(ex.getMessage());
        // when number of IFs would go out of hand, this can be refactored to individual handler methods
        if(ex instanceof IllegalArgumentException) {
            entity.setMessage(message == null ? "Invalid request" : "Invalid request: " + message);
            return new ResponseEntity<>(entity, HttpStatus.BAD_REQUEST);
        }
        if(ex instanceof NotFoundException) {
            entity.setMessage(message == null ? "Entity not found" : "Entity not found: " + message);
            return new ResponseEntity<>(entity, HttpStatus.NOT_FOUND);
        }
        if (ex instanceof MethodArgumentNotValidException) {
            return handleMethodArgumentNotValidException(entity, (MethodArgumentNotValidException) ex);
        }
        entity.setMessage(message == null ? "Server error" : "Server error: " + message);

        return new ResponseEntity<>(entity, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Exception handler for {@link MethodArgumentNotValidException} exceptions. It populates error message with
     * info on the cause of validation failure.
     *
     * @param errorDTO error object to populate
     * @param ex validation exception
     * @return response object ready to be returned
     */
    private static ResponseEntity<Object> handleMethodArgumentNotValidException(
            ErrorDTO errorDTO, MethodArgumentNotValidException ex) {
        Assert.notNull(errorDTO, "Error DTO should not be null");
        Assert.notNull(ex, "Exception should not be null");

        List<ObjectError> allErrors = ex.getBindingResult().getAllErrors();
        String message = CollectionUtils.emptyIfNull(allErrors).stream()
                .map(error -> {
                    String objectName = error.getObjectName();
                    String fieldName = ((FieldError) error).getField();
                    String rejectedValue = Objects.toString(((FieldError) error).getRejectedValue());
                    String errorMessage = error.getDefaultMessage();
                    return String.format("{field: %s.%s, value: %s, message: %s}", objectName, fieldName, rejectedValue, errorMessage);
                })
                .collect(Collectors.joining("; "));
        errorDTO.setMessage("Validation failed: " + message);

        return new ResponseEntity<>(errorDTO, HttpStatus.BAD_REQUEST);
    }

}
