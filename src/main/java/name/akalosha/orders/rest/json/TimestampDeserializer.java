package name.akalosha.orders.rest.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import name.akalosha.orders.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;

/**
 * Deserializes ISO date string as {@linkplain Timestamp}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class TimestampDeserializer extends JsonDeserializer<Timestamp> {

    private static final Logger LOG = LoggerFactory.getLogger(TimestampDeserializer.class);

    @Override
    public Timestamp deserialize(JsonParser jsonParser, DeserializationContext context)
            throws IOException {
        String isoDate = jsonParser.getText();
        LOG.trace("Deserializing a date value {}", isoDate);
        if (isoDate != null) {
            ZonedDateTime zonedDateTime = ZonedDateTime.parse(isoDate, DateTimeUtil.DATE_TIME_FORMATTER);
            return DateTimeUtil.toTimestamp(zonedDateTime);
        } else {
            return null;
        }
    }

}
