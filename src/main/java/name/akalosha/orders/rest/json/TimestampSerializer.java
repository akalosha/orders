package name.akalosha.orders.rest.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import name.akalosha.orders.util.DateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

/**
 * Serializes {@linkplain Timestamp} as ISO date string.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class TimestampSerializer extends JsonSerializer<Timestamp> {

    private static final Logger LOG = LoggerFactory.getLogger(TimestampSerializer.class);

    @Override
    public void serialize(Timestamp timestamp, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        LOG.trace("Serializing a timestamp {}", timestamp);
        if (timestamp == null) {
            jsonGenerator.writeNull();
        } else {
            // timestamp represents an instant in UTC time zone, so UTC should be used when converting timestamp to date-time
            OffsetDateTime offsetDateTime = timestamp.toInstant().atOffset(ZoneOffset.UTC);
            String isoDate = DateTimeUtil.DATE_TIME_FORMATTER.format(offsetDateTime);
            LOG.debug("Serialized a timestamp {} to date {}", timestamp, isoDate);
            jsonGenerator.writeString(isoDate);
        }
    }

}
