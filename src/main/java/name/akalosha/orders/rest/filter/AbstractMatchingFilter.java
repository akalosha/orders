package name.akalosha.orders.rest.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Base class for filters that need to match request URI against regex pattern.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public abstract class AbstractMatchingFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractMatchingFilter.class);

    protected Pattern pattern;

    protected abstract String getRegex();
    protected abstract String getCapturingGroupName();

    @PostConstruct
    public void init() {
        pattern = Pattern.compile(getRegex());
    }

    /**
     * Extracts a value from request URI based on regex.
     *
     * @param httpServletRequest request to extract value from
     * @param valueConverter function to convert the value to desired type
     * @param <T> result type
     * @return extracted value, possibly empty
     */
    protected  <T> Optional<T> getMatchedValue(HttpServletRequest httpServletRequest, Function<String, T> valueConverter) {
        Assert.notNull(httpServletRequest, "Request should not be null");
        Assert.notNull(valueConverter, "Value converter should not be null");

        String requestURI = httpServletRequest.getRequestURI();
        Matcher matcher = pattern.matcher(requestURI);
        LOG.debug("Created a matcher {} for URI {}", matcher, requestURI);
        if (matcher.find()) {
            String capturedValue = matcher.group(getCapturingGroupName());
            LOG.debug("Extracted raw value: {}", capturedValue);
            T convertedValue;
            try {
                convertedValue = valueConverter.apply(capturedValue);
                LOG.debug("Converted value: {}", capturedValue);
            } catch (Exception ex) {
                LOG.error("Failed to convert matched value '{}' to target type. URI was '{}', regex was '{}'",
                        capturedValue, requestURI, getRegex());
                throw ex;
            }
            return Optional.of(convertedValue);
        }
        return Optional.empty();
    }

    /**
     * Extracts long value from request URI based on regex.
     *
     * @param httpServletRequest request to extract value from
     * @return extracted long value, possibly empty
     */
    protected Optional<Long> getMatchedLongValue(HttpServletRequest httpServletRequest) {
        return getMatchedValue(httpServletRequest, Long::valueOf);
    }

}
