package name.akalosha.orders.rest.filter;

import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Ensures that authenticated user can access only their own orders. Forbids access to orders of other users.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class OrderMatchingFilter extends AbstractMatchingFilter {

    private static final Logger LOG = LoggerFactory.getLogger(OrderMatchingFilter.class);

    @Value("${filter.order.uripattern}")
    protected String regex;
    @Value("${filter.order.groupname}")
    protected String capturingGroupName;

    private UserService userService;
    private OrderService orderService;

    @Autowired
    public OrderMatchingFilter(UserService userService, OrderService orderService) {
        this.userService = userService;
        this.orderService = orderService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException
    {
        Optional<Long> orderIdOptional = getMatchedLongValue(request);
        if (orderIdOptional.isPresent()) {
            Long orderId = orderIdOptional.get();
            LOG.debug("Running an OrderMatchingFilter for orderId {}", orderId);
            Optional<Order> orderOptional = orderService.find(orderId);
            if (orderOptional.isEmpty()) {
                // if we have invalid orderId in URI, fail fast
                throw new NotFoundException(Order.class, orderId);
            }
            String currentUserName = userService.getCurrentUser().getUsername();
            String orderUserName = orderOptional.get().getUser().getUsername();
            if (!Objects.equals(currentUserName, orderUserName)) {
                // currently authenticated user tries to access other user's cart - forbid doing that
                LOG.error("User '{}' attempted illegal access to order '{}' owned by '{}' user", currentUserName, orderId, orderUserName);
                throw new IllegalArgumentException("Authenticated user does not own requested order");
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected String getRegex() {
        return regex;
    }

    @Override
    protected String getCapturingGroupName() {
        return capturingGroupName;
    }

}
