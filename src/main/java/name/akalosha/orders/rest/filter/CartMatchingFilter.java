package name.akalosha.orders.rest.filter;

import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Ensures that authenticated user can access only their own carts. Forbids access to carts of other users.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class CartMatchingFilter extends AbstractMatchingFilter {

    private static final Logger LOG = LoggerFactory.getLogger(CartMatchingFilter.class);

    @Value("${filter.cart.uripattern}")
    protected String regex;
    @Value("${filter.cart.groupname}")
    protected String capturingGroupName;

    private UserService userService;
    private CartService cartService;

    @Autowired
    public CartMatchingFilter(UserService userService, CartService cartService) {
        this.userService = userService;
        this.cartService = cartService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException
    {
        Optional<Long> cartIdOptional = getMatchedLongValue(request);
        if (cartIdOptional.isPresent()) {
            Long cartId = cartIdOptional.get();
            LOG.debug("Running a CartMatchingFilter for cartId {}", cartId);
            Optional<Cart> cartOptional = cartService.find(cartId);
            if (cartOptional.isEmpty()) {
                // if we have invalid cartId in URI, fail fast
                throw new NotFoundException(Cart.class, cartId);
            }
            String currentUserName = userService.getCurrentUser().getUsername();
            String cartUserName = cartOptional.get().getUser().getUsername();
            if (!Objects.equals(currentUserName, cartUserName)) {
                // currently authenticated user tries to access other user's cart - forbid doing that
                LOG.error("User '{}' attempted illegal access to cart '{}' owned by '{}' user", currentUserName, cartId, cartUserName);
                throw new IllegalArgumentException("Authenticated user does not own requested cart");
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected String getRegex() {
        return regex;
    }

    @Override
    protected String getCapturingGroupName() {
        return capturingGroupName;
    }

}
