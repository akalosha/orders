package name.akalosha.orders.rest.filter;

import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.service.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Resolves the site for current request URL and saves it as a request attribute.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class SiteMatchingFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(SiteMatchingFilter.class);

    private SiteService siteService;

    @Autowired
    public SiteMatchingFilter(SiteService siteService) {
        this.siteService = siteService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException
    {
        request.removeAttribute(SiteService.CURRENT_SITE_ATTR);
        Site currentSite = siteService.resolveCurrentSite(request);
        LOG.debug("Current site is resolved as " + currentSite.getName());
        request.setAttribute(SiteService.CURRENT_SITE_ATTR, currentSite);

        filterChain.doFilter(request, response);
    }

}
