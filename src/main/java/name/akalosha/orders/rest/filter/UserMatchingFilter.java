package name.akalosha.orders.rest.filter;

import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Ensures that authenticated user can access only their own data. Forbids access to data of any other user.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class UserMatchingFilter extends AbstractMatchingFilter {

    private static final Logger LOG = LoggerFactory.getLogger(UserMatchingFilter.class);

    @Value("${filter.user.uripattern}")
    protected String regex;
    @Value("${filter.user.groupname}")
    protected String capturingGroupName;

    private UserService userService;

    @Autowired
    public UserMatchingFilter(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException
    {
        Optional<Long> userIdOptional = getMatchedLongValue(request);
        if (userIdOptional.isPresent()) {
            Long userId = userIdOptional.get();
            LOG.debug("Running an UserMatchingFilter for userId {}", userId);
            Optional<User> userOptional = userService.find(userId);
            if (userOptional.isEmpty()) {
                // if we have invalid userId in URI, fail fast
                throw new NotFoundException(User.class, userId);
            }
            String currentUserName = userService.getCurrentUser().getUsername();
            String requestedUserName = userOptional.get().getUsername();
            if (!Objects.equals(currentUserName, requestedUserName)) {
                // currently authenticated user tries to access other user's data - forbid doing that
                LOG.error("User '{}' attempted illegal access to '{}' user", currentUserName, requestedUserName);
                throw new IllegalArgumentException("Requested user does not match authenticated user");
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected String getRegex() {
        return regex;
    }

    @Override
    protected String getCapturingGroupName() {
        return capturingGroupName;
    }

}
