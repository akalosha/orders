package name.akalosha.orders.rest.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import name.akalosha.orders.rest.handler.RestExceptionHandler;
import name.akalosha.orders.rest.util.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.NestedServletException;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Handler for exceptions thrown from filters. It is responsible for setting proper response
 * based on an exception that was caught.
 *
 * @implNote this was a @ControllerAdvice before. But then a request filters were added and they would
 * throw exceptions too. @ControllerAdvice exception handler would not catch exceptions thrown from filters
 * and such exceptions would give different response than the same exception thrown from controller.
 * So this handler was introduced to be able to catch exceptions that
 * got past {@link RestExceptionHandler}.
 *
 * @see RestExceptionHandler
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ExceptionHandlingFilter extends OncePerRequestFilter {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlingFilter.class);

    private ObjectMapper objectMapper;

    @Autowired
    public ExceptionHandlingFilter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (Exception ex) {
            // unroll nested exception
            if (ex instanceof NestedServletException && ex.getCause() instanceof Exception) {
                ex = (Exception) ex.getCause();
            }
            LOG.error("Exception caught by handler: ", ex);

            ResponseEntity<Object> responseEntity = ResponseBuilder.errorResponse(request, ex);
            writeResponse(response, responseEntity);
        }
    }

    /**
     * Sets response properties necessary to return error message.
     *
     * @param response response object
     * @param errorEntity DTO containing error details
     * @throws IOException if writing error message to JSON or writing JSON to response body fails
     */
    private void writeResponse(HttpServletResponse response, ResponseEntity<Object> errorEntity) throws IOException {
        Assert.notNull(response, "Response should not be null");
        Assert.notNull(errorEntity, "Error entity should not be null");

        Object body = errorEntity.getBody();
        String bodyJson = objectMapper.writeValueAsString(body);
        response.setStatus(errorEntity.getStatusCodeValue());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(bodyJson);
    }

}
