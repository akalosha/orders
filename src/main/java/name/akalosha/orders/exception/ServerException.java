package name.akalosha.orders.exception;

/**
 * Indicates that some internal error occurred.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class ServerException extends RuntimeException {

    public ServerException() {
    }

    public ServerException(String message) {
        super(message);
    }

    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }

}
