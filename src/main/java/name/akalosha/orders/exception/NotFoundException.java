package name.akalosha.orders.exception;

/**
 * Indicates that entity was not found.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public class  NotFoundException extends RuntimeException {

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Class<?> type, Object id) {
        super(String.format("%s with ID=%s was not found.", type == null ? "" : type.getSimpleName(), id));
    }

}
