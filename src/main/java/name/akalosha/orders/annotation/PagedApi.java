package name.akalosha.orders.annotation;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that helps to document methods that support pagination of response.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Page number [0..N]", example = "0", defaultValue = "0"),
        @ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Page size [1..N]", example = "0", defaultValue = "5"),
        @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query", value = "Sort parameters", example = "id,asc", defaultValue = "id,desc")
})
public @interface PagedApi {
}
