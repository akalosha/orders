package name.akalosha.orders.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Utility for working wih {@link Timestamp} type.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public final class DateTimeUtil {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

    private DateTimeUtil() {
    }

    /**
     * Converts {@link ZonedDateTime} to {@link Timestamp}.
     *
     * @param zonedDateTime {@link ZonedDateTime} to convert
     * @return converted {@link Timestamp}
     */
    public static Timestamp toTimestamp(ZonedDateTime zonedDateTime) {
        if (zonedDateTime == null) {
            return null;
        }
        long epochMilli = zonedDateTime.toInstant().toEpochMilli();
        return new Timestamp(epochMilli);
    }

    /**
     * Converts {@link LocalDate} to {@link Timestamp}. Resulting timestamp is representing
     * the moment <b>at the start of  a day</b>.
     *
     * @param localDate {@link LocalDate} to convert
     * @return converted {@link Timestamp}
     */
    public static Timestamp toTimestamp(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        long epochMilli = localDate.atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli();
        return new Timestamp(epochMilli);
    }

    /**
     * Converts {@link LocalDateTime} to {@link Timestamp}.
     *
     * @param localDateTime {@link LocalDateTime} to convert
     * @return converted {@link Timestamp}
     */
    public static Timestamp toTimestamp(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        long epochMilli = localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
        return new Timestamp(epochMilli);
    }

    /**
     * Converts {@link Timestamp} to {@link LocalDate}.
     *
     * @param timestamp {@link Timestamp} to convert
     * @return converted {@link LocalDate}
     */
    public static LocalDate toLocalDate(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.toInstant().atOffset(ZoneOffset.UTC).toLocalDate();
    }

}
