package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.report.RevenueReport;
import name.akalosha.orders.repository.jpa.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Implementation of {@link CRUDService} for {@link Order} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface OrderService extends CRUDService<Order, Long> {

    /**
     * Gets the order by id, additionally fetching order entries.
     *
     * @param id id of an order to get
     * @return {@link Order} instance
     * @see OrderRepository#findByIdWithEagerFetch(Long)
     */
    Optional<Order> findWithEagerFetch(Long id);

    /**
     * Creates an order from a cart with given ID.
     *
     * @param cartId ID of cart to create an order from
     * @return created order
     */
    Order placeOrder(Long cartId);

    /**
     * Returns a daily revenue report. Format is following:
     * <pre>
     *     day N: total revenue for day N
     *     day N + 1: total revenue for day N + 1
     *     ...
     * </pre>
     *
     * @param sinceDate a starting date for the report
     * @return report
     */
    RevenueReport generateRevenueReport(LocalDate sinceDate);

    /**
     * Finds orders that contain entries that contain a product with given name.
     *
     * @param pageable pagination parameters
     * @param productName name (or part of it) of a product to search for
     * @return orders that were found
     */
    Page<Order> findOrdersByProductName(Pageable pageable, String productName);

    /**
     * Gets order history for user. Similar to {@link #findAll(Pageable)}, but gets orders for given user
     * instead of all orders ever placed.
     *
     * @param pageable pagination/sort parameters
     * @param user user that orders should belong to
     * @return orders that were found
     */
    Page<Order> getOrderHistory(Pageable pageable, User user);
}
