package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.Category;

/**
 * Implementation of {@link CRUDService} for {@link Category} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CategoryService extends CRUDService<Category, Long> {
}
