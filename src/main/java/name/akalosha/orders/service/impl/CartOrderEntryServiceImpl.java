package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.CartDocMapper;
import name.akalosha.orders.converters.mappers.search.CartOrderEntryDocMapper;
import name.akalosha.orders.converters.mappers.util.CollectionMapper;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.CartOrderEntry;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.search.CartDoc;
import name.akalosha.orders.model.dto.search.CartOrderEntryDoc;
import name.akalosha.orders.repository.elastic.CartOrderEntrySearchRepository;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.service.CartOrderEntryService;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.ProductService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Implementation of {@link CartOrderEntryService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class CartOrderEntryServiceImpl implements CartOrderEntryService {

    private static final Logger LOG = LoggerFactory.getLogger(CartOrderEntryServiceImpl.class);
    
    private final CartOrderEntryRepository cartOrderEntryRepository;
    private final CartService cartService;
    private final ProductService productService;
    private final CartSearchRepository cartSearchRepository;
    private final CartOrderEntrySearchRepository cartOrderEntrySearchRepository;
    private final CartDocMapper cartDocMapper;
    private final CartOrderEntryDocMapper cartOrderEntryDocMapper;

    @Value("${cart.maxEntries:10}")
    private int maxCartEntries;

    @Autowired
    public CartOrderEntryServiceImpl(
            CartOrderEntryRepository cartOrderEntryRepository,
            CartService cartService,
            ProductService productService,
            CartOrderEntrySearchRepository cartOrderEntrySearchRepository,
            CartSearchRepository cartSearchRepository,
            CartOrderEntryDocMapper cartOrderEntryDocMapper,
            CartDocMapper cartDocMapper) {
        this.cartOrderEntryRepository = cartOrderEntryRepository;
        this.cartService = cartService;
        this.productService = productService;
        this.cartSearchRepository = cartSearchRepository;
        this.cartOrderEntrySearchRepository = cartOrderEntrySearchRepository;
        this.cartDocMapper = cartDocMapper;
        this.cartOrderEntryDocMapper = cartOrderEntryDocMapper;
    }

    /** This constructor is needed only for unit tests. In that case CartOrderEntryServiceImpl is not instantiated by DI
     * container, so we need a way to init property 'maxCartEntries' without relying on Spring. */
    public CartOrderEntryServiceImpl(
            CartOrderEntryRepository cartOrderEntryRepository,
            CartService cartService,
            ProductService productService,
            CartOrderEntrySearchRepository cartOrderEntrySearchRepository,
            CartSearchRepository cartSearchRepository,
            CartOrderEntryDocMapper cartOrderEntryDocMapper,
            CartDocMapper cartDocMapper,
            @Value("${cart.maxEntries:10}") int maxCartEntries) {
        this.cartOrderEntryRepository = cartOrderEntryRepository;
        this.cartService = cartService;
        this.productService = productService;
        this.cartSearchRepository = cartSearchRepository;
        this.cartOrderEntrySearchRepository = cartOrderEntrySearchRepository;
        this.cartDocMapper = cartDocMapper;
        this.cartOrderEntryDocMapper = cartOrderEntryDocMapper;
        this.maxCartEntries = maxCartEntries;
    }

    @Override
    // we are returning a non-paginated response here because a cart can't have so many entries that we would need a
    // pagination (we are limiting the number of entries that can be added to cart)
    public Collection<CartOrderEntry> findAll(Long cartId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        LOG.trace("Getting all entries for a cart with ID={}", cartId);
        Iterable<CartOrderEntryDoc> cartEntryDocIterable = cartOrderEntrySearchRepository.findAllByCartId(cartId);
        List<CartOrderEntryDoc> cartEntryDocs = IterableUtils.toList(cartEntryDocIterable);
        LOG.debug("Found {} entries for a cart with ID={}", cartEntryDocs.size(), cartId);
        return CollectionMapper.map(cartEntryDocs, cartOrderEntryDocMapper);
    }

    @Override
    @Transactional
    public Optional<CartOrderEntry> find(Long cartId, Long entryId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Assert.notNull(entryId, "Cart entry ID should not be null");
        LOG.trace("Getting an entry with ID={} for a cart with ID={}", entryId, cartId);

        Optional<CartOrderEntry> cartEntryOptional = cartOrderEntryRepository.findById(entryId);
        if (cartEntryOptional.isEmpty() || !isEntryLinkedToCart(cartEntryOptional.get(), cartId)) {
            return Optional.empty();
        }
        return cartEntryOptional;
    }

    private boolean isEntryLinkedToCart(CartOrderEntry entry, Long cartId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        boolean isLinked = entry != null && entry.getCart() != null && cartId.equals(entry.getCart().getId());
        if (entry != null) {
            LOG.debug("Entry with ID={} is {}linked to a cart with ID={}", entry.getId(), isLinked ? "" : "not ", cartId);
        }
        return isLinked;
    }

    @Override
    @Transactional
    public CartOrderEntry save(Long cartId, CartOrderEntry entry) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Assert.notNull(entry, "Cart entry should not be null");
        Assert.notNull(entry.getProduct(), "Cart entry should not have null product");
        Long productId = entry.getProduct().getId();
        Assert.notNull(productId, "Product ID should not be null");
        LOG.trace("Adding a new entry with product ID={} to a cart with ID={}", productId, cartId);

        Cart cart = getCart(cartId);
        checkIfEntryCanBeAdded(cart);
        entry.setCart(cart);

        Product product = getProduct(productId);
        entry.setProduct(product);

        Optional<CartOrderEntry> sameProductEntry = getSameProductEntry(cart, productId);
        CartOrderEntry savedCartEntry;
        if (sameProductEntry.isPresent()) {
            // There is already an entry with same productId in cart. We do not want to create multiple entries with
            // same product, instead we want to increase the quantity for existing entry.
            LOG.debug("Found same product entry (ID={}) on cart with ID={}, merging entries", sameProductEntry.get().getId(), cartId);
            savedCartEntry = mergeEntries(sameProductEntry.get(), entry);
        } else {
            savedCartEntry = cartOrderEntryRepository.save(entry);
            LOG.debug("Saved new entry with ID={} for cart with ID={}", savedCartEntry.getId(), cartId);
        }

        CartOrderEntryDoc cartEntryDoc = cartOrderEntryDocMapper.mapReverse(savedCartEntry);
        cartOrderEntrySearchRepository.save(cartEntryDoc);
        LOG.debug("Saved new entry with ID={} for cart with ID={}", savedCartEntry.getId(), cartId);

        boolean cartDoesNotContainSavedEntry = CollectionUtils.emptyIfNull(cart.getEntries()).stream()
                .noneMatch(item -> Objects.equals(item.getId(), savedCartEntry.getId()));
        // we want to see updated cart.entries after we save a new cart entry, but that is not possible, because the
        // transaction in which we are saving the new entry is not committed yet. And we can see only changes that are
        // committed, because the default transaction isolation level in PostgreSQL is READ_COMMITTED. That's why we
        // are adding new entry manually before indexing the cart.
        if (cartDoesNotContainSavedEntry) {
            cart.setEntries(new ArrayList<>(CollectionUtils.emptyIfNull(cart.getEntries())));
            cart.getEntries().add(savedCartEntry);
        }

        CartDoc cartDoc = cartDocMapper.mapReverse(cart);
        cartSearchRepository.save(cartDoc);
        LOG.debug("Indexed new entry with ID={} for cart with ID={}", savedCartEntry.getId(), cartId);

        return savedCartEntry;
    }

    private void checkIfEntryCanBeAdded(Cart cart) {
        if (CollectionUtils.emptyIfNull(cart.getEntries()).size() >= getMaxCartEntries()) {
            throw new IllegalStateException("Can't add a new cart entry: cart already has the maximum allowed number of entries ("
                    + getMaxCartEntries() + ")");
        }
    }

    /**
     * Searches cart for an entry containing given product.
     *
     * @param cart cart to check
     * @param productId ID of a product to search for
     * @return found entry, possibly empty
     */
    private Optional<CartOrderEntry> getSameProductEntry(Cart cart, Long productId) {
        Assert.notNull(cart, "Cart should not be null");
        Assert.notNull(productId, "Product ID should not be null");
        return CollectionUtils.emptyIfNull(cart.getEntries()).stream()
                .filter(item -> Objects.equals(item.getProduct().getId(), productId))
                .findAny();
    }

    /**
     * Adjusts quantity of existing cart entry in case if we are adding the same product to cart.
     *
     * @param existingEntry existing cart entry
     * @param newEntry new cart entry
     * @return existing entry with adjusted quantity
     */
    private CartOrderEntry mergeEntries(CartOrderEntry existingEntry, CartOrderEntry newEntry) {
        Assert.notNull(existingEntry, "Entry #1 should not be null");
        Assert.notNull(newEntry, "Entry #2 should not be null");

        int oldQuantity = existingEntry.getQuantity();
        existingEntry.setQuantity(oldQuantity + newEntry.getQuantity());
        CartOrderEntry savedEntry = cartOrderEntryRepository.save(existingEntry);
        LOG.debug("Updated existing entry (ID={}) quantity from {} to {}", existingEntry.getId(), oldQuantity, existingEntry.getQuantity());
        return savedEntry;
    }

    @Override
    @Transactional
    public CartOrderEntry editQuantity(Long cartId, Long entryId, int newQty) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Assert.notNull(entryId, "Entry ID should not be null");
        Assert.isTrue(newQty > 0, "New quantity should be greater than zero");
        LOG.trace("Editing quantity of a cart entry: cartID={}, entryId={}, newQuantity={}", cartId, entryId, newQty);

        CartOrderEntry existingCartEntry = getExistingEntry(entryId);
        if (!isEntryLinkedToCart(existingCartEntry, cartId)) {
            throw new NotFoundException(CartOrderEntry.class, entryId);
        }

        // here could be some logic checking if we can adjust quantity to new value, considering available stock.
        // But we do not have stock for products, so we just set new quantity unconditionally.
        existingCartEntry.setQuantity(newQty);

        CartOrderEntry editedCartEntry = cartOrderEntryRepository.save(existingCartEntry);
        LOG.debug("Persisted a cart entry with updated quantity: cartID={}, entryId={}, newQuantity={}", cartId, editedCartEntry.getId(), editedCartEntry.getQuantity());
        CartOrderEntryDoc cartEntryDoc = cartOrderEntryDocMapper.mapReverse(editedCartEntry);
        cartOrderEntrySearchRepository.save(cartEntryDoc);
        LOG.debug("Indexed a cart entry with updated quantity: cartID={}, entryId={}, newQuantity={}", cartId, editedCartEntry.getId(), editedCartEntry.getQuantity());

        CartDoc cartDoc = cartDocMapper.mapReverse(editedCartEntry.getCart());
        cartSearchRepository.save(cartDoc);
        LOG.debug("Indexed a cart with updated entry quantity: cartID={}, entryId={}, newQuantity={}", cartDoc.getId(), editedCartEntry.getId(), editedCartEntry.getQuantity());

        return editedCartEntry;
    }

    /**
     * Finds cart entry by ID.
     *
     * @param entryId ID of entry to find
     * @return entry found
     */
    private CartOrderEntry getExistingEntry(Long entryId) {
        Assert.notNull(entryId, "Item ID should not be null");
        Optional<CartOrderEntry> existingEntryOptional = cartOrderEntryRepository.findById(entryId);
        if (existingEntryOptional.isEmpty()) {
            throw new NotFoundException(CartOrderEntry.class, entryId);
        }
        LOG.debug("Found existing cart entry with ID={}", entryId);
        return existingEntryOptional.get();
    }

    /**
     * Finds product with given ID.
     *
     * @param productId ID of product to find
     * @return product found
     */
    private Product getProduct(Long productId) {
        Assert.notNull(productId, "Product ID should not be null");
        Optional<Product> productOptional = productService.find(productId);
        if (productOptional.isEmpty()) {
            throw new NotFoundException(Product.class, productId);
        }
        LOG.debug("Found existing product with ID={}", productId);
        return productOptional.get();
    }


    /**
     * Finds cart with given ID.
     *
     * @param cartId ID of cart to find
     * @return cart found
     */
    private Cart getCart(Long cartId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Optional<Cart> cartOptional = cartService.find(cartId);
        if (cartOptional.isEmpty()) {
            throw new NotFoundException(Cart.class, cartId);
        }
        LOG.debug("Found existing cart with ID={}", cartId);
        return cartOptional.get();
    }

    @Override
    @Transactional
    public void delete(Long cartId, Long entryId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Assert.notNull(entryId, "Entry ID should not be null");
        LOG.trace("Deleting an entry with ID={} from a cart with ID={}", entryId, cartId);

        CartOrderEntry entry = getExistingEntry(entryId);
        if (entry.getOrder() != null) {
            throw new IllegalArgumentException("This order item belongs to order and can not be deleted");
        }

        if (!isEntryLinkedToCart(entry, cartId)) {
            throw new NotFoundException(CartOrderEntry.class, entryId);
        }

        cartOrderEntryRepository.deleteById(entryId);
        LOG.debug("Deleted an entry with ID={} from DB", entryId);
        cartOrderEntrySearchRepository.deleteById(entryId);
        LOG.debug("Deleted an entry with ID={} from search repository", entryId);

        Cart cart = getCart(cartId);
        CartDoc cartDoc = cartDocMapper.mapReverse(cart);
        // cart still contains removed entry, so removing it manually
        CollectionUtils.emptyIfNull(cartDoc.getEntries())
                .removeIf(item -> entryId.equals(item.getId()));

        cartSearchRepository.save(cartDoc);
        LOG.debug("Updated a cart with ID={} in search index", cartDoc.getId());
    }

    public int getMaxCartEntries() {
        return maxCartEntries;
    }
}
