package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.ProductDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.ProductUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.domain.Product;
import name.akalosha.orders.model.dto.search.ProductDoc;
import name.akalosha.orders.repository.elastic.ProductSearchRepository;
import name.akalosha.orders.repository.jpa.ProductRepository;
import name.akalosha.orders.service.CategoryService;
import name.akalosha.orders.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Implementation of {@link ProductService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class ProductServiceImpl extends BaseCRUDService<Long, Product, ProductDoc> implements ProductService {

    private static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final CategoryService categoryService;
    private final ProductSearchRepository productSearchRepository;

    @Autowired
    public ProductServiceImpl(
            ProductRepository productRepository,
            ProductSearchRepository productSearchRepository,
            ProductDocMapper productDocMapper,
            ProductUpdater productUpdater,
            CategoryService categoryService) {
        super(productRepository, productSearchRepository, productDocMapper, productUpdater);
        this.categoryService = categoryService;
        this.productSearchRepository = productSearchRepository;
    }

    @Override
    public Product save(Product product) {
        Assert.notNull(product, "Product should not be null");
        Assert.notNull(product.getCategory(), "Product's category should not be null");
        LOG.trace("Creating a new product with sku={} and category ID={}", product.getSku(), product.getCategory().getId());

        // Getting category from DB. This is needed to:
        // 1) check that category provided in payload exists
        // 2) get a category object with all properties. Category from payload may contain only ID. When we would index
        // new product later, we want product's category to have a full set of properties and not only an ID.
        Long categoryId = product.getCategory().getId();
        setProductCategory(product, categoryId);

        return super.save(product);
    }

    /**
     * Sets a category for given product.
     *
     * @param product product to set category for
     * @param categoryId ID of a category
     */
    private void setProductCategory(Product product, Long categoryId) {
        Assert.notNull(product, "Product should not be null");
        Assert.notNull(categoryId, "Product's category ID should not be null");
        LOG.trace("Setting a category with ID={} to product with sku={}", categoryId, product.getSku());

        Optional<Category> categoryOptional = categoryService.find(categoryId);
        if (categoryOptional.isEmpty()) {
            throw new NotFoundException(Category.class, categoryId);
        }

        product.setCategory(categoryOptional.get());
    }

    /**
     * Updates product's properties.
     *
     * @param product product to update
     * @param updater updater for product
     * @param categoryHandler function for setting category to product
     * @return updated product
     */
    private Product edit(Product product, BiConsumer<Product, Product> updater, Consumer<Product> categoryHandler) {
        Assert.notNull(product, "Product should not be null");
        Assert.notNull(product.getId(), "Product ID should not be null");
        Assert.notNull(updater, "Updater function should not be null");
        Assert.notNull(categoryHandler, "Category handler function should not be null");
        LOG.trace("Updating a product with ID={}", product.getId());

        Optional<Product> existingProductOptional = find(product.getId());
        if (existingProductOptional.isEmpty()) {
            throw new NotFoundException();
        }

        Product existingProduct = existingProductOptional.get();
        updater.accept(product, existingProduct);

        categoryHandler.accept(existingProduct);

        Product editedProduct = getPersistenceRepository().save(existingProduct);
        LOG.debug("Updated a product with ID={} in DB", editedProduct.getId());
        ProductDoc productDoc = getMapper().mapReverse(editedProduct);
        getSearchRepository().save(productDoc);
        LOG.debug("Updated a product with ID={} in search repository", productDoc.getId());

        return editedProduct;
    }

    @Override
    public Product edit(Product entity) {
        LOG.trace("Editing a product with ID={}", entity.getId());
        return edit(entity, getUpdater()::update, getCategoryHandlerForEditAndPatch());
    }

    @Override
    public Product patch(Product entity) {
        LOG.trace("Patching a product with ID={}", entity.getId());
        return edit(entity, getUpdater()::updateNotNull, getCategoryHandlerForEditAndPatch());
    }

    /**
     * Returns a function that handles setting a category to product during product update process.
     *
     * @return handler function for setting product's category
     */
    private Consumer<Product> getCategoryHandlerForEditAndPatch() {
        return product -> {
            Assert.notNull(product.getCategory(), "Product's category is null");
            setProductCategory(product, product.getCategory().getId());
        };
    }

    @Override
    public Page<Product> findByName(Pageable pageable, String name) {
        Assert.notNull(name, "Product name should not be null");
        Assert.notNull(pageable, "Pagination object should not be null");
        LOG.trace("Finding a product by name '{}'", name);

        Page<ProductDoc> searchResult = productSearchRepository.findProductDocsByName(pageable, name);
        LOG.debug("Found {} products by name '{}'", searchResult.getTotalElements(), name);
        return mapPaginatedSearchResult(searchResult);
    }

}
