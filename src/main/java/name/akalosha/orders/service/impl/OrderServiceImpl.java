package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.OrderDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.OrderUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.Order;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.report.RevenueReport;
import name.akalosha.orders.model.dto.search.OrderDoc;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.elastic.OrderSearchRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.OrderRepository;
import name.akalosha.orders.service.OrderService;
import name.akalosha.orders.util.DateTimeUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation of {@link OrderService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class OrderServiceImpl extends BaseCRUDService<Long, Order, OrderDoc> implements OrderService {

    private static final Logger LOG = LoggerFactory.getLogger(OrderServiceImpl.class);

    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;
    private final OrderSearchRepository orderSearchRepository;
    private final CartSearchRepository cartSearchRepository;
    private final OrderDocMapper orderDocMapper;
    private final CartOrderEntryRepository cartOrderEntryRepository;

    @Autowired
    public OrderServiceImpl(
            OrderRepository orderRepository,
            CartRepository cartRepository,
            OrderSearchRepository orderSearchRepository,
            CartSearchRepository cartSearchRepository,
            OrderDocMapper orderDocMapper,
            CartOrderEntryRepository cartOrderEntryRepository,
            OrderUpdater orderUpdater) {
        super(orderRepository, orderSearchRepository, orderDocMapper, orderUpdater);
        this.orderRepository = orderRepository;
        this.cartRepository = cartRepository;
        this.orderSearchRepository = orderSearchRepository;
        this.cartSearchRepository = cartSearchRepository;
        this.orderDocMapper = orderDocMapper;
        this.cartOrderEntryRepository = cartOrderEntryRepository;
    }

    @Override
    public Optional<Order> findWithEagerFetch(Long id) {
        LOG.trace("Getting an order with ID={} including order entries", id);
        return orderRepository.findByIdWithEagerFetch(id);
    }

    @Override
    @Transactional
    public Order placeOrder(final Long cartId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        LOG.trace("Placing an order for a cart with ID={}", cartId);

        Optional<Cart> cartOptional = cartRepository.findById(cartId);
        if (cartOptional.isEmpty()) {
            throw new NotFoundException(Cart.class, cartId);
        }

        Cart cart = cartOptional.get();
        if (CollectionUtils.isEmpty(cart.getEntries())) {
            throw new IllegalArgumentException("Cart has no items.");
        }

        if (cart.getShippingAddress() == null) {
            throw new IllegalArgumentException("Cart has no shipping address set.");
        }

        if (cart.getDeliveryMethod() == null) {
            throw new IllegalArgumentException("Cart has no delivery method set.");
        }

        Order order = toOrder(cart);
        order.setDatePlaced(DateTimeUtil.toTimestamp(ZonedDateTime.now()));
        LOG.debug("Setting a datePlaced to new order as {}", order.getDatePlaced());
        Order savedOrder = orderRepository.save(order);
        LOG.debug("Persisted a new order with ID={}", savedOrder.getId());

        // We do not need this cart anymore, so it should be deleted. This also prevents creating multiple orders from a single cart.
        cartSearchRepository.deleteById(cartId);
        LOG.debug("Deleted a cart with ID={} from search repository", cartId);
        cartRepository.deleteById(cartId);
        LOG.debug("Deleted a cart with ID={} from DB", cartId);

        OrderDoc orderDoc = orderDocMapper.mapReverse(order);
        orderSearchRepository.save(orderDoc);
        LOG.debug("Saved a new order with ID={} to search repository", orderDoc.getId());

        return order;
    }

    /**
     * Creates an order object based on a given cart.
     *
     * @param cart cart to create an order from
     * @return created order object
     */
    private Order toOrder(Cart cart) {
        Assert.notNull(cart, "Cart should not be null");
        // TODO create a mapper
        Order order = new Order();
        order.setEntries(cart.getEntries());
        order.setUser(cart.getUser());
        order.setSite(cart.getSite());
        order.setShippingAddress(cart.getShippingAddress());
        order.setDeliveryMethod(cart.getDeliveryMethod());
        return order;
    }

    @Override
    public RevenueReport generateRevenueReport(LocalDate sinceDate) {
        Assert.notNull(sinceDate, "Date should not be null");
        LOG.trace("Generating a revenue report since date {}", sinceDate);

        // TODO: query Elasticsearch here instead of DB
        List<Order> orders = orderRepository.findByDatePlacedAfter(DateTimeUtil.toTimestamp(sinceDate));
        LOG.debug("Found {} orders placed after {}", orders.size(), sinceDate);
        Map<LocalDate, BigDecimal> reportData = CollectionUtils.emptyIfNull(orders).stream()
                .collect(
                        Collectors.groupingBy(
                                order -> DateTimeUtil.toLocalDate(order.getDatePlaced()),
                                Collectors.reducing(BigDecimal.ZERO, Order::getTotal, BigDecimal::add)
                        )
                );
        LOG.trace("Data for report {}", reportData);

        RevenueReport report = new RevenueReport();
        report.setRevenueByDay(reportData);
        return report;
    }

    @Override
    public Page<Order> findOrdersByProductName(Pageable pageable, String productName) {
        Assert.notNull(pageable, "Pageable should not be null");
        Assert.notNull(productName, "Product name should not be null");
        LOG.trace("Searching for orders that contain a product with name like '{}'", productName);

        Page<OrderDoc> orderDocPage = orderSearchRepository.findAllByProductNameContaining(pageable, productName);
        LOG.debug("Found {} orders that contain a product with name like '{}'", orderDocPage.getTotalElements(), productName);
        return mapPaginatedSearchResult(orderDocPage);
    }

    @Override
    public Page<Order> getOrderHistory(Pageable pageable, User user) {
        Assert.notNull(pageable, "Pageable should not be null");
        Assert.notNull(user, "User should not be null");
        LOG.trace("Getting order history for user {}", user.getUsername());

        Page<OrderDoc> orderDocPage = orderSearchRepository.findAllByUser(pageable, user.getUsername());
        LOG.debug("Found {} order history entries for user {}", orderDocPage.getTotalElements(), user.getUsername());
        return mapPaginatedSearchResult(orderDocPage);
    }

    @Override
    public Order save(Order entity) {
        throw new UnsupportedOperationException("Orders are created via placeOrder()");
    }

    @Override
    public Order edit(Order entity) {
        throw new UnsupportedOperationException("Once placed, order can not be changed.");
    }

    @Override
    public Order patch(Order entity) {
        throw new UnsupportedOperationException("Once placed, order can not be changed.");
    }
}
