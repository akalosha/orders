package name.akalosha.orders.service.impl;

import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Optional;

/**
 * Implementation of {@link UserDetailsService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    private UserService userService;

    public UserDetailServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Assert.isTrue(StringUtils.isNotBlank(username), "Username should not be empty");
        LOG.trace("Loading user bby name '{}'", username);

        Optional<User> userOptional = userService.findByUsername(username);
        return userOptional.map(this::mapToSpringUser)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found: " + username));
    }

    /**
     * Maps {@link User} to {@link UserDetails}.
     *
     * @param user {@link User} object to map
     * @return mapped {@link UserDetails} object
     */
    private UserDetails mapToSpringUser(User user) {
        Assert.notNull(user, "User should not be null");
        LOG.trace("Mapping user '{}' to UserDetails", user.getUsername());

        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(user.getRoles().toArray(new String[] {}))
                .build();
    }
}
