package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.BidirectionalMapper;
import name.akalosha.orders.converters.mappers.util.CollectionMapper;
import name.akalosha.orders.converters.updaters.CombinedUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Identifiable;
import name.akalosha.orders.service.CRUDService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.function.BiConsumer;

/**
 * Abstract service implementing generic CRUD operations.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public abstract class BaseCRUDService<ID, PERSISTENCE_TYPE extends Identifiable<ID>, INDEXED_TYPE> implements CRUDService<PERSISTENCE_TYPE, ID> {

    private static final Logger LOG = LoggerFactory.getLogger(BaseCRUDService.class);

    private final PagingAndSortingRepository<PERSISTENCE_TYPE, ID> persistenceRepository;
    private final PagingAndSortingRepository<INDEXED_TYPE, ID> searchRepository;
    private final BidirectionalMapper<INDEXED_TYPE, PERSISTENCE_TYPE> mapper;
    private final CombinedUpdater<PERSISTENCE_TYPE> updater;

    /**
     * Creates a new instance of this service.
     *
     * @param persistenceRepository repository working with persistence type
     * @param searchRepository repository working with indexed type
     * @param mapper bidirectional mapper between persistence and indexed types
     * @param updater updated for persistence type
     */
    public BaseCRUDService(
            PagingAndSortingRepository<PERSISTENCE_TYPE, ID> persistenceRepository,
            PagingAndSortingRepository<INDEXED_TYPE, ID> searchRepository,
            BidirectionalMapper<INDEXED_TYPE, PERSISTENCE_TYPE> mapper,
            CombinedUpdater<PERSISTENCE_TYPE> updater) {
        Assert.notNull(persistenceRepository, "Persistence repository should not be null");
        Assert.notNull(searchRepository, "Search repository should not be null");
        Assert.notNull(mapper, "Mapper should not be null");

        this.persistenceRepository = persistenceRepository;
        this.searchRepository = searchRepository;
        this.mapper = mapper;
        this.updater = updater;
    }

    @Override
    public Page<PERSISTENCE_TYPE> findAll(Pageable pageable) {
        Assert.notNull(pageable, "Pageable should not be null");
        LOG.trace("Getting all entities");
        Page<INDEXED_TYPE> searchResultPage = getSearchRepository().findAll(pageable);
        if (!searchResultPage.isEmpty()) {
            LOG.debug("Number of {} entities found: {}", searchResultPage.stream().findFirst().get().getClass().getSimpleName(), searchResultPage.getTotalElements());
        }  else {
            LOG.debug("No entities were found");
        }
        Collection<PERSISTENCE_TYPE> mappedCollection = CollectionMapper.map(searchResultPage.getContent(), getMapper());
        return new PageImpl<>(new ArrayList<>(mappedCollection), searchResultPage.getPageable(), searchResultPage.getTotalElements());
    }

    @Override
    @Transactional
    public Optional<PERSISTENCE_TYPE> find(ID id) {
        Assert.notNull(id, "ID should not be null");
        LOG.trace("Getting entity with ID={}", id);
        return getPersistenceRepository().findById(id);
    }

    @Override
    @Transactional
    public PERSISTENCE_TYPE save(PERSISTENCE_TYPE entity) {
        Assert.notNull(entity, "Entity should not be null");
        LOG.trace("Creating new entity of type {}", entity.getClass().getSimpleName());
        PERSISTENCE_TYPE savedPersistenceEntity = getPersistenceRepository().save(entity);
        LOG.debug("Persisted new entity with type {} and ID={}", savedPersistenceEntity.getClass().getSimpleName(), savedPersistenceEntity.getId());
        INDEXED_TYPE indexedEntity = getMapper().mapReverse(savedPersistenceEntity);
        getSearchRepository().save(indexedEntity);
        LOG.debug("Indexed new entity with type {} and ID={}", indexedEntity.getClass().getSimpleName(), savedPersistenceEntity.getId());
        return savedPersistenceEntity;
    }

    /**
     * Updates properties of persistence entity.
     *
     * @param entity entity to update
     * @param updater updater object
     * @return updated entity
     */
    private PERSISTENCE_TYPE edit(PERSISTENCE_TYPE entity, BiConsumer<PERSISTENCE_TYPE, PERSISTENCE_TYPE> updater) {
        Assert.notNull(entity, "Entity should not be null");
        Assert.notNull(updater, "Updater function should not be null");

        Optional<PERSISTENCE_TYPE> existingPersistenceEntityOptional = find(entity.getId());
        if (existingPersistenceEntityOptional.isEmpty()) {
            throw new NotFoundException();
        }

        PERSISTENCE_TYPE existingPersistenceEntity = existingPersistenceEntityOptional.get();
        updater.accept(entity, existingPersistenceEntity);

        PERSISTENCE_TYPE editedPersistenceEntity = getPersistenceRepository().save(existingPersistenceEntity);
        LOG.debug("Persisted an updated entity with type {} and ID={}", editedPersistenceEntity.getClass().getSimpleName(), editedPersistenceEntity.getId());
        INDEXED_TYPE indexedEntity = getMapper().mapReverse(editedPersistenceEntity);
        getSearchRepository().save(indexedEntity);
        LOG.debug("Indexed an updated entity with type {} and ID={}", indexedEntity.getClass().getSimpleName(), editedPersistenceEntity.getId());

        return editedPersistenceEntity;
    }

    @Override
    @Transactional
    public PERSISTENCE_TYPE edit(PERSISTENCE_TYPE entity) {
        LOG.trace("Updating an entity of type {}", entity.getClass().getSimpleName());
        return edit(entity, getUpdater()::update);
    }

    @Override
    public PERSISTENCE_TYPE patch(PERSISTENCE_TYPE entity) {
        LOG.trace("Patching an entity of type {}", entity.getClass().getSimpleName());
        return edit(entity, getUpdater()::updateNotNull);
    }

    @Override
    @Transactional
    public void delete(ID id) {
        Assert.notNull(id, "ID should not be null");
        LOG.trace("Deleting an entity with ID={}", id);
        Optional<PERSISTENCE_TYPE> entityOptional = getPersistenceRepository().findById(id);
        if (entityOptional.isEmpty()) {
            throw new NotFoundException();
        }
        getPersistenceRepository().deleteById(id);
        LOG.debug("Deleted an entity with type {} and ID={} from DB", entityOptional.get().getClass().getSimpleName(), id);
        getSearchRepository().deleteById(id);
        LOG.debug("Deleted an entity with type {} and ID={} from search repository", entityOptional.get().getClass().getSimpleName(), id);
    }

    protected Page<PERSISTENCE_TYPE> mapPaginatedSearchResult(Page<INDEXED_TYPE> indexedEntityPage) {
        Assert.notNull(indexedEntityPage, "Page to convert should not be null");

        Collection<PERSISTENCE_TYPE> mappedContent = CollectionMapper.map(CollectionUtils.emptyIfNull(indexedEntityPage.getContent()), getMapper());
        return new PageImpl<>(new ArrayList<>(mappedContent), indexedEntityPage.getPageable(), indexedEntityPage.getTotalElements());
    }

    protected PagingAndSortingRepository<PERSISTENCE_TYPE, ID> getPersistenceRepository() {
        return persistenceRepository;
    }

    protected PagingAndSortingRepository<INDEXED_TYPE, ID> getSearchRepository() {
        return searchRepository;
    }

    protected BidirectionalMapper<INDEXED_TYPE, PERSISTENCE_TYPE> getMapper() {
        return mapper;
    }

    protected CombinedUpdater<PERSISTENCE_TYPE> getUpdater() {
        return updater;
    }
}
