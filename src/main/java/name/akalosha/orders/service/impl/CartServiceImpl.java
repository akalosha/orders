package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.form.AddressFormMapper;
import name.akalosha.orders.converters.mappers.search.AddressDocMapper;
import name.akalosha.orders.converters.mappers.search.CartDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.CartUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.Address;
import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.model.dto.search.AddressDoc;
import name.akalosha.orders.model.dto.search.CartDoc;
import name.akalosha.orders.repository.elastic.AddressSearchRepository;
import name.akalosha.orders.repository.elastic.CartSearchRepository;
import name.akalosha.orders.repository.jpa.AddressRepository;
import name.akalosha.orders.repository.jpa.CartOrderEntryRepository;
import name.akalosha.orders.repository.jpa.CartRepository;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.service.CartService;
import name.akalosha.orders.service.UserService;
import name.akalosha.orders.service.delivery.DeliveryMethodChangeHandler;
import name.akalosha.orders.service.delivery.DeliveryMethodHandlerResolver;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Objects;
import java.util.Optional;

/**
 * Implementation of {@link CartService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class CartServiceImpl extends BaseCRUDService<Long, Cart, CartDoc> implements CartService {

    private static final Logger LOG = LoggerFactory.getLogger(CartServiceImpl.class);
    
    private final UserService userService;
    private final CartRepository cartRepository;
    private final DeliveryMethodRepository deliveryMethodRepository;
    private final CartOrderEntryRepository cartOrderEntryRepository;
    private final AddressRepository addressRepository;
    private final AddressFormMapper addressFormMapper;
    private final AddressSearchRepository addressSearchRepository;
    private final AddressDocMapper addressDocMapper;
    private final DeliveryMethodHandlerResolver deliveryMethodHandlerResolver;

    @Autowired
    public CartServiceImpl(
            UserService userService,
            CartRepository cartRepository,
            DeliveryMethodRepository deliveryMethodRepository,
            CartSearchRepository categorySearchRepository,
            CartOrderEntryRepository cartOrderEntryRepository,
            AddressRepository addressRepository,
            CartDocMapper cartDocMapper,
            CartUpdater cartUpdater,
            AddressFormMapper addressFormMapper,
            AddressSearchRepository addressSearchRepository,
            AddressDocMapper addressDocMapper,
            DeliveryMethodHandlerResolver deliveryMethodHandlerResolver) {
        super(cartRepository, categorySearchRepository, cartDocMapper, cartUpdater);
        this.userService = userService;
        this.cartRepository = cartRepository;
        this.deliveryMethodRepository = deliveryMethodRepository;
        this.cartOrderEntryRepository = cartOrderEntryRepository;
        this.addressRepository = addressRepository;
        this.addressFormMapper = addressFormMapper;
        this.addressSearchRepository = addressSearchRepository;
        this.addressDocMapper = addressDocMapper;
        this.deliveryMethodHandlerResolver = deliveryMethodHandlerResolver;
    }

    @Override
    public Optional<Cart> findWithEagerFetch(Long id) {
        LOG.trace("Getting a cart with ID={} including cart entries", id);
        return cartRepository.findByIdWithEagerFetch(id);
    }

    @Override
    public Cart save(Cart cart) {
        Assert.notNull(cart, "Cart should not be null");
        Assert.notNull(cart.getUser(), "Cart's user should not be null");
        LOG.trace("Creating a new cart");

        // Getting user from DB. This is needed to:
        // 1) check that user provided in payload exists
        // 2) get a user ID from DB. User from payload would contain only username.
        String username = cart.getUser().getUsername();
        setCartUser(cart, username);
        LOG.debug("Creating a new cart for user {}", username);

        return super.save(cart);
    }

    /**
     * Sets a user with given name as a cart owner.
     *
     * @param cart cart to set user to
     * @param username name of user to set to cart
     */
    private void setCartUser(Cart cart, String username) {
        Assert.notNull(cart, "Cart should not be null");
        Assert.isTrue(StringUtils.isNotBlank(username), "Username should not be empty");
        LOG.trace("Setting user {} to a new cart", username);

        Optional<User> userOptional = userService.findByUsername(username);
        if (userOptional.isEmpty()) {
            throw new NotFoundException(User.class, username);
        }

        cart.setUser(userOptional.get());
    }

    @Override
    public void delete(Long cartId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        LOG.trace("Deleting a cat with ID={}", cartId);
        
        Optional<Cart> cartOptional = getPersistenceRepository().findById(cartId);
        if (cartOptional.isEmpty()) {
            throw new NotFoundException(Cart.class, cartId);
        }
        // instead of having this method and doing cascade deletion of entries manually, we could rely on
        // BaseCRUDService.delete() and enable cascade deletion for Cart.entries field by utilizing @OneToMany.cascade
        // annotation. But that would break the order placement, because there we need to delete a cart, but keep its
        // entries (we are reassigning all cart entries to order). So we can not have cascade delete for cart's entries,
        // because during order placement that would delete all order entries.
        cartOrderEntryRepository.deleteAll(cartOptional.get().getEntries());
        LOG.debug("Deleted all entries from a cart with ID={}", cartId);
        getPersistenceRepository().deleteById(cartId);
        LOG.debug("Deleted a cart with ID={} from DB", cartId);
        getSearchRepository().deleteById(cartId);
        LOG.debug("Deleted a cart with ID={} from search repository", cartId);
        // TODO: also delete shipping address from Elastic
    }

    @Override
    public Cart edit(Cart entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Cart patch(Cart entity) {
        throw new UnsupportedOperationException();
    }

    @Transactional
    @Override
    public Cart setShippingAddress(Long cartId, AddressForm shippingAddressForm) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Assert.notNull(shippingAddressForm, "Shipping address form should not be null");
        LOG.trace("Setting a shipping address to a cart with ID={}", cartId);

        Optional<Cart> cartOptional = cartRepository.findByIdWithEagerFetch(cartId);
        if (cartOptional.isEmpty()) {
            throw new NotFoundException(Cart.class, cartId);
        }

        Cart cart = cartOptional.get();
        Address shippingAddress = addressFormMapper.map(shippingAddressForm);
        if (cart.getShippingAddress() != null) {
            replaceShippingAddress(cart, shippingAddress);
            LOG.debug("Replaced an existing shipping address on cart with ID={}", cartId);
        } else {
            Address savedShippingAddress = saveAddress(shippingAddress);
            cart.setShippingAddress(savedShippingAddress);
            LOG.debug("Set a new shipping address (ID={}) on cart with ID={}", savedShippingAddress.getId(), cartId);
        }

        return super.save(cart);
    }

    // replaces existing shipping address for a cart with a new one
    private void replaceShippingAddress(Cart cart, Address newShippingAddress) {
        Assert.notNull(cart, "Cart should not be null");
        Assert.notNull(newShippingAddress, "Shipping address should not be null");
        LOG.trace("Replacing an existing shipping address on cart with ID={}", cart.getId());

        Address savedAddress = saveAddress(newShippingAddress);
        Address existingAddress = cart.getShippingAddress();
        cart.setShippingAddress(savedAddress);

        addressSearchRepository.deleteById(existingAddress.getId());
        LOG.debug("Deleted address with ID={} from search repository", existingAddress.getId());
        addressRepository.deleteById(existingAddress.getId());
        LOG.debug("Deleted address with ID={} from DB", existingAddress.getId());
    }

    // in the future, may be extracted into a dedicated service if we would add more address-related operations
    private Address saveAddress(Address shippingAddress) {
        Assert.notNull(shippingAddress, "Shipping address should not be null");
        LOG.trace("Saving a new shipping address object");

        Address savedAddress = addressRepository.save(shippingAddress);
        LOG.debug("Saved a new address with ID={} to DB", savedAddress.getId());
        AddressDoc addressDoc = addressDocMapper.mapReverse(savedAddress);
        addressSearchRepository.save(addressDoc);
        LOG.debug("Saved a new address with ID={} to search repository", addressDoc.getId());
        return savedAddress;
    }

    @Override
    public Cart setDeliveryMethod(Long cartId, Long deliveryMethodId) {
        Assert.notNull(cartId, "Cart ID should not be null");
        Assert.notNull(deliveryMethodId, "Delivery method ID should not be null");
        LOG.trace("Setting a delivery method with ID={} to a cart with ID={}", deliveryMethodId, cartId);

        Optional<Cart> cartOptional = cartRepository.findByIdWithEagerFetch(cartId);
        if (cartOptional.isEmpty()) {
            throw new NotFoundException(Cart.class, cartId);
        }

        Optional<DeliveryMethod> deliveryMethodOptional = deliveryMethodRepository.findById(deliveryMethodId);
        if (deliveryMethodOptional.isEmpty()) {
            throw new NotFoundException(DeliveryMethod.class, deliveryMethodId);
        }

        Cart cart = cartOptional.get();
        DeliveryMethod deliveryMethod = deliveryMethodOptional.get();
        if (!hasSameSite(cart, deliveryMethod)) {
            String message = String.format("Can't set delivery method %s to cart %s because they belong to different sites",
                    deliveryMethodId, cartId);
            throw new IllegalArgumentException(message);
        }

        DeliveryMethodChangeHandler deliveryMethodChangeHandler = deliveryMethodHandlerResolver.resolveHandler(deliveryMethod);
        deliveryMethodChangeHandler.beforeDeliveryMethodChange(cart, deliveryMethod);
        cart.setDeliveryMethod(deliveryMethod);
        deliveryMethodChangeHandler.afterDeliveryMethodChange(cart);
        LOG.debug("Set a delivery method with ID={} to a cart with ID={}", deliveryMethodId, cartId);

        return super.save(cart);
    }

    private static boolean hasSameSite(Cart cart, DeliveryMethod deliveryMethod) {
        Assert.notNull(cart, "Cart should not be null");
        Assert.notNull(deliveryMethod, "Delivery method should not be null");
        Assert.notNull(cart.getId(), "Cart has no id");
        LOG.trace("Checking if a delivery method with ID={} has a same site as a cart with ID={}", deliveryMethod.getId(), cart.getId());
        
        if (deliveryMethod.getId() == null) {
            throw new IllegalArgumentException("Delivery method" + deliveryMethod.getName() + " has no id");
        }

        return Objects.equals(cart.getSite().getId(), deliveryMethod.getSite().getId());
    }

}
