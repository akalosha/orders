package name.akalosha.orders.service.impl;

import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.repository.jpa.SiteRepository;
import name.akalosha.orders.service.SiteService;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class SiteServiceImpl implements SiteService {

    private static final Logger LOG = LoggerFactory.getLogger(SiteServiceImpl.class);

    private final SiteRepository siteRepository;

    @Autowired
    public SiteServiceImpl(SiteRepository siteRepository) {
        this.siteRepository = siteRepository;
    }

    @Override
    public Site resolveCurrentSite(HttpServletRequest request) {
        Assert.notNull(request, "Request should not be null");
        LOG.trace("Resolving current site");

        Object currentSite = request.getAttribute(CURRENT_SITE_ATTR);
        if (currentSite == null) {
            LOG.debug("Resolving current site from request URL");
        } else if (currentSite instanceof Site) {
            LOG.debug("Getting current site from request attributes");
            return (Site) currentSite;
        } else {
            LOG.error("Request attribute contains an invalid site object " + currentSite);
            // proceed with site resolution logic
        }

        final String requestUrl = request.getRequestURL() + "?" + request.getQueryString();
        LOG.debug("Request URL that will be used to resolve a site: {}", requestUrl);
        List<Site> sites = siteRepository.findAll();
        Map<String, List<Matcher>> urlMapping = sites.stream()
                .collect(
                        Collectors.toMap(
                                Site::getName,
                                site -> toMatchers(site.getUrlPatterns(), requestUrl),
                                ListUtils::union
                        )
                );
        LOG.trace("URL mappings for resolving a site: {}", urlMapping);

        for (Site site : sites) {
            if (urlMapping.get(site.getName()).stream().anyMatch(Matcher::matches)) {
                LOG.debug("Site is resolved as '{}'", site.getName());
                return site;
            }
        }

        throw new IllegalStateException("No site is associated with request URL " + requestUrl);
    }

    private List<Matcher> toMatchers(List<String> patterns, String input) {
        return patterns.stream()
                .map(p -> Pattern.compile(p).matcher(input))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Site> findSiteByName(String siteName) {
        Assert.isTrue(StringUtils.isNotBlank(siteName), "Site name should not be null");
        LOG.trace("Getting a site by name '{}'", siteName);

        return siteRepository.findSiteByName(siteName);
    }

}
