package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.CategoryDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.CategoryUpdater;
import name.akalosha.orders.model.domain.Category;
import name.akalosha.orders.model.dto.search.CategoryDoc;
import name.akalosha.orders.repository.elastic.CategorySearchRepository;
import name.akalosha.orders.repository.jpa.CategoryRepository;
import name.akalosha.orders.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link CategoryService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class CategoryServiceImpl extends BaseCRUDService<Long, Category, CategoryDoc> implements CategoryService {

    @Autowired
    public CategoryServiceImpl(
            CategoryRepository categoryRepository,
            CategorySearchRepository categorySearchRepository,
            CategoryDocMapper categoryDocMapper,
            CategoryUpdater categoryUpdater) {
        super(categoryRepository, categorySearchRepository, categoryDocMapper, categoryUpdater);
    }

    @Override
    public Category patch(Category entity) {
        // only category name can be updated, and that can be done via edit(), so at the moment there is no need to have patch()
        throw new UnsupportedOperationException("Category can only be edited. Patching is not supported.");
    }
}
