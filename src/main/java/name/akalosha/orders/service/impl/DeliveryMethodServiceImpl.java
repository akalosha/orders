package name.akalosha.orders.service.impl;

import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Site;
import name.akalosha.orders.repository.jpa.DeliveryMethodRepository;
import name.akalosha.orders.service.DeliveryMethodService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class DeliveryMethodServiceImpl implements DeliveryMethodService {

    private static final Logger LOG = LoggerFactory.getLogger(DeliveryMethodServiceImpl.class);

    private final DeliveryMethodRepository deliveryMethodRepository;

    @Autowired
    public DeliveryMethodServiceImpl(DeliveryMethodRepository deliveryMethodRepository) {
        this.deliveryMethodRepository = deliveryMethodRepository;
    }

    @Override
    public List<DeliveryMethod> getDeliveryMethodsForSite(Site site) {
        Assert.notNull(site, "Site should not be null");
        LOG.trace("Getting delivery methods for site with ID={}", site.getId());

        return deliveryMethodRepository.findBySite(site);
    }

}
