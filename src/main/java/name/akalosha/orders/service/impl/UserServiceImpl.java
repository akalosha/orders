package name.akalosha.orders.service.impl;

import name.akalosha.orders.converters.mappers.search.UserDocMapper;
import name.akalosha.orders.converters.updaters.domain.impl.UserUpdater;
import name.akalosha.orders.exception.NotFoundException;
import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.UserRegisterForm;
import name.akalosha.orders.model.dto.search.UserDoc;
import name.akalosha.orders.repository.elastic.UserSearchRepository;
import name.akalosha.orders.repository.jpa.UserRepository;
import name.akalosha.orders.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Implementation of {@link UserService}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Service
public class UserServiceImpl extends BaseCRUDService<Long, User, UserDoc> implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    public static final Set<String> DEFAULT_ROLES = Collections.singleton("USER");

    private final UserRepository userRepository;
    private final UserSearchRepository userSearchRepository;
    private final UserDocMapper userDocMapper;
    private final Supplier<org.springframework.security.core.userdetails.User> authenticatedUserSupplier;

    // we can't inject PasswordEncoder bean directly because that creates a circular dependency WebSecurityConfig ->
    // UserDetailServiceImpl -> UserServiceImpl -> WebSecurityConfig
    private final ObjectProvider<PasswordEncoder> passwordEncoderProvider;

    @Autowired
    public UserServiceImpl(
            UserRepository userRepository,
            UserSearchRepository userSearchRepository,
            UserDocMapper userDocMapper,
            UserUpdater userUpdater,
            Supplier<org.springframework.security.core.userdetails.User> authenticatedUserSupplier,
            ObjectProvider<PasswordEncoder> passwordEncoderProvider) {
        super(userRepository, userSearchRepository, userDocMapper, userUpdater);
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.userDocMapper = userDocMapper;
        this.authenticatedUserSupplier = authenticatedUserSupplier;
        this.passwordEncoderProvider = passwordEncoderProvider;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        Assert.isTrue(StringUtils.isNotBlank(username), "Username should not be empty");
        LOG.trace("Getting a user with username={}", username);

        return userRepository.findByUsername(username);
    }

    @Override
    public User getCurrentUser() {
        LOG.trace("Getting current user");
        String currentUserName = getCurrentUserName();
        LOG.debug("Current user's username is: {}", currentUserName);
        Optional<User> userOptional = userRepository.findByUsername(currentUserName);
        return userOptional.orElseThrow(() -> new NotFoundException(User.class, currentUserName));
    }

    /**
     * Returns the name of currently authenticated user.
     *
     * @return name of currently authenticated user
     */
    private String getCurrentUserName() {
        LOG.trace("Getting current user's username");
        org.springframework.security.core.userdetails.User user = authenticatedUserSupplier.get();

        String username = user.getUsername();
        Assert.isTrue(StringUtils.isNotBlank(username), "Current user has an empty username");
        return username;
    }

    @Override
    public User register(UserRegisterForm registerForm) {
        Assert.notNull(registerForm, "Registration form should not be null");
        LOG.trace("Registering a new user with username={}", registerForm.getUsername());

        User user = toUser(registerForm);
        User savedUser = userRepository.save(user);
        LOG.debug("Saved new user with username={} and ID={} to DB", user.getUsername(), user.getId());
        UserDoc userDoc = userDocMapper.mapReverse(savedUser);
        userSearchRepository.save(userDoc);
        LOG.debug("Saved new user with username={} and ID={} to search repository", userDoc.getUsername(), userDoc.getId());
        return savedUser;
    }

    /**
     * Creates {@link User} object from {@link UserRegisterForm}.
     *
     * @param registerForm form containing user properties
     * @return creates user object
     */
    private User toUser(UserRegisterForm registerForm) {
        Assert.notNull(registerForm, "Register form should not be null");
        LOG.trace("Converting a registration form to a user object for a user with username={}", registerForm.getUsername());

        User user = new User();
        user.setUsername(registerForm.getUsername());
        user.setPassword(encodePassword(registerForm.getPassword()));

        // trying to dispose the raw password as soon as we can
        Arrays.fill(registerForm.getPassword(), 'x');

        user.setRoles(DEFAULT_ROLES);
        LOG.debug("New user was given following roles: {}", user.getRoles());
        return user;
    }

    private String encodePassword(char[] rawPassword) {
        return  getPasswordEncoder().encode(CharBuffer.wrap(rawPassword));
    }

    private PasswordEncoder getPasswordEncoder() {
        return passwordEncoderProvider.getIfAvailable();
    }

    @Override
    public User save(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public User edit(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public User patch(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long userId) {
        throw new UnsupportedOperationException();
    }

}
