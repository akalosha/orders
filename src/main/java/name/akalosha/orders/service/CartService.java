package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.dto.rest.form.AddressForm;
import name.akalosha.orders.repository.jpa.CartRepository;

import java.util.Optional;

/**
 * Implementation of {@link CRUDService} for {@link Cart} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CartService extends CRUDService<Cart, Long> {

    /**
     * Gets the cart by id, additionally fetching cart entries.
     *
     * @param id id of a cart to get
     * @return {@link Cart} instance
     * @see CartRepository#findByIdWithEagerFetch(Long)
     */
    Optional<Cart> findWithEagerFetch(Long id);

    Cart setShippingAddress(Long cartId, AddressForm shippingAddressForm);

    Cart setDeliveryMethod(Long cartId, Long deliveryMethodId);

}
