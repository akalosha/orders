package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.Identifiable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service for CRUD operations on entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CRUDService<T extends Identifiable<ID>, ID> {

    /**
     * Finds all available instances of entity.
     *
     * @param pageable pagination parameters
     * @return collection of entities that were found
     */
    Page<T> findAll(Pageable pageable);

    /**
     * Finds a single entity by its ID.
     *
     * @param id ID of entity to find
     * @return found entity, possibly empty
     */
    Optional<T> find(ID id);

    /**
     * Saves new entity to storage.
     *
     * @param entity entity to save
     * @return entity after saving
     */
    T save(T entity);

    /**
     * Edits properties of an entity.
     *
     * @param entity entity with edited properties. All properties of this entity (including null ones) are saved.
     * @return entity after editing
     */
    T edit(T entity);

    /**
     * Partially edits properties of an entity.
     *
     * @param entity entity with edited properties. Only not null properties are saved.
     * @return entity after editing
     */
    T patch(T entity);

    /**
     * Deletes an entity with given ID.
     * @param id ID of entity to delete
     */
    void delete(ID id);

}
