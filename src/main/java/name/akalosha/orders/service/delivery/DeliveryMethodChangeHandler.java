package name.akalosha.orders.service.delivery;

import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.DeliveryMethod;

/**
 * An observer-like type that can react to delivery method changes.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface DeliveryMethodChangeHandler {

    void beforeDeliveryMethodChange(Cart cart, DeliveryMethod newDeliveryMethod);

    void afterDeliveryMethodChange(Cart cart);

}
