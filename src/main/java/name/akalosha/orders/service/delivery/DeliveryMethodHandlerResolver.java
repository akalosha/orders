package name.akalosha.orders.service.delivery;

import name.akalosha.orders.model.domain.DeliveryMethod;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * A utility that can get a {@link DeliveryMethodChangeHandler} for a given {@link DeliveryMethod}.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class DeliveryMethodHandlerResolver {

    private static final Logger LOG = LoggerFactory.getLogger(DeliveryMethodHandlerResolver.class);

    private ApplicationContext applicationContext;

    @Autowired
    public DeliveryMethodHandlerResolver(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public DeliveryMethodChangeHandler resolveHandler(DeliveryMethod deliveryMethod) {
        Assert.notNull(deliveryMethod, "Delivery method should not be null");

        String handlerBean = deliveryMethod.getHandlerBean();
        LOG.trace("Delivery method {} has a handler bean {}", deliveryMethod.getName(), handlerBean);
        if (StringUtils.isBlank(handlerBean)) {
            throw new IllegalArgumentException("Handler is not set for delivery method " + deliveryMethod.getName());
        }

        return applicationContext.getBean(handlerBean, DeliveryMethodChangeHandler.class);
    }

}
