/**
 * In this package we cn have implementations for delivery/shipping providers we are integrated with. For example, we
 * can have UPSDeliveryMethodProvider that would handle all delivery method options for US site, and FedExDeliveryMethodProvider
 * for CA delivery method options. For now, we don't have any real integrations, so only {@link MockDeliveryMethodProvider} can
 * be found here.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
package name.akalosha.orders.service.delivery.impl;