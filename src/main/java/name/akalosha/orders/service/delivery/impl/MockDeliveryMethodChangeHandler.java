package name.akalosha.orders.service.delivery.impl;

import name.akalosha.orders.model.domain.Cart;
import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.service.delivery.DeliveryMethodChangeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * A mock implementation of a {@link DeliveryMethodChangeHandler} that does nothing but logging.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
@Component
public class MockDeliveryMethodChangeHandler implements DeliveryMethodChangeHandler {

    private static final Logger LOG = LoggerFactory.getLogger(MockDeliveryMethodChangeHandler.class);

    @Override
    public void beforeDeliveryMethodChange(Cart cart, DeliveryMethod newDeliveryMethod) {
        LOG.info(String.format("Changing a delivery method for cart %s to %s", cart.getId(), newDeliveryMethod.getName()));
    }

    @Override
    public void afterDeliveryMethodChange(Cart cart) {
        LOG.info(String.format("Delivery method for cart %s was changed to %s", cart.getId(), cart.getDeliveryMethod().getName()));
    }

}
