package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.Site;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface SiteService {

    String CURRENT_SITE_ATTR = "name.akalosha.orders.currentSite";

    Site resolveCurrentSite(HttpServletRequest request);

    Optional<Site> findSiteByName(String siteName);

}
