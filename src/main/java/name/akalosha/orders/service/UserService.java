package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.User;
import name.akalosha.orders.model.dto.rest.form.UserRegisterForm;

import java.util.Optional;

/**
 * Implementation of {@link CRUDService} for {@link User} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface UserService extends CRUDService<User, Long> {

    /**
     * Finds a user with given name.
     *
     * @param username name to search for
     * @return found user, possibly empty
     */
    Optional<User> findByUsername(String username);

    /**
     * Returns currently authenticated user.
     *
     * @return current user
     */
    User getCurrentUser();

    /**
     * Registers a new user.
     *
     * @param registerForm form containing new user's properties
     * @return created user
     */
    User register(UserRegisterForm registerForm);
}
