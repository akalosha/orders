package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.DeliveryMethod;
import name.akalosha.orders.model.domain.Site;

import java.util.List;

/**
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface DeliveryMethodService {

    List<DeliveryMethod> getDeliveryMethodsForSite(Site site);

}
