package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Implementation of {@link CRUDService} for {@link Product} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface ProductService extends CRUDService<Product, Long> {

    /**
     * Returns a list of products with names matching a given pattern.
     *
     * @param pageable pagination parameters
     * @param name name pattern
     * @return a list of products with matching names
     */
    Page<Product> findByName(Pageable pageable, String name);

}
