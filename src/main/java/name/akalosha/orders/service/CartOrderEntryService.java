package name.akalosha.orders.service;

import name.akalosha.orders.model.domain.CartOrderEntry;

import java.util.Collection;
import java.util.Optional;

/**
 * Implementation of {@link CRUDService} for {@link CartOrderEntry} entity.
 *
 * @author Aleksey Kalosha <aleksey.kalosha@gmail.com>
 */
public interface CartOrderEntryService {

    /**
     * Finds all cart entries by cart ID.
     *
     * @param cartId ID of cart containing entries
     * @return cart entries that were found
     */
    Collection<CartOrderEntry> findAll(Long cartId);

    /**
     * Finds a single cart entry by cart ID.
     *
     * @param cartId ID of cart containing entry
     * @param entryId ID of entry to find
     * @return found entry, possibly empty
     */
    Optional<CartOrderEntry> find(Long cartId, Long entryId);

    /**
     * Saves new cart entry for cart with given ID.
     *
     * @param cartId ID of cart that would contain new entry
     * @param entry entry to save
     * @return entry after saving
     */
    CartOrderEntry save(Long cartId, CartOrderEntry entry);

    /**
     * Changes quantity of cart entry.
     *
     * @param cartId ID of cart containing entry
     * @param entryId ID of entry to change quantity for
     * @param newQty new entry quantity
     * @return entry after quantity change
     */
    CartOrderEntry editQuantity(Long cartId, Long entryId, int newQty);

    /**
     * Deletes cart entry with given ID.
     *
     * @param cartId ID of cart containing entry
     * @param entryId ID of entry to delete
     */
    void delete(Long cartId, Long entryId);

}
